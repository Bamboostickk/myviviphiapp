//
//  VCReportEditorLandingController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-28.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import UIKit

class VCReportEditorLandingController:UITableViewController
{
	var arrayOfQuestions:[String] = [String]()
	var questionDictionary:[Int:String] = [:]
	var arrayOfAnswers:[String] = [String]()
	
	var hasGender:Bool = false
	var hasCancer:Bool = false
	
	@IBAction func unwindToReports(_ sender: AnyObject)
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	override func viewDidLoad()
	{
		self.title = "TEST" //SharedResourceManager.sharedInstance.patientDataController.patientsList[SharedResourceManager.sharedInstance.patientID]
		self.updateCancerForGender()
		self.updateCancerFieldsFromType()
		self.generateQuestionArray()
		super.viewDidLoad()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(true)
		self.tableView.reloadData()
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	// Return the number of rows for questions
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return arrayOfQuestions.count
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath)
		
		if (arrayOfQuestions.count > 0)
		{
			cell.textLabel?.text = arrayOfQuestions[indexPath.row]
			let keyForRow:Int = self.questionDictionary.allKeys(forValue: (cell.textLabel?.text)!)[0]
			cell.detailTextLabel?.text = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[keyForRow]!
		}
			
		else
		{
			cell.textLabel?.text = "No questions available."
		}
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		if (tableView.cellForRow(at: indexPath) != nil)
		{
			let selectedQuestion = indexPath.row + 1
			loadQuestionView(questionNumber:selectedQuestion)
		}
	}
	
	func generateQuestionArray()
	{
		let localQuestionDictionary = SharedResourceManager.sharedInstance.questionController.returnQuestions()
		
		for number in SharedResourceManager.sharedInstance.questionController.orderedQuestionIDs
		{
			let questionField:String = String(describing: localQuestionDictionary[number]!.questionName!)
			self.questionDictionary[number] = questionField
			self.arrayOfQuestions.append(questionField)
		}
	}
	
	func loadQuestionView(questionNumber:Int)
	{
		switch questionNumber
		{
			case 1:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 1
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditComplexButtonView") as? VCEditButtonComplex {
					present(resultController, animated: true, completion: nil)
				}
			case 2:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 2
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditAgeView") as? VCEditAge {
					present(resultController, animated: true, completion: nil)
				}
			case 3:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 3
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditComplexButtonView") as? VCEditButtonComplex {
					self.updateCancerForGender()
					present(resultController, animated: true, completion: nil)
				}
			case 4:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 4
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditChecklistLanding") as? UINavigationController {
					self.updateCancerFieldsFromType()
					present(resultController, animated: true, completion: nil)
				}
			case 5:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 5
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditComplexButtonView") as? VCEditButtonComplex {
					present(resultController, animated: true, completion: nil)
				}
			case 6:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 6
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditChecker") as? VCEditChecker {
					present(resultController, animated: true, completion: nil)
				}
			case 7:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 7
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditChecker") as? VCEditChecker {
					present(resultController, animated: true, completion: nil)
				}
			case 8:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 8
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditChecklistLanding") as? UINavigationController  {
					present(resultController, animated: true, completion: nil)
				}
			case 9:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 9
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditChecklistLanding") as? UINavigationController {
					present(resultController, animated: true, completion: nil)
				}
			case 10:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 10
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditChecklistLanding") as? UINavigationController
				{
					present(resultController, animated: true, completion: nil)
				}
			case 11:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 11
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditComplexButtonView") as? VCEditButtonComplex {
					present(resultController, animated: true, completion: nil)
				}
			case 12:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 12
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditPostalCodeView") as? VCEditPostalCode {
					present(resultController, animated: true, completion: nil)
				}
			case 13:
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 13
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditComplexButtonView") as? VCEditButtonComplex {
					present(resultController, animated: true, completion: nil)
				}
			default:
				break
		}
	}
	
	func updateCancerForGender()
	{
		let genderValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientGender![0].codeID
		
		if (genderValue != 0)
		{
			SharedResourceManager.sharedInstance.questionController.getCancersForGender(genderID: genderValue){ () -> () in
				self.hasGender = true
			}
		}
	}
	
	func updateCancerFieldsFromType()
	{
		if (SharedResourceManager.sharedInstance.patientDataController.patientCancer!.count > 0)
		{
			let cancerValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientCancer![0].codeID
			
			if (cancerValue != 0)
			{
				SharedResourceManager.sharedInstance.questionController.getHistology(cancerID: cancerValue){() -> () in}
				SharedResourceManager.sharedInstance.questionController.getCancerStage(cancerID: cancerValue)
				SharedResourceManager.sharedInstance.questionController.getTumours(cancerID: cancerValue)
			}
		}
	}
	
}
