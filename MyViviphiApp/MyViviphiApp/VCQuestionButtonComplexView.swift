//
//  VCQuestionButtonComplexView.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-06.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import UIKit

class VCQuestionButtonComplexView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
	@IBOutlet weak var textLabel: UILabel!
	
	@IBOutlet weak var mainCollectionView: UICollectionView!
	@IBOutlet weak var otherCollectionView: UICollectionView!
	
	var isEditingQuestion = false
	var nextButtonText = "Next" // Set what text should be used in the top right corner

	var questionsList:[String] = [String]()
	var questionDictionary:[Int:String] = [:]
	
	var shortOptionsList:[MVButtonEntry] = [MVButtonEntry]()
	var otherOptionsList:[MVButtonEntry] = [MVButtonEntry]()
	
	var answerObjects:[MVPatientAnswers] = [MVPatientAnswers]()
	var storedStartingObjects:[MVPatientAnswers] = [MVPatientAnswers]()
	var selectedStringValues:[String] = [String]()
	var buttonEntries:[MVButtonEntry] = [MVButtonEntry]()
	var otherSelectedValue:String = ""
	
	var answersSelectedDictionary:[Int:Bool] = [:]
	
	var hasCheckController:Bool? = false
	
	var selectedList:[Bool] = [Bool]()
	
	override func willMove(toParentViewController parent: UIViewController?)
	{
		super.willMove(toParentViewController: parent)
		
		if parent == nil
		{
			if (self.hasCheckController == false && SharedResourceManager.sharedInstance.questionController.activeQuestionNumber > 1)
			{
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
				SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			}
				
			else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 1)
			{
				SharedResourceManager.sharedInstance.questionController.resetAll()
			}

		}
		

	}
	
	@IBAction func nextButtonPressed(_ sender: AnyObject)
	{
		
		if(isEditingQuestion){
			// nothing specific for now?
		}
		
		
		// Go to the next screen when tapping the next button
		//self.writeChoicesSelected()
		self.loadNextScreen()
	}
	
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		if (self.hasCheckController == false && SharedResourceManager.sharedInstance.questionController.activeQuestionNumber > 1)
		{
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 1)
		{
			SharedResourceManager.sharedInstance.questionController.resetAll()
		}
		//self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func cancelButtonPressed(_ sender: AnyObject)
	{
		if (self.storedStartingObjects.count > 0)
		{
			self.writeOriginalValuesOnCancel()
		}
		
		self.dismiss(animated: true, completion: nil)
	}
	
	// Do any additional setup after loading the view, typically from a nib.
	override func viewDidLoad()
	{
		super.viewDidLoad()

		// If the patient is editing their previous answer from MyHealth tab
		if isEditingQuestion
		{
			nextButtonText = "Done"
			
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			
			// Update the question Title
			//self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText!
			
			// Add Done button if they did answer the question originally
			self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: nextButtonText, style: .done, target: self, action: #selector(self.nextButtonPressed(_:)))

			// Add Cancel button to the left since we are in edit mode
			self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelButtonPressed(_:)))

			self.setupPageForQuestionType()			
		}
		
		// Otherwise the page appears in the questionnaire
		else
		{
			// Set the right bar button to "Skip" by default
			let nextButton = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.nextButtonPressed(_:)))
			self.navigationItem.setRightBarButton(nextButton, animated: false)

			// Adjust Question Values as needed?
			if (self.hasCheckController == false)
			{
				SharedResourceManager.sharedInstance.questionController.activeQuestionNumber += 1
				SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			}
			
			// If the question has the ability to select multiple options, enable it in the collection views.
			if let multiSelectSupport = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionSupportsMultipleAnswers
			{
				mainCollectionView.allowsMultipleSelection = multiSelectSupport
				otherCollectionView.allowsMultipleSelection = multiSelectSupport
			}

			self.setupPageForQuestionType()
		}

		self.textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
		self.textLabel.numberOfLines = 3
		self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText!	

		// Make sure this class is controlling the collectionviews
		self.mainCollectionView.delegate = self
		self.mainCollectionView.dataSource = self

		
		/*
		// !!!!!!! Note: This is required to be true to add the ability to DEselect cells. However, we must add logic to ensure that only one (or x amount) of cells are selected at a time!!!!
		mainCollectionView.allowsMultipleSelection = true
		otherCollectionView.allowsMultipleSelection = true
		*/
		
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		// Return the appropriate number of items for that collection view
		
		if(collectionView == mainCollectionView)
		{
			return self.shortOptionsList.count
		}
			
		else
		{
			return self.otherOptionsList.count
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QuestionCollectionViewCell
		
		if collectionView == mainCollectionView
		{
			if(self.shortOptionsList.count > 0)
			{
				cell.label.numberOfLines = 2
				cell.label.text = self.shortOptionsList[indexPath.row].description
				
				if (self.isEditingQuestion == true)
				{
					if (self.shortOptionsList[indexPath.item].isSelected == true)
					{
						//cell.contentView.backgroundColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 1.0)
						//cell.backgroundColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 1.0)
						cell.isSelected = true
						self.mainCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
					}
				}
				
				else
				{
					self.selectedStringValues = SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.retrieveValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID)
					
					if (self.selectedStringValues.contains(cell.label.text!))
					{
						//cell.backgroundColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 1.0)
						cell.isSelected = true
						self.mainCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
					}
				}
			}
		}
			
		else
		{
			if (self.otherOptionsList.count > 0)
			{
				//cell.label.numberOfLines = 2
				cell.label.text = self.otherOptionsList[indexPath.row].description
				
				if (self.isEditingQuestion == true)
				{
					
					if (self.otherOptionsList[indexPath.item].isSelected == true)
					{

						//cell.contentView.backgroundColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 1.0)
						cell.isSelected = true
						self.otherCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
						
//						if (self.otherOptionsList[indexPath.item].description == "Other")
//						{
//							cell.isSelected = false
//							self.otherCollectionView.deselectItem(at: indexPath, animated: false)
//						}
					}
				}
				
				else
				{
					if (self.selectedStringValues.contains(cell.label.text!))
					{
						//cell.backgroundColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 1.0)
						cell.isSelected = true
						self.otherCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
					}
				}
			}
		}
		
		self.selectedList.append(false)
		
		return cell
	}
	
	
	// MARK: - UICollectionViewDelegate protocol
	
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		let cell = collectionView.cellForItem(at: indexPath)
		let selectedCell:QuestionCollectionViewCell = cell as! QuestionCollectionViewCell
		let cellLabel:String = selectedCell.label.text!
		let answerValue:Int = self.questionDictionary.allKeys(forValue: cellLabel)[0]
		self.answersSelectedDictionary[answerValue] = true
		self.buttonSelectedAction(buttonNamed:cellLabel)
		self.selectedList[indexPath.item] = true
		
		cell?.isSelected = true

		//print("You selected cell #\(indexPath.item)!")
	}
	
	func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool
	{

		/*
		let cell = collectionView.cellForItem(at: indexPath)
		if (cell?.isSelected)!{
		
		return false
		}
		
		*/
		// Set the right bar button to "Next" if the list was empty before selecting something
		if !selectedList.contains(true){
			let nextButton = UIBarButtonItem(title: nextButtonText, style: .done, target: self, action: #selector(self.nextButtonPressed(_:)))
			self.navigationItem.setRightBarButton(nil, animated: true)
			self.navigationItem.setRightBarButton(nextButton, animated: true)
		}
		
		return true
	}
	
	
	func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool
	{
		return true
	}
	
	
	func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
	{
		
		// handle tap events
		//print("You deselected cell #\(indexPath.item)!")
		let cell = collectionView.cellForItem(at: indexPath)
		let selectedCell:QuestionCollectionViewCell = cell as! QuestionCollectionViewCell
		let cellLabel:String = selectedCell.label.text!
		let answerValue:Int = self.questionDictionary.allKeys(forValue: cellLabel)[0]
		self.answersSelectedDictionary[answerValue] = false
		//self.buttonDeselectedAction(buttonNamed: cellLabel)
		
		self.selectedList[indexPath.item] = false
		cell?.isSelected = false
		
		// Set the right bar button to "Skip" if the list is empty after deselecting something
		/*if !selectedList.contains(true){
		let nextButton = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.nextButtonPressed(_:)))
		self.navigationItem.setRightBarButton(nil, animated: true)
		self.navigationItem.setRightBarButton(nextButton, animated: true)
		}*/
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		if (collectionView.numberOfItems(inSection: 0) < 3) && (collectionView.numberOfItems(inSection: 0) > 0)
		{
			return CGSize(width: mainCollectionView.bounds.width/(CGFloat(collectionView.numberOfItems(inSection: 0)) + 0.25), height: 50)
			
		}
		
		return CGSize(width: mainCollectionView.bounds.width/3.25, height: 50)
	}
	/*
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
	return CGSize(width: self.view.frame.width/3, height: 100);
	}
	*/
	
	
	// Called when we're editing questions from MyHealth Tab
	func setupPageForQuestionType()
	{
		let question:String = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		
		switch question
		{
		case "Gender":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfGenders!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfGenders!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientGender!
		case "Cancer Diagnosis":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfCancers!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfCancers!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientCancer!
		case "Histology":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfHistologies!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfHistologies!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientHistology!
		case "Cancer Stage":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfCancerStages!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfCancerStages!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientStage!
		case "Tumor Sequencing":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfMarkers!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMarkers!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientTumourMarkers!
		case "Genomic Alterations":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfAlterations!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAlterations!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientGenomicAlterations!
		case "Other Diagnosis":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientOtherDiseases!
		case "Other Medications":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfMedications!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMedications!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientMedications!
		case "Allergies":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfAllergies!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAllergies!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientAllergies!
		case "Ethnicity":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfEthnicities!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfEthnicities!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientEthnicity!
		case "Health Insurer":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfHealthInsurers!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfHealthInsurers!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientInsuruer!
		default:
			break
		}
		
		self.storedStartingObjects = answerObjects
		
		self.initialiseSelectionArray()
		self.determineSelectedEntries()
		self.setQuestionArrays(totalArray:buttonEntries)
		self.findOtherSelectionInAnswer(patientAnswer: answerObjects)

		//addButtons(listOfOptions: questionsList)
	}
	
	
	func setQuestionArrays(totalArray:[MVButtonEntry])
	{
		for element in totalArray
		{
			if (element.description == "None" || element.description == "I'm not sure" || element.description == "I don't have cancer" || element.description == "Other" || element.description == "Recurrent")
			{
				
				// Used element.description when merging the "edit" version of the page
				self.otherOptionsList.append(element)
			}
				
			else
			{
				// Used element.description when merging the "edit" version of the page
				self.shortOptionsList.append(element)

			}
		}
	}
	
//	func setQuestionArrays(totalArray:[String])
//	{
//		for element in totalArray
//		{
//			if (element == "None" || element == "I'm not sure" || element == "I don't have cancer" || element == "Other" || element == "Recurrent")
//			{
//				self.otherOptionsListString.append(element)
//			}
//				
//			else
//			{
//				self.shortOptionsListString.append(element)
//			}
//		}
//	}
	
	func determineSelectedEntries()
	{
		var selectedAnswerIDs:[Int] = [Int]()
		var hasOther = false
		
		for answer in self.answerObjects
		{
			selectedAnswerIDs.append(answer.codeID)
			
			if (answer.description == "Other" && answer.customText != "")
			{
				hasOther = true
			}
			
		}
		
		if (self.buttonEntries.count > 0)
		{
			for index in 0...self.buttonEntries.count - 1
			{
				if (selectedAnswerIDs.contains(self.buttonEntries[index].codeID))
				{
					self.buttonEntries[index].isSelected = true
				}
				
				if (self.buttonEntries[index].description == "Other" && hasOther == true)
				{
					self.buttonEntries[index].isSelected = true
					hasOther = false
				}
			}
		}
	}
	
	func initialiseSelectionArray()
	{
		if (questionsList.count > 0)
		{
			for choice in questionsList
			{
				var choiceAttributes:MVButtonEntry = MVButtonEntry()
				choiceAttributes.codeID = self.questionDictionary.allKeys(forValue: choice)[0]
				choiceAttributes.description = choice
				choiceAttributes.isSelected = false
				
				buttonEntries.append(choiceAttributes)
			}
		}
	}

	func findOtherSelectionInAnswer(patientAnswer:[MVPatientAnswers])
	{
		for answer in patientAnswer
		{
			if (answer.description == "Other" || answer.description == "")
			{
				self.otherSelectedValue = answer.customText
				break
			}
		}
	}
	
	// Actions to carry out when a button is pressed
	// When a button is pressed, grabs the button's title and then updates the questionnaire
	// Either by loading a new scene, or grabbing new data and then loading the new scene
	
	func buttonSelectedAction(buttonNamed:String)
	{
		/*
		if (self.allowsMultipleSelection == true)
		{
		if (buttonNamed == "None")
		{
		deselectCells(exceptCellNamed:"None")
		self.writeChoice(buttonValue: buttonNamed, willClear: true)
		}
		
		else if (buttonNamed == "Other")
		{
		self.selectedStringValues.append(buttonNamed)
		self.customTextField()
		}
		
		else
		{
		deselectNone()
		self.selectedStringValues.append(buttonNamed)
		self.writeChoice(buttonValue: buttonNamed, willClear: false)
		}
		}
		
		else
		{
		if (buttonNamed == "None")
		{
		deselectCells(exceptCellNamed:"None")
		self.writeChoice(buttonValue: buttonNamed, willClear: true)
		self.loadNextScreen()
		}
		
		else if (buttonNamed == "I don't have cancer")
		{
		deselectCells(exceptCellNamed:"I don't have cancer")
		self.writeChoice(buttonValue: buttonNamed, willClear: true)
		self.loadNextScreen()
		}
		
		else if (buttonNamed == "Other")
		{
		self.selectedStringValues.append(buttonNamed)
		self.customTextField()
		}
		
		else
		{
		deselectNone()
		self.writeChoice(buttonValue: buttonNamed, willClear: false)
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonNamed)
		let selectedKeyFromArray = keyForValueArray[0]
		self.selectedStringValues.append(buttonNamed)
		updateQuestionnaireFromSelection(selectedAnswer: selectedKeyFromArray)
		//self.loadNextScreen()
		}
		
		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: self.selectedStringValues)
		}*/
		
		// When tapping "None' or "I don't have cancer", make sure we deselect other cells too
		if (buttonNamed == "None" || buttonNamed == "I'm not sure" || buttonNamed == "I don't have cancer" || buttonNamed == "Recurrent")
		{
			self.deselectOther()
			deselectCells(exceptCellNamed:buttonNamed)
			self.writeChoice(buttonValue: buttonNamed, willClear: true)
		}
			
		// When tapping "Other" ask the user to input text too
		else if (buttonNamed == "Other")
		{
			self.deselectNone()
			self.deselectMainCells()
			self.selectedStringValues.append(buttonNamed)
			self.customTextField()
			
		}
			
		// General Case
		else
		{
			self.deselectNone()
			self.deselectOther()

			self.writeChoice(buttonValue: buttonNamed, willClear: false)
			let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonNamed)
			
			let selectedKeyFromArray = keyForValueArray[0]
			self.selectedStringValues.append(buttonNamed)
			self.updateQuestionnaireFromSelection(selectedAnswer: selectedKeyFromArray)

			if (isEditingQuestion == true)
			{
				self.clearSelectionsFromDependencies(buttonValue: buttonNamed)
			}
		}

		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: self.selectedStringValues)
		
	}
	
	func buttonDeselectedAction(buttonNamed:String)
	{
		self.removeChoice(buttonValue: buttonNamed)
		if let indexToRemove:Int = self.selectedStringValues.index(of: buttonNamed)
		{
			self.selectedStringValues.remove(at: indexToRemove)
		}
		
		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: self.selectedStringValues)
	}
	
	func deselectOther()
	{

		for indexPath in self.otherCollectionView.indexPathsForSelectedItems!
		{
			let selectedCell:QuestionCollectionViewCell = self.otherCollectionView.cellForItem(at: indexPath) as! QuestionCollectionViewCell
			let cellLabel:String = selectedCell.label.text!
			
			if (cellLabel == "Other")
			{
				selectedCell.isSelected = false
				selectedCell.backgroundColor = UIColor.clear
				self.otherCollectionView.deselectItem(at: indexPath, animated:false)
				self.selectedList[indexPath.item] = false
			}
		}
		
		if let indexOther = self.selectedStringValues.index(of: "Other")
		{
			self.selectedStringValues.remove(at: indexOther)
		}
	}
	
	func deselectNone()
	{
		for indexPath in self.otherCollectionView.indexPathsForSelectedItems!
		{
			let selectedCell:QuestionCollectionViewCell = self.otherCollectionView.cellForItem(at: indexPath) as! QuestionCollectionViewCell
			let cellLabel:String = selectedCell.label.text!
			
			if (cellLabel == "None" || cellLabel == "I don't have cancer" || cellLabel == "I\'m not sure" || cellLabel == "Recurrent")
			{
				selectedCell.isSelected = false
				selectedCell.backgroundColor = UIColor.clear
				self.otherCollectionView.deselectItem(at: indexPath, animated:false)
				self.selectedList[indexPath.item] = false
			}
		}
		
		if let indexNone = self.selectedStringValues.index(of: "None")
		{
			self.selectedStringValues.remove(at: indexNone)
		}
		
		if let indexNoCancer = self.selectedStringValues.index(of: "I don't have cancer")
		{
			self.selectedStringValues.remove(at: indexNoCancer)
		}
		
		if let indexNotSure = self.selectedStringValues.index(of: "I\'m not sure")
		{
			self.selectedStringValues.remove(at: indexNotSure)
		}
		
		if let indexNotSure = self.selectedStringValues.index(of: "Recurrent")
		{
			self.selectedStringValues.remove(at: indexNotSure)
		}
	}
	
	func deselectMainCells()
	{
		for indexPath in self.mainCollectionView.indexPathsForSelectedItems!
		{
			self.mainCollectionView.deselectItem(at: indexPath, animated: false)
			self.selectedList[indexPath.item] = false
		}
	}

	func deselectCells(exceptCellNamed:String)
	{
		for indexPath in self.mainCollectionView.indexPathsForSelectedItems!
		{
			self.mainCollectionView.deselectItem(at: indexPath, animated: false)
			self.selectedList[indexPath.item] = false
		}
		
		for indexPath in self.otherCollectionView.indexPathsForSelectedItems!
		{
			let selectedCell:QuestionCollectionViewCell = self.otherCollectionView.cellForItem(at: indexPath) as! QuestionCollectionViewCell
			let cellLabel:String = selectedCell.label.text!
			
			if (cellLabel != exceptCellNamed)
			{
				self.otherCollectionView.deselectItem(at: indexPath, animated:false)
				self.selectedList[indexPath.item] = false
			}
		}
		
		self.selectedStringValues.removeAll()
		self.selectedStringValues.append(exceptCellNamed)
	}
	
	// Method only to be used if multiple selections are allowed. Checks if anything is selected
	
	func checkIfAnythingSelected() -> Bool
	{
		return self.selectedList.contains(true)
	}
	
	
	
	// Determines what kind of view controller to load next based on the question ID
	// Only useful if the questions are in a particular order.
	// We can write an algorithm that forces a particualer scene to load based on what the question itself is
	// Needs to be smarter, i.e. checks for question name
	
	func loadNextScreen()
	{
		
		if isEditingQuestion
		{
			let patientIDValue = SharedResourceManager.sharedInstance.patientID
			SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
				DispatchQueue.main.async
				{
					VCOverlayController.hide()
					self.updateCancerForGender()
					self.updateCancerFieldsFromType()
					self.dismiss(animated: true, completion: nil)
				}
			}
		}
			
		else
		{
			
			if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 2)
			{
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "AgeView") as? VCQuestionAge
				{
					self.navigationController?.pushViewController(resultController, animated: true)
					//self.updateCancerForGender()
				}
			}
				
			else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 13)
			{
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView
				{
					self.navigationController?.pushViewController(resultController, animated: true)
				}
			}
				
			else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 5 || SharedResourceManager.sharedInstance.questionController.nextQuestionID == 6)
			{
				
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "AlterationView") as? VCQuestionTableView
				{
					self.navigationController?.pushViewController(resultController, animated: true)
				}
				
				//			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AlterationLanding") as? UINavigationController {
				//				present(resultController, animated: true, completion: nil)
				//			}
			}
				
			else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID  == 11)
			{
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "PostalCodeView") as? VCQuestionPostalCode
				{
					self.navigationController?.pushViewController(resultController, animated: true)
				}
			}
				
				// If the next question ID is negative, the end of the questionnaire is reached
				
			else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID  == -1)
			{
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "QuestionnaireFinished") as? VCQuestionnaireFinished
				{
					self.navigationController?.pushViewController(resultController, animated: true)
				}
			}
				
			else
			{
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView
				{
					self.navigationController?.pushViewController(resultController, animated: true)
				}
			}
		}
	}
	
	// If the current question affects the next question, update the options for the next question
	// Otherwise it loads the next button view controller
	
	func updateQuestionnaireFromSelection(selectedAnswer:Int)
	{
		let question:String = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		
		switch question
		{
		case "Gender":
			SharedResourceManager.sharedInstance.questionController.genderID = selectedAnswer
			self.updateCancers(withGenderID: selectedAnswer)
		case "Cancer Diagnosis":
			SharedResourceManager.sharedInstance.questionController.cancerID = selectedAnswer
			self.updateCancerStages(withCancerID: selectedAnswer)
			self.updateHistology(withCancerID:selectedAnswer)
			self.updateTumourMarkers(withCancerID: selectedAnswer)
		default:
			break
		}
	}
	
	func updateCancerForGender()
	{
		let genderValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientGender![0].codeID
		
		if (genderValue != 0)
		{
			SharedResourceManager.sharedInstance.questionController.listOfCancers = [:]
			SharedResourceManager.sharedInstance.questionController.getCancersForGender(genderID: genderValue){ () -> () in
			}
		}
		
	}
	
	func updateCancerFieldsFromType()
	{
		if (SharedResourceManager.sharedInstance.patientDataController.patientCancer!.count > 0)
		{
			let cancerValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientCancer![0].codeID
			
			if (cancerValue != 0)
			{
				SharedResourceManager.sharedInstance.questionController.listOfHistologies = [:]
				SharedResourceManager.sharedInstance.questionController.listOfCancerStages = [:]
				SharedResourceManager.sharedInstance.questionController.listOfMarkers = [:]
				//SharedResourceManager.sharedInstance.questionController.listOfAlterations = [:]
				
				SharedResourceManager.sharedInstance.questionController.getHistology(cancerID: cancerValue){() -> () in}
				SharedResourceManager.sharedInstance.questionController.getCancerStage(cancerID: cancerValue)
				SharedResourceManager.sharedInstance.questionController.getTumours(cancerID: cancerValue)
			}
		}
	}
	
	func updateCancers(withGenderID:Int)
	{
		SharedResourceManager.sharedInstance.questionController.listOfCancers = [:]
		SharedResourceManager.sharedInstance.questionController.getCancersForGender(genderID: withGenderID){() -> () in
			/*if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "AgeView") as? VCQuestionAge {
			DispatchQueue.main.async
			{
			//self.navigationController?.pushViewController(resultController, animated: true)
			}
			}*/
		}
	}
	
	func updateCancerStages(withCancerID:Int)
	{
		SharedResourceManager.sharedInstance.questionController.listOfCancerStages = [:]
		SharedResourceManager.sharedInstance.questionController.getCancerStage(cancerID: withCancerID)
	}
	
	func updateHistology(withCancerID:Int)
	{
		SharedResourceManager.sharedInstance.questionController.listOfHistologies = [:]
		SharedResourceManager.sharedInstance.questionController.getHistology(cancerID: withCancerID){() -> () in
			/*if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView {
			DispatchQueue.main.async
			{
			//self.navigationController?.pushViewController(resultController, animated: true)
			}
			}*/
		}
	}
	
	func updateTumourMarkers(withCancerID:Int)
	{
		SharedResourceManager.sharedInstance.questionController.getTumours(cancerID: withCancerID)
	}
	
	// Displays error message if nothing is selected
	
	func displayErrorMessage()
	{
		let alertView = UIAlertController(title: "Nothing selected", message: "Please make a selection" as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	func writeOriginalValuesOnCancel()
	{
		for answer in self.storedStartingObjects
		{
			if (answer.description != "" && self.questionDictionary.count > 0)
			{
				self.writeChoice(buttonValue: answer.description, willClear: false)
			}
		}
	}
	
	func writeChoice(buttonValue:String, willClear:Bool)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: willClear)
		
	}
	
	func removeChoice(buttonValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	func removeChoiceFromOtherQuestion(theQuestionID:Int, theCodeID:Int, theLookupID:Int)
	{
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(describing: theQuestionID)
		let codeID:String = String(describing: theCodeID)
		let lookupID:String = String(describing: theLookupID)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	func deselectCustomChoice()
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: "")
	}
	
	func customTextField()
	{
		let myMessage:String = "Last value: " + self.setupTextField(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber)
		
		let alertView = UIAlertController(title: "Enter your data here", message: myMessage as String, preferredStyle: .alert)
		
		let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertView.textFields![0] as UITextField
			
			let textFieldInput:String = firstTextField.text!
			
			self.storeTextFieldValues(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber, userInput: textFieldInput)
			self.customChoice(userInput:textFieldInput)
			
			//self.dismiss(animated: true, completion: nil)
			
			
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
			(action : UIAlertAction!) -> Void in
		})
		
		alertView.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = SharedResourceManager.sharedInstance.patientOtherInputs.lastEntry
		}
		
		alertView.addAction(saveAction)
		alertView.addAction(cancelAction)
		
		self.present(alertView, animated: true, completion: nil)
	}
	
	func storeTextFieldValues(questionNumber:Int, userInput:String)
	{
		switch questionNumber
		{
		case 3:
			SharedResourceManager.sharedInstance.patientOtherInputs.cancer = userInput
		case 4:
			SharedResourceManager.sharedInstance.patientOtherInputs.histology = userInput
		case 5:
			SharedResourceManager.sharedInstance.patientOtherInputs.stage = userInput
		case 6:
			SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers = userInput
		case 7:
			SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations = userInput
		case 8:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions = userInput
		case 9:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications = userInput
		case 10:
			SharedResourceManager.sharedInstance.patientOtherInputs.allergies = userInput
		case 11:
			SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity = userInput
		case 13:
			SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer = userInput
		default:
			break
		}
	}
	
	func setupTextField(questionNumber:Int) -> String
	{
		var theString = "Nothing selected"
		
		switch questionNumber
		{
		case 3:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.cancer
		case 4:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.histology
		case 5:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.stage
		case 6:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers
		case 7:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations
		case 8:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions
		case 9:
			theString  = SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications
		case 10:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.allergies
		case 11:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity
		case 13:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer
		default:
			theString = "Nothing selected"
			break
		}
		
		return theString
	}
	
	// If a previously selected answer doesn't exist in the list of new answers, deselect it.
	
	func clearSelectionsFromDependencies(buttonValue:String)
	{
		let question:Int = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionID!
		
		switch question
		{
		case 1:
			self.deselectCancerIfGenderChanged(buttonValue: buttonValue)
		case 3:
			self.deselectStageIfCancerChanged(buttonValue: buttonValue)
			self.deselectHistologyIfCancerChanged(buttonValue: buttonValue)
			self.deselectTumourMarkerIfCancerChanged(buttonValue: buttonValue)
		default:
			break
		}
	}
	
	func deselectCancerIfGenderChanged(buttonValue:String)
	{
		let cancerSelected:[MVPatientAnswers] = SharedResourceManager.sharedInstance.patientDataController.patientCancer!
		let cancerQuestion:MVQuestion = SharedResourceManager.sharedInstance.questionController.returnSelectedQuestion(pickedQuestionID: 3)
		
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let genderValue:Int = keyForValueArray[0]
		
		if (genderValue != 0)
		{
			SharedResourceManager.sharedInstance.questionController.listOfCancers = [:]
			SharedResourceManager.sharedInstance.questionController.getCancersForGender(genderID: genderValue){ () -> () in

				let availableCancersForGender:[Int:String] = SharedResourceManager.sharedInstance.questionController.listOfCancers!

				for selection in cancerSelected
				{
					if (availableCancersForGender[selection.codeID] == nil)
					{
						self.removeChoiceFromOtherQuestion(theQuestionID: cancerQuestion.questionID!, theCodeID: selection.codeID, theLookupID: cancerQuestion.questionLookupType)
						
						// If uncommented, this will clear out everything. To reproduce website functionality, leave this commented out.
						
						/*
						let stageSelected:[MVPatientAnswers] = SharedResourceManager.sharedInstance.patientDataController.patientStage!
						let stageQuestion:MVQuestion = SharedResourceManager.sharedInstance.questionController.returnSelectedQuestion(pickedQuestionID: 4)
						let histologySelected:[MVPatientAnswers] = SharedResourceManager.sharedInstance.patientDataController.patientHistology!
						let histologyQuestion:MVQuestion = SharedResourceManager.sharedInstance.questionController.returnSelectedQuestion(pickedQuestionID: 13)
						let markersSelected:[MVPatientAnswers] = SharedResourceManager.sharedInstance.patientDataController.patientTumourMarkers!
						let markerQuestion:MVQuestion = SharedResourceManager.sharedInstance.questionController.returnSelectedQuestion(pickedQuestionID: 5)
						
						
						
						for stage in stageSelected
						{
							self.removeChoiceFromOtherQuestion(theQuestionID: stageQuestion.questionID!, theCodeID: stage.codeID, theLookupID: stageQuestion.questionLookupType)
						}
						
						for histology in histologySelected
						{
							self.removeChoiceFromOtherQuestion(theQuestionID: histologyQuestion.questionID!, theCodeID: histology.codeID, theLookupID: histologyQuestion.questionLookupType)
						}
						
						for marker in markersSelected
						{
							self.removeChoiceFromOtherQuestion(theQuestionID: markerQuestion.questionID!, theCodeID: marker.codeID, theLookupID: markerQuestion.questionLookupType)
						}
						*/
					}
				}
			}
		}
	}
	
	func deselectStageIfCancerChanged(buttonValue:String)
	{
		let stageSelected:[MVPatientAnswers] = SharedResourceManager.sharedInstance.patientDataController.patientStage!
		let stageQuestion:MVQuestion = SharedResourceManager.sharedInstance.questionController.returnSelectedQuestion(pickedQuestionID: 4)

		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let cancerValue:Int = keyForValueArray[0]
		
		if (cancerValue != 0)
		{
			SharedResourceManager.sharedInstance.questionController.listOfCancerStages = [:]
			SharedResourceManager.sharedInstance.questionController.getCancerStageWithDelay(cancerID: cancerValue){ () -> () in
				
				let availableStagesForCancer:[Int:String] = SharedResourceManager.sharedInstance.questionController.listOfCancerStages!

				for selection in stageSelected
				{
					if (availableStagesForCancer[selection.codeID] == nil)
					{
						self.removeChoiceFromOtherQuestion(theQuestionID: stageQuestion.questionID!, theCodeID: selection.codeID, theLookupID: stageQuestion.questionLookupType)
					}
				}
			}
		}
	}
	
	func deselectHistologyIfCancerChanged(buttonValue:String)
	{
		let histologySelected:[MVPatientAnswers] = SharedResourceManager.sharedInstance.patientDataController.patientHistology!
		let histologyQuestion:MVQuestion = SharedResourceManager.sharedInstance.questionController.returnSelectedQuestion(pickedQuestionID: 13)
		
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let cancerValue:Int = keyForValueArray[0]
		
		if (cancerValue != 0)
		{
			SharedResourceManager.sharedInstance.questionController.listOfHistologies = [:]
			SharedResourceManager.sharedInstance.questionController.getHistology(cancerID: cancerValue){ () -> () in
				
				let availableHistologiesForCancer:[Int:String] = SharedResourceManager.sharedInstance.questionController.listOfHistologies!
				
				for selection in histologySelected
				{
					if (availableHistologiesForCancer[selection.codeID] == nil)
					{
						self.removeChoiceFromOtherQuestion(theQuestionID: histologyQuestion.questionID!, theCodeID: selection.codeID, theLookupID: histologyQuestion.questionLookupType)
					}
				}
			}
		}
	}
	
	func deselectTumourMarkerIfCancerChanged(buttonValue:String)
	{
		let markersSelected:[MVPatientAnswers] = SharedResourceManager.sharedInstance.patientDataController.patientTumourMarkers!
		let markersQuestion:MVQuestion = SharedResourceManager.sharedInstance.questionController.returnSelectedQuestion(pickedQuestionID: 5)
		
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let cancerValue:Int = keyForValueArray[0]
		
		if (cancerValue != 0)
		{
			SharedResourceManager.sharedInstance.questionController.listOfMarkers = [:]
			SharedResourceManager.sharedInstance.questionController.getTumoursWithDelay(cancerID: cancerValue){ () -> () in
				
				let availableMarkersForCancer:[Int:String] = SharedResourceManager.sharedInstance.questionController.listOfMarkers!

				for selection in markersSelected
				{
					if (availableMarkersForCancer[selection.codeID] == nil)
					{
						self.removeChoiceFromOtherQuestion(theQuestionID: markersQuestion.questionID!, theCodeID: selection.codeID, theLookupID: markersQuestion.questionLookupType)
					}
				}
			}
		}
	}
}
