//
//  VCDictionaryView.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-09.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import UIKit

class VCDictionaryView: UITableViewController, UISearchBarDelegate, UISearchResultsUpdating
{
	let searchController = UISearchController(searchResultsController: nil)

	var searchTerms:String = ""
	
	var originalCancerDictionary:[String:String] = [:]
	
	var listOfTerms:[String] = [String]()
	var filteredTerms:[String] = [String]()
	
	// Need these to figure out where to put entries
	
	let letters = CharacterSet.letters
	
	let sections = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z", "#"]

	var sectionedCancerTerms:[String:[String]] = ["A":[String](),"B":[String](),"C":[String](),"D":[String](),"E":[String](),"F":[String](),"G":[String](),"H":[String](),"I":[String](),"J":[String](),"K":[String](),"L":[String](),"M":[String](),"N":[String](),"O":[String](),"P":[String](),"Q":[String](),"R":[String](),"S":[String](),"T":[String](),"U":[String](),"V":[String](),"W":[String](),"X":[String](),"Y":[String](),"Z":[String](), "#":[String]()]
	
	// This is needed to adjust the background of the status bar for the table view
	var viewCover:UIView!

	
	
	override func viewDidLoad()
	{
		// Slow O(n) way of populating the dictionary
		
		for (key, value) in SharedResourceManager.sharedInstance.cancerDictionary.cancerDictionary!
		{
			self.originalCancerDictionary[key as! String] = value as? String
		}
		
		
		// Setup the Search Controller
		searchController.searchResultsUpdater = self
		searchController.hidesNavigationBarDuringPresentation = false
		searchController.dimsBackgroundDuringPresentation = false
		searchController.searchBar.sizeToFit()
		searchController.searchBar.searchBarStyle = .minimal
		searchController.searchBar.backgroundColor = UIColor.white
		searchController.searchBar.delegate = self
		definesPresentationContext = true
		self.tableView.tableHeaderView = searchController.searchBar
		
		/*
		searchController.searchResultsUpdater = self
		searchController.dimsBackgroundDuringPresentation = false
		searchController.searchBar.isTranslucent = false
		searchController.searchBar.setValue("Done", forKey: "_cancelButtonText")
		searchController.searchBar.searchBarStyle = .default
		searchController.searchBar.backgroundColor = UIColor.white
		searchController.searchBar.barTintColor = UIColor.white
		searchController.searchBar.tintColor = UIColor.blue
		self.extendedLayoutIncludesOpaqueBars = true
		
		
		if let searchTextField = searchController.searchBar.value(forKey: "searchField") as? UITextField{
			searchTextField.backgroundColor = UIColor(colorLiteralRed: 200/255, green: 199/255, blue: 204/255, alpha: 0.33)
		}
		
		
		searchController.searchBar.backgroundImage = UIImage()
		self.navigationController?.navigationBar.isTranslucent = false;
		//definesPresentationContext = true
		//searchController.searchBar.sizeToFit()
		self.edgesForExtendedLayout = []
		//self.extendedLayoutIncludesOpaqueBars = false
		self.automaticallyAdjustsScrollViewInsets = false
		self.navigationController?.extendedLayoutIncludesOpaqueBars = true
		self.tableView.tableHeaderView = searchController.searchBar
		//self.extendedLayoutIncludesOpaqueBars = true
		//self.tableView.clipsToBounds = true
*/
		
		self.listOfTerms = Array(self.originalCancerDictionary.keys).sorted(by: <)
		self.populateSectionsDictionary()
		super.viewDidLoad()
		tableView.delegate = self
		tableView.dataSource = self
	}
	
 
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		
		if (searchController.isActive && searchController.searchBar.text != "")
		{
			return 1
		}
		
		return sections.count
	}
	
	
	override func sectionIndexTitles(for tableView: UITableView) -> [String]?
	{
		return sections
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		if ((searchController.isActive) && (searchController.searchBar.text != ""))
		{
			return filteredTerms.count
		}
		
		return (self.sectionedCancerTerms[sections[section]]?.count)!
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
	{
		
		if (searchController.isActive && searchController.searchBar.text != "")
		{
			return "Searching..."
		}
		
		let theSectionLetter:String = sections[section]
		return theSectionLetter
	}
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(withIdentifier: "TermCell", for: indexPath)
		
		var cellText:String = ""
		
		if ((listOfTerms.count) > 0)
		{
			if ((searchController.isActive) && (searchController.searchBar.text != ""))
			{
				 cellText = filteredTerms[indexPath.row]
			}
			
			else
			{
				let theSectionLetter:String = sections[indexPath.section]
				let theCellItem = sectionedCancerTerms[theSectionLetter]
				
				cellText = theCellItem![indexPath.row].description
			}
		}

		cell.textLabel?.text = cellText
		cell.detailTextLabel?.text = createPreviewString(forTerm: cellText)
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		if let cell = tableView.cellForRow(at: indexPath)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "DefinitionView") as? VCDefinitionViewer {
				resultController.termText = (cell.textLabel?.text)!
				resultController.definitionText = self.originalCancerDictionary[(cell.textLabel?.text)!]!
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
	}
	
	
	func position(for bar: UIBarPositioning) -> UIBarPosition {
		return UIBarPosition.topAttached
	}
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchBar.searchBarStyle = .default
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searchBar.searchBarStyle = .minimal
	}
	
	
	func updateSearchResultsForSearchController(searchController: UISearchController)
	{
		filterContentForSearchText(searchText: searchController.searchBar.text!)
	}
	
	func filterContentForSearchText(searchText:String)
	{
		filteredTerms = listOfTerms.filter{ terms in
			return terms.lowercased().contains(searchText.lowercased())
		}
		
		tableView.reloadData()
	}
	
	func populateSectionsDictionary()
	{
		for entry in self.listOfTerms
		{
			let characterArrayFromString:[Character] = Array(entry.description.characters)
			let firstCharacter:String = String(describing:characterArrayFromString[0])
			
			for unicodeScalarElement in firstCharacter.unicodeScalars
			{
				if letters.contains(unicodeScalarElement)
				{
					self.sectionedCancerTerms[firstCharacter]?.append(entry)
				}
					
				else
				{
					self.sectionedCancerTerms["#"]?.append(entry)
				}
			}
		}
	}
	
	func createPreviewString(forTerm:String) -> String
	{
		let definition:String = self.originalCancerDictionary[forTerm]!
		
		let defArray:[String] = definition.characters.split(separator: " ").map(String.init)
		var resultantStringArray:[String] = [String]()
		
		let defArraySize:Int = defArray.count
		let maxToUse:Int = min(defArraySize - 1, 20)

		for index in 0...maxToUse
		{
			resultantStringArray.append(defArray[index])
		}
		
		let finalString:String = resultantStringArray.joined(separator: " ") + "..."
		
		return finalString
	}
	
	func filterContentForSearchText(searchString:String)
	{
		
		filteredTerms = listOfTerms.filter(){ terms in
			return terms.lowercased().contains(searchString.lowercased())
		}
		
		tableView.reloadData()
	}
	
	// MARK: - UISearchBar Delegate
	func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int)
	{
		filterContentForSearchText(searchText: searchBar.text!)
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
	{
		self.searchTerms = searchText
	}
	
	func updateSearchResults(for searchController: UISearchController)
	{
		filterContentForSearchText(searchText: searchController.searchBar.text!)
	}
}
