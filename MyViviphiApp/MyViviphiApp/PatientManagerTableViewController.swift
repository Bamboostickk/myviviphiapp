//
//  PatientManagerTableViewController.swift
//  MyViviphiApp
//
//  Created by Sasha Ivanov on 2016-12-20.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class PatientManagerTableViewController: UITableViewController {

	
	var patientList:[MVPatient] = []
	var patientNames:[String] = [String]()	
	var patientImages:[UIImage] = [UIImage]()

	
    @IBAction func addButtonPressed(_ sender: Any){
        showNamePatientAlert()
    }
	
	
    @IBAction func logoutButtonPressed(_ sender: Any) {
		
		
		// From Original Collection View Controller (Currently not working?)
		UserDefaults.standard.removeObject(forKey: "accessToken")
		UserDefaults.standard.removeObject(forKey: "hasLoginKey")
		UserDefaults.standard.synchronize()
		
		if let resultController = storyboard!.instantiateViewController(withIdentifier: "LoginScene") as? VCLoginScreenViewController
		{
			present(resultController, animated: true, completion: nil)
		}
    }
    override func viewDidLoad() {
        super.viewDidLoad()

		// Add "Edit" button as left bar button
		//self.navigationItem.leftBarButtonItem = self.editButtonItem

		// Add + button as right bar button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed(_:)))
		
		// Make sure the bottom toolbar is visible
		self.navigationController?.isToolbarHidden = false
		
		// From original Collection View Controller
		if (SharedResourceManager.sharedInstance.checkIfAppOnline() == true)
		{
			populateReportsView()
		}
			
		else
		{
			let halfWidth:CGFloat = UIScreen.main.bounds.width * CGFloat(0.5) - CGFloat(200)
			let halfHeight:CGFloat = UIScreen.main.bounds.height * CGFloat(0.5)
			let message:UILabel = UILabel(frame: CGRect(x:halfWidth, y:halfHeight, width:400, height:45))
			
			message.textAlignment =  .center
			message.text = "Please connect to WiFi to access your reports"
			self.view.addSubview(message)
		}
		
		
		
		
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return patientNames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PatientManagerTableViewCell

		cell.patientName.text = patientNames[indexPath.row]
		cell.userImage.image = patientImages[indexPath.row]
        
        // Configure the cell...

        return cell
    }
    

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		// Hide the view for now
		self.dismiss(animated: true, completion: nil)
	}
	
	
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	
	// MARK: - Original Collection View Controller Functions
	// (Adapted slightly for tableview)
	
	override func viewDidAppear(_ animated: Bool)
	{
		self.patientList = SharedResourceManager.sharedInstance.patientDataController.patientsList
		SharedResourceManager.sharedInstance.patientOtherInputs.resetAll()
		self.tableView?.reloadData()
	}

	
	func generatePatientNames()
	{
		var unsortedPatients:[String] = [String]()
		
		for patient in self.patientList
		{
			unsortedPatients.append(patient.patientName)
			self.patientImages.append(#imageLiteral(resourceName: "UserIcon"))
		}
		
		self.patientNames = unsortedPatients.sorted(by: <)
		
	}
	
	
	func updateQuestionsAndChoices()
	{
		SharedResourceManager.sharedInstance.questionController.getQuestions()
		
		SharedResourceManager.sharedInstance.questionController.getGenders()
		SharedResourceManager.sharedInstance.questionController.getGenomicAlterations()
		SharedResourceManager.sharedInstance.questionController.getOtherConditions()
		SharedResourceManager.sharedInstance.questionController.getMedications()
		SharedResourceManager.sharedInstance.questionController.getAllergies()
		SharedResourceManager.sharedInstance.questionController.getHealthInsurers()
		SharedResourceManager.sharedInstance.questionController.getEthnicities()
		
		SharedResourceManager.sharedInstance.questionController.resetQuestionNumbers()
	}
	
	func populateReportsView()
	{
		self.updateQuestionsAndChoices()
		
		if (self.patientNames.count == 0)
		{
			VCOverlayController.show(self.view, loadingText: "Loading your reports...")
		}
		
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfPatientsFromServerWithDelay(){patientData -> () in
			self.patientList = patientData
			self.generatePatientNames()
			
			DispatchQueue.main.async{
				self.tableView.reloadData()
				VCOverlayController.hide()
			}
		}
	}

	func showNamePatientAlert(){
		
		// Create the elements in the alert
		var projectNameField: UITextField?
		let nameProjectAlert = UIAlertController(title: "Patient Name", message: nil, preferredStyle: UIAlertControllerStyle.alert)
		nameProjectAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
		nameProjectAlert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { (action) -> Void in
			
			// Set the default name of a alert
			if(projectNameField?.text == nil){
				projectNameField?.text = "New Patient"
			}
			// ************************************************************************************
			// TODO Create a new patient then go to the donut view
			// ************************************************************************************
		}))
		
		// Add the elements to the alert
		nameProjectAlert.addTextField(configurationHandler: {(textField: UITextField!) in
			textField.placeholder = "New Patient"
			projectNameField = textField
		})
		
		// Show the alert
		present(nameProjectAlert, animated: true, completion: nil)
		
	}

}
