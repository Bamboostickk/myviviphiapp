//
//  VCEditAge.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-01.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

/*****************************************************************/
/*****************************************************************/
/*****************************************************************/
/***************************DEPRECIATED***************************/
/*****************************************************************/
/*****************************************************************/
/*****************************************************************/

import UIKit

class VCEditAge : UIViewController
{
	@IBOutlet var theNavigationBar: UINavigationBar!
	@IBOutlet var textLabel: UILabel!
	
	//@IBOutlet weak var currentAge: UILabel!
	
	@IBOutlet var ageValue: UITextField!
	
	var patientAge:Int = 0
	
	// Constraint age to 0 - 99
	
	@IBAction func finishedSettingAge(_ sender: AnyObject)
	{
		
		if (ageValue.text != "")
		{
			self.patientAge = Int(ageValue.text!)!
			
			if ((self.patientAge >= 0) && (self.patientAge < 100))
			{				
				self.customChoice(userInput: String(describing: self.patientAge))
				let patientIDValue = SharedResourceManager.sharedInstance.patientID
				SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
					DispatchQueue.main.async{
						self.dismiss(animated: true, completion: nil)
					}
				}
			}
				
			else
			{
				displayErrorMessage(theTitle: "Invalid Age", theMessage: "Age should be between 0 and 99")
			}
			
		}
			
		else
		{
			displayErrorMessage(theTitle: "Nothing Entered", theMessage: "Please enter your age")
		}
	}
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	func dismissKeyboard()
	{
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
	
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		
		
		
		super.viewDidLoad()
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()

		self.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName
		theNavigationBar.topItem?.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName
		self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText
		self.ageValue.text = SharedResourceManager.sharedInstance.patientDataController.patientAge?[0].customText
		
		self.ageValue.becomeFirstResponder()

		
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VCQuestionAge.dismissKeyboard))
		view.addGestureRecognizer(tap)
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	// Displays error message if nothing is entered
	
	func displayErrorMessage(theTitle:String, theMessage:String)
	{
		let alertView = UIAlertController(title: theTitle, message: theMessage as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}

}
