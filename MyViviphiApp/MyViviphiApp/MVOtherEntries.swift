//
//  MVOtherEntries.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-14.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

class MVOtherEntries
{
	var cancer:String = ""
	var histology:String = ""
	var stage:String = ""
	var tumourMarkers:String = ""
	var geneticAlterations:String = ""
	var otherConditions:String = ""
	var otherMedications:String = ""
	var allergies:String = ""
	var ethnicity:String = ""
	var healthInsurer:String = ""

	var lastEntry:String = ""
	
	func resetAll()
	{
		self.cancer = ""
		self.histology = ""
		self.stage = ""
		self.tumourMarkers = ""
		self.geneticAlterations = ""
		self.otherConditions = ""
		self.otherMedications = ""
		self.allergies = ""
		self.ethnicity = ""
		self.healthInsurer = ""
		
		self.lastEntry = ""
	}
	
	func clearLast()
	{
		self.lastEntry = ""
	}
}
