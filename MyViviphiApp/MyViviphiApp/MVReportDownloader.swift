//
//  MVReportDownloader.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-13.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

class MVReportDownloader: NSObject, URLSessionDataDelegate
{
	var buffer:NSMutableData = NSMutableData()
	var percentDownloaded:Float = 0.0
	
	func retrievePDFForPatient(patientID:String, onFinish:@escaping (URL) -> Void)
	{
		let token = String(describing:UserDefaults.standard.value(forKey: "accessToken")!)
		let headers:Dictionary = ["Authorization": token, "content-type": "application/x-www-form-urlencoded"]
		
		let theURL:String = SharedResourceManager.sharedInstance.URL_PATH + "api/Report/" +  patientID
		
		let fileURL:URL = URL(string: theURL)!
		
		let container = HTTPCookieStorage.shared
		let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers, for: fileURL)
		container.setCookies(cookies, for: fileURL, mainDocumentURL: fileURL)
		
		var request = URLRequest(url: fileURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 100.0)
		request.allHTTPHeaderFields = headers
		request.httpMethod = "GET"
		
		let session = URLSession(configuration: URLSessionConfiguration.ephemeral, delegate: self, delegateQueue: OperationQueue.main)
		
		let task = session.downloadTask(with: fileURL)
		
		/*
		let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
			
			var documentURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last
			documentURL = documentURL?.appendingPathComponent("Report.pdf")
			
			do
			{
				try data?.write(to: documentURL!, options: .atomic)
				//onFinish(documentURL!)
			}
			catch
			{
				print(error)
			}
		})*/
		
		task.resume()
	}
	
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64)
	{
		percentDownloaded = (Float(writ)/Float(exp)) * 100
		print("Progress: " + String(describing: percentDownloaded))
	}
	
	func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
	{
		
	}

	
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL)
	{
		var documentURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last
		documentURL = documentURL?.appendingPathComponent("Report.pdf")
		
		let data = location.dataRepresentation
		
		print("DONE")
		
		do
		{
			try data.write(to: documentURL!, options: .atomic)
		}
			
		catch
		{
			print(error)
		}
	}
	
}
