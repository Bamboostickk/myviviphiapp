//
//  VCNewsLandingViewController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-21.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCNewsLandingViewController: UITableViewController
{
	var articleObjects:[MVNewsArticle] = []
	
	//var articleTitles = ["New Cancer Study shows things are getting better", "Here's another news article with a title!"]
	//var articleDates = ["October 4, 2016", "September 23,2016"]
	//var articleImages = [#imageLiteral(resourceName: "NewsImageExample"),#imageLiteral(resourceName: "NewsImageExample")]
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		populateNewsTable()
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		self.view.setNeedsDisplay()
	}
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int
	{
		// #warning Incomplete implementation, return the number of sections
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		// #warning Incomplete implementation, return the number of rows
		return articleObjects.count
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyNewsTableViewCell
		
		cell.articleTitle.text = articleObjects[indexPath.row].articleTitle
		//cell.articleImage.image = articleObjects[indexPath.row]
		cell.articleDate.text = articleObjects[indexPath.row].articleDate
		
		cell.articleImage.layer.cornerRadius = 6
		cell.articleImage.layer.masksToBounds = true
		
		
		return cell
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		let link:String = articleObjects[indexPath.row].articleLink
		
		if let resultController = storyboard!.instantiateViewController(withIdentifier: "NewsDetail") as? VCNewsDetailController {
			resultController.articlePath = link
			self.navigationController?.pushViewController(resultController, animated: true)
		}
		
		self.tableView.deselectRow(at: indexPath, animated: true)
	}
	
	func populateNewsTable()
	{
		if (self.articleObjects.count == 0)
		{
			VCOverlayController.show(self.view, loadingText: "Loading your reports...")
		}
		
		SharedResourceManager.sharedInstance.newsController.getListOfArticles(){listOfArticles -> () in
			self.articleObjects = listOfArticles
			DispatchQueue.main.async{
				self.tableView?.reloadData()
				VCOverlayController.hide()
			}
		}
	}
}
