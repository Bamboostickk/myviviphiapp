//
//  FilledButton.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-12-02.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

class FilledButton: UIButton {
    
    
    //var outlineColor:CGColor = UIColor(colorLiteralRed: 221/255, green: 221/255, blue: 221/255, alpha: 1.0).cgColor
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setTitleColor(UIColor.white, for: .normal)
        
        //layer.borderWidth = 2.0
        //layer.borderColor = outlineColor
        
        layer.cornerRadius = 9.0
        clipsToBounds = true
        contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        //layer.backgroundColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 1.0).cgColor
        
        
        
    }
    
    override func tintColorDidChange() {
        layer.backgroundColor = self.tintColor.cgColor
    }

}
