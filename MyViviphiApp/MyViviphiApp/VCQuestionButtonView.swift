//
//  VCQuestionButtonView.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-15.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCQuestionButtonView:  UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
	
	//@IBOutlet weak var theNavigationBar: UINavigationBar!
	@IBOutlet weak var textLabel: UILabel!
	
	@IBOutlet weak var collectionView: UICollectionView!
	
	//@IBOutlet weak var nextButton: UIBarButtonItem!

	
	let viewControllerIdentifier:String? = ""
	
	var questionsList:[String] = [String]()
	var questionDictionary:[Int:String] = [:]
	
	var hasCheckController:Bool? = false
	var allowsMultipleSelection:Bool? = false
	
	var selectedList:[Bool] = [Bool]()
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber > 1)
		{
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 1)
		{
			SharedResourceManager.sharedInstance.questionController.resetAll()
		}
		
		self.dismiss(animated: true, completion: nil)
	}
	/*
	@IBAction func saveButtonPressed(_ sender: AnyObject)
	{
		
		if(self.allowsMultipleSelection == true)
		{
			if (self.checkIfAnythingSelected() == true)
			{
				//self.writeChoicesSelected()
				self.loadNextScreen()
			}
				
			else
			{
				self.displayErrorMessage()
			}
		}
		else
		{
			self.loadNextScreen()
		}
	}
*/
	
	// Do any additional setup after loading the view, typically from a nib.
	
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		collectionView.allowsMultipleSelection = true
		SharedResourceManager.sharedInstance.questionController.activeQuestionNumber += 1
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		
//		self.collectionView.delegate = self
//		self.collectionView.dataSource = self
//		self.collectionView.reloadData()
		
		//self.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		//theNavigationBar.topItem?.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionSubtext!
		
		if let hasMultipleSupport = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionSupportsMultipleAnswers
		{
			self.allowsMultipleSelection = hasMultipleSupport
		}
		
		/*
		if (self.allowsMultipleSelection == false)
		{
			nextButton = UIBarButtonItem(title: "Next", style: .done, target: self, action: #selector(self.saveButtonPressed(_:)))
			self.navigationItem.rightBarButtonItem = nextButton
			//nextButton.isEnabled = false
		}
		*/
		
		setupPageForQuestionType()
	}
	
	
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return questionsList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QuestionCollectionViewCell
		
		cell.label.text = questionsList[indexPath.row]
		cell.label.textColor = self.view.tintColor
		
		return cell
	}
	
	// MARK: - UICollectionViewDelegate protocol
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		// handle tap events
		//print("You selected cell #\(indexPath.item)!")
		let cell = collectionView.cellForItem(at: indexPath)
		cell?.isSelected = true
		buttonAction(cell:cell!)
	}
	
	
	// Probably not needed
	
	func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
	{
		// handle tap events
		print("You deselected cell #\(indexPath.item)!")
		let cell = collectionView.cellForItem(at: indexPath)
		cell?.isSelected = false
	}
	
	
	func setupPageForQuestionType()
	{
		let question:String = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
			
		switch question
		{
		case "Gender":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfGenders!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfGenders!
		case "Ethnicity":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfEthnicities!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfEthnicities!
		case "Health Insurer":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfHealthInsurers!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfHealthInsurers!
		default:
			break
		}
		
		//compareQuestionWithSelection()
		//addButtons(listOfOptions: questionsList)
	}
	
	// Determines what kind of view controller to load next based on the question ID
	// Only useful if the questions are in a particular order.
	// We can write an algorithm that forces a particualer scene to load based on what the question itself is
	// Needs to be smarter, i.e. checks for question name
	
	func loadNextScreen()
	{
		if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 2)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AgeView") as? VCQuestionAge {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 11)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "PostalCodeView") as? VCQuestionPostalCode {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
			// If the next question ID is negative, the end of the questionnaire is reached
			
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID  == -1)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "QuestionnaireFinished") as? VCQuestionnaireFinished {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonView") as? VCQuestionButtonView {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
	}
	
	// Actions to carry out when a button is pressed
	// When a button is pressed, grabs the button's title and then updates the questionnaire
	// Either by loading a new scene, or grabbing new data and then loading the new scene
	
	func buttonAction(cell:UICollectionViewCell)
	{
		let selectedCell:QuestionCollectionViewCell = cell as! QuestionCollectionViewCell
		
		let buttonValue:String = selectedCell.label.text!
		
		if (buttonValue == "Other")
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditOtherView") as? VCEditOtherView {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else
		{
			let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
			let selectedKeyFromArray = keyForValueArray[0]
			updateQuestionnaireFromSelection(selectedAnswer: selectedKeyFromArray)
			self.loadNextScreen()
		}
		
		self.writeChoice(buttonValue: buttonValue, willClear: false)
		
		//		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
	}
	
	// If the current question affects the next question, update the options for the next question
	// Otherwise it loads the next button view controller
	
	func updateQuestionnaireFromSelection(selectedAnswer:Int)
	{
		let question:String = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		
		switch question
		{
		case "Gender":
			SharedResourceManager.sharedInstance.questionController.genderID = selectedAnswer
			self.updateCancers(withGenderID: selectedAnswer)
		default:
			break
		}
	}
	
	func updateCancers(withGenderID:Int)
	{
		SharedResourceManager.sharedInstance.questionController.getCancersForGender(genderID: withGenderID){() -> () in
			
		}
	}
	
	// Displays error message if nothing is selected
	
	func displayErrorMessage()
	{
		let alertView = UIAlertController(title: "Nothing selected", message: "Please make a selection" as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	
	// Method only to be used if multiple selections are allowed. Checks if anything is selected
	
	func checkIfAnythingSelected() -> Bool
	{
		for value in self.selectedList
		{
			if (value == true)
			{
				return true
			}
		}
		
		return false
	}
	
	// Method only to be used if multiple selections are allowed. Writes selected choices to an array and sends it to the patient data instance
	
	func writeChoicesSelected()
	{
		var choicesArray:[String] = [String]()
		
		if (self.selectedList.count > 0)
		{
			for index in 0...(self.selectedList.count - 1)
			{
				if (self.selectedList[index] == true)
				{
					choicesArray.append(self.questionsList[index])
				}
			}
		}
		
		//let questionName:String = SharedResourceManager.sharedInstance.questionController.makePatientDataKeyFromQuestion(question: SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!)
		//SharedResourceManager.sharedInstance.activeReportStorage.updateInformationDictionary(key: questionName, value: choicesArray as AnyObject)
	}
	
	func writeChoice(buttonValue:String, willClear:Bool)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: willClear)
		
	}
	
	func removeChoice(buttonValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	func deselectCustomChoice()
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: "")
	}
	
}
