//
//  PatientManagerTableViewCell.swift
//  MyViviphiApp
//
//  Created by Sasha Ivanov on 2016-12-20.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class PatientManagerTableViewCell: UITableViewCell {

	@IBOutlet weak var userImage: UIImageView!
	@IBOutlet weak var patientName: UILabel!
	@IBOutlet weak var patientPlan: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
