//
//  MVServerCommunicationController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-01.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

class MVServerCommunicationController
{
	var attributeArray:Array? = []	// Empty array to return if login fails
	var buffer:NSMutableData = NSMutableData()
	
	func createURLRequest(theURL:String) -> URLRequest
	{
		var request = URLRequest(url: URL(string: theURL)!)
		request.addValue(UserDefaults.standard.value(forKey: "accessToken") as! String, forHTTPHeaderField: "Authorization")
		request.httpMethod = "GET"
		
		return request
	}
	
	
	func getJSONFromServer(request:URLRequest, onFinish:@escaping ([[String:AnyObject]]) -> Void)
	{
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)

				if (httpResponse.statusCode == 200)
				{
					do
					{
						if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String:AnyObject]]
						{
							onFinish(convertedJsonIntoDict)
						}
					}
						
					catch let error as NSError
					{
						print(error.localizedDescription)
					}
				}
				
				//If a request object could not be found
					
				else if (httpResponse.statusCode == 404)
				{
					let emptyArray:[[String:AnyObject]] = [[String:AnyObject]]()
					onFinish(emptyArray)
				}
			}
		}
		
		dataTask.resume()
	}
	
	func getBasicJSONFromServer(request:URLRequest, onFinish:@escaping ([String:AnyObject]) -> Void)
	{
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)

				if (httpResponse.statusCode == 200)
				{
					do
					{
						if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
						{
							//print(convertedJsonIntoDict)
							onFinish(convertedJsonIntoDict)
						}
					}
						
					catch let error as NSError
					{
						print(error.localizedDescription)
					}
				}
			}
		}
		
		dataTask.resume()
	}
	
	// Updates the local dictionary. The VCLoginScreenViewController can call this method after the connection is made.
	
	func updateLocalArray(theArray:Array<Any>)
	{
		attributeArray = theArray
	}
	
	func writeSelectToServer(thePatientID:String, theQuestionID:String, theCodeID:String, theLookUpType:String, willClear:Bool)
	{
		let token = String(describing:UserDefaults.standard.value(forKey: "accessToken")!)
		let headers:Dictionary = ["Authorization": token, "content-type": "application/x-www-form-urlencoded"]
		
		let patientID = "PatientId=" + thePatientID
		let postData:NSMutableData =  NSMutableData.init(data: patientID .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		let questionID = "&QuestionId=" + theQuestionID
		let codeID = "&CodeId=" + theCodeID
		let lookUpTypeID = "&LookUpTypeId=" + theLookUpType
		let clearOthers = "&ClearOtherAnswers=" + String(willClear)

		
		postData.append(questionID.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(codeID.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(lookUpTypeID.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(clearOthers.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Survey/Select"
		
		let fileURL = URL(string:urlString)

		let container = HTTPCookieStorage.shared
		let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers, for: fileURL!)
		container.setCookies(cookies, for: fileURL, mainDocumentURL: fileURL)
		
		var request = URLRequest(url: fileURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 100.0)
		request.allHTTPHeaderFields = headers
		request.httpMethod = "PUT"
		request.httpBody = postData as Data
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)
				
				if (httpResponse.statusCode == 200)
				{
					print("WRITE SUCEEDED")
				}
				
				else
				{
					print("WRITE FAILED " + String(describing: httpResponse.statusCode))
				}
			}
		}
		
		dataTask.resume()
	}
	
	func writeUnselectToServer(thePatientID:String, theQuestionID:String, theCodeID:String, theLookUpType:String)
	{
		let token = String(describing:UserDefaults.standard.value(forKey: "accessToken")!)
		let headers:Dictionary = ["Authorization": token, "content-type": "application/x-www-form-urlencoded"]
		
		let patientID = "PatientId=" + thePatientID
		let postData:NSMutableData =  NSMutableData.init(data: patientID .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		let questionID = "&QuestionId=" + theQuestionID
		let codeID = "&CodeId=" + theCodeID
		let lookUpTypeID = "&LookUpTypeId=" + theLookUpType
		
		postData.append(questionID.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(codeID.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(lookUpTypeID.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Survey/UnSelect"
		
		let fileURL = URL(string:urlString) //Select will be used eventually
		
		let container = HTTPCookieStorage.shared
		let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers, for: fileURL!)
		container.setCookies(cookies, for: fileURL, mainDocumentURL: fileURL)
		
		var request = URLRequest(url: fileURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 100.0)
		request.allHTTPHeaderFields = headers
		request.httpMethod = "PUT"
		request.httpBody = postData as Data
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)
				
				if (httpResponse.statusCode == 200)
				{
					print("WRITE REMOVE")
				}
					
				else
				{
					print("REMOVE FAILED " + String(describing: httpResponse.statusCode))
				}
			}
		}
		
		dataTask.resume()
	}
	
	func writeCustomTextToServer(thePatientID:String, theQuestionID:String, theLookUpType:String, theAnswerText:String)
	{
		let token = String(describing:UserDefaults.standard.value(forKey: "accessToken")!)
		let headers:Dictionary = ["Authorization": token, "content-type": "application/x-www-form-urlencoded"]
		
		let patientID = "PatientId=" + thePatientID
		let postData:NSMutableData =  NSMutableData.init(data: patientID .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		let questionID = "&QuestionId=" + theQuestionID
		let lookUpTypeID = "&LookUpTypeId=" + theLookUpType
		let answerType = "&AnswerText=" + theAnswerText
		
		postData.append(questionID.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(lookUpTypeID.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(answerType.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Survey/CustomText"
		
		let fileURL = URL(string:urlString) //Select will be used eventually
		
		let container = HTTPCookieStorage.shared
		let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers, for: fileURL!)
		container.setCookies(cookies, for: fileURL, mainDocumentURL: fileURL)
		
		var request = URLRequest(url: fileURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 100.0)
		request.allHTTPHeaderFields = headers
		request.httpMethod = "PUT"
		request.httpBody = postData as Data
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)
				
				if (httpResponse.statusCode == 200)
				{
					//print("CUSTOM WRITE")
				}
			}
		}
		
		dataTask.resume()
	}

	func createNewPatient(patientNamed:String, isForSelf:Bool, onFinish:@escaping (Int) -> Void)
	{
		let token = String(describing:UserDefaults.standard.value(forKey: "accessToken")!)
		let headers:Dictionary = ["Authorization": token, "content-type": "application/x-www-form-urlencoded"]
		
		let patientName = "Name=" + patientNamed
		let postData:NSMutableData =  NSMutableData.init(data: patientName .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		let forSelf = "&forSelf=" + String(isForSelf)
		
		postData.append(forSelf.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Patient"
		
		let fileURL = URL(string:urlString)
		
		let container = HTTPCookieStorage.shared
		let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers, for: fileURL!)
		container.setCookies(cookies, for: fileURL, mainDocumentURL: fileURL)
		
		var request = URLRequest(url: fileURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 100.0)
		request.allHTTPHeaderFields = headers
		request.httpMethod = "POST"
		request.httpBody = postData as Data
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)
				
				if (httpResponse.statusCode == 201)
				{
					do
					{
						if let patientIDString = String(data:data!, encoding: .utf8)
						{
							let patientID = Int(patientIDString)
							onFinish(patientID!)
						}
					}
				}
					
				else
				{
					print("Failed with code " + String(describing: httpResponse.statusCode))
				}
			}
		}
		
		dataTask.resume()

	}
	
	func retrievePDFForPatient(patientID:String, onFinish:@escaping (URL) -> Void)
	{
		let token = String(describing:UserDefaults.standard.value(forKey: "accessToken")!)
		let headers:Dictionary = ["Authorization": token, "content-type": "application/x-www-form-urlencoded"]
		
		let theURL:String =  SharedResourceManager.sharedInstance.URL_PATH + "api/Report/" +  patientID
		
		let fileURL:URL = URL(string: theURL)!
		
		let container = HTTPCookieStorage.shared
		let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers, for: fileURL)
		container.setCookies(cookies, for: fileURL, mainDocumentURL: fileURL)
		
		var request = URLRequest(url: fileURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 100.0)
		request.allHTTPHeaderFields = headers
		request.httpMethod = "GET"
		
		let session = URLSession.shared
		
		let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
			
			var documentURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last
			documentURL = documentURL?.appendingPathComponent("Report.pdf")
						
			do
			{
				try data?.write(to: documentURL!, options: .atomic)
				onFinish(documentURL!)
			}
			catch
			{
				print(error)
			}
		})
		
		task.resume()
	}
}
