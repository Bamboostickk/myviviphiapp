//
//  VCQuestionChecklistView.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-19.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCQuestionChecklistView:UITableViewController
{
	
	var isEditingQuestion = false
	var nextButtonText = "Next" // Set what text should be used in the top right corner
	
	
	var questionList:[String] = [String]()
	var tooltipList:[String:String] = [:]
	var questionDictionary:[Int:String] = [:]
	
	var answerObjects:[MVPatientAnswers] = [MVPatientAnswers]()
	var storedStartingObjects:[MVPatientAnswers] = [MVPatientAnswers]()
	var tableEntries:[MVTableEntry] = [MVTableEntry]()
	
	var selectedDictionary:[Int:Bool] = [:]
	var selectedStringValues:[String] = [String]()
	var selectedList:[Bool] = [Bool]()
	
	var allowsMultipleSelection:Bool = false
	
	// This is needed to adjust the background of the status bar for the table view
	var viewCover:UIView!
	
	//let selectedColour:UIColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 0.25)	// light purple
	let selectedColour:UIColor = UIColor(colorLiteralRed: 200/255, green: 199/255, blue: 204/255, alpha: 1.0)	// light gray
	
	var titles = ["Option 1","Option 2","Option 3","Option 4","Option 5","Option 6","Option 7","Option 8","Option 9","Option 10","Option 11","Option 12"]
	var subtitles = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta."]
	
	var headerText = "Select an Option"
	
	@IBOutlet weak var navigationBar: UINavigationItem!
	
	override func willMove(toParentViewController parent: UIViewController?)
	{
		super.willMove(toParentViewController: parent)
		
		if parent == nil
		{
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		}
	}
	
	@IBAction func nextButtonPressed(_ sender: AnyObject)
	{
		//self.writeChoicesSelected()
		
		let patientIDValue = SharedResourceManager.sharedInstance.patientID
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
			DispatchQueue.main.async{
				self.loadNextScreen()
			}
		}
	}
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func cancelButtonPressed(_ sender: AnyObject)
	{
		if (self.storedStartingObjects.count > 0)
		{
			self.writeOriginalValuesOnCancel()
		}
		
		self.dismiss(animated: true, completion: nil)
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		// Make the navigation bar white so the list doesn't scroll into it!
		viewCover = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: UIApplication.shared.statusBarFrame.height))
		viewCover.backgroundColor = UIColor.white
		self.navigationController?.view.addSubview(viewCover)
		self.navigationController?.navigationBar.backgroundColor = UIColor.white
		
		self.tableView.reloadData()
		
	}
	
	
	override func viewWillDisappear(_ animated: Bool)
	{
		// Make the navigation bar transparent again when we leave the view
		viewCover.removeFromSuperview()
		self.navigationController?.navigationBar.backgroundColor = TransparentNavigationController().navigationBar.backgroundColor
		self.navigationController?.setToolbarHidden(true, animated: true)
	}
	
	
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		// If we're just updating the question from MyHealth
		if isEditingQuestion
		{
			
			nextButtonText = "Done"
			
			// Add Skip button by default
			self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.nextButtonPressed(_:)))
			
			// Add Cancel button to the left since we are in edit mode
			self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelButtonPressed(_:)))
			
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			
			self.obtainChoicesListForEditing()
			self.initialiseSelectionArray()
			self.determineCheckedEntries()
		}
			
			// If we're in the questionnaire
		else
		{
			// Add Skip button by default
			self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.nextButtonPressed(_:)))
			
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber += 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			
			self.obtainChoicesList()
			self.initialiseSelectionArray()
		}
		
		// Some questions should only allow single selections
		if (SharedResourceManager.sharedInstance.questionController.activeQuestionID == 13)
		{
			self.tableView.allowsMultipleSelection = false
		}
			
		else
		{
			self.tableView.allowsMultipleSelection = true
		}
		
		self.allowsMultipleSelection = self.tableView.allowsMultipleSelection
		
		
		// Generate header
		
		let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 120))
		headerLabel.layoutMargins = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
		
		headerLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText
		headerLabel.textAlignment = .center
		headerLabel.font = UIFont.systemFont(ofSize: 28, weight: UIFontWeightLight)
		headerLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
		headerLabel.numberOfLines = 0
		headerLabel.adjustsFontSizeToFitWidth = true
		
		
		self.tableView.tableHeaderView = headerLabel
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(true)
		self.tableView.reloadData()
	}
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int
	{
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return max(tableEntries.count, 1) //tableEntries.count //max(tableEntries.count, subtitles.count)
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		self.selectedStringValues = SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.retrieveValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
		
		if (self.questionDictionary.count > 0)
		{
			cell.textLabel?.text = tableEntries[indexPath.row].description
			
			//cell.detailTextLabel?.text = subtitles[indexPath.row]
			
			if (self.isEditingQuestion == true)
			{
				if (tableEntries[indexPath.row].isSelected == true)
				{
					self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .middle)
					cell.accessoryType = .checkmark
					cell.backgroundColor = selectedColour
					//cell.isSelected = true
				}
					
				else
				{
					cell.accessoryType = .none
					//cell.isSelected = false
				}
			}
				
			else
			{
				if (self.selectedStringValues.contains((cell.textLabel?.text)!))
				{
					self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .middle)
					cell.accessoryType = .checkmark
					cell.backgroundColor = selectedColour
					cell.isSelected = true
				}
					
				else
				{
					cell.accessoryType = .none
					cell.isSelected = false
				}
			}
			
		}
			
		else
		{
			cell.textLabel?.text = "No options to select"
			cell.isUserInteractionEnabled = false
		}
		
		cell.textLabel?.backgroundColor = UIColor.clear
		cell.detailTextLabel?.backgroundColor = UIColor.clear
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return 60
	}
	
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		
		// Switch Skip with Next button if the list is empty right before selection
		if !selectedList.contains(true)
		{
			let nextButton = UIBarButtonItem(title: nextButtonText, style: .done, target: self, action: #selector(self.nextButtonPressed(_:)))
			self.navigationItem.setRightBarButton(nil, animated: true)
			self.navigationItem.setRightBarButton(nextButton, animated: true)
		}
		
		let cell = self.tableView.cellForRow(at: indexPath)
		cell?.accessoryType = .checkmark
		
		// Animate the tapped cell's background color on tap
		UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseOut], animations: {
			cell?.backgroundColor = self.selectedColour
		}, completion: { (finished: Bool) in })
		
		
		cell?.textLabel?.backgroundColor = UIColor.clear
		cell?.detailTextLabel?.backgroundColor = UIColor.clear
		
		self.tableEntries[indexPath.row].isSelected = true
		
		if (self.allowsMultipleSelection == true)
		{
			if (self.tableEntries[indexPath.row].description == "None")
			{
				self.deselectCells(exceptCellNamed: "None")
				self.writeChoice(rowValue: self.tableEntries[indexPath.row].description, clearState: true)
			}
				
			else if (self.tableEntries[indexPath.row].description == "Other")
			{
				self.deselectNone()
				self.selectedStringValues.append((self.tableView.cellForRow(at: indexPath)?.textLabel?.text)!)
				self.customTextField(indexPath: indexPath)
			}
				
			else
			{
				self.deselectNone()
				self.selectedStringValues.append((self.tableView.cellForRow(at: indexPath)?.textLabel?.text)!)
				self.writeChoice(rowValue: self.tableEntries[indexPath.row].description, clearState: false)
			}
		}
			
		else
		{
			self.writeChoice(rowValue: self.tableEntries[indexPath.row].description, clearState: false)
			self.selectedStringValues.removeAll()
			self.selectedStringValues.append((self.tableView.cellForRow(at: indexPath)?.textLabel?.text)!)
			//self.loadNextScreen()
		}
		
		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: self.selectedStringValues)
	}
	
	
	override func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
		
		// Switch Next button with Skip button if the list is empty right after deselection
		if !selectedList.contains(true){
			let nextButton = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.nextButtonPressed(_:)))
			self.navigationItem.setRightBarButton(nil, animated: true)
			self.navigationItem.setRightBarButton(nextButton, animated: true)
		}
		
		return indexPath
		
	}
	
	override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
	{
		self.tableEntries[indexPath.row].isSelected = false
		self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
		self.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.clear
		
		if let indexToRemove = self.selectedStringValues.index(of: (self.tableView.cellForRow(at: indexPath)?.textLabel?.text)!)
		{
			self.selectedStringValues.remove(at: indexToRemove)
		}
		
		self.removeChoice(rowValue: self.tableEntries[indexPath.row].description)
		
		
		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: self.selectedStringValues)
	}
	
	func deselectNone()
	{
		for indexPath in self.tableView.indexPathsForSelectedRows!
		{
			let cell = self.tableView.cellForRow(at: indexPath)
			
			if (cell?.textLabel?.text! == "None")
			{
				self.tableView.deselectRow(at: indexPath, animated: false)
				cell?.accessoryType = .none
				cell?.backgroundColor = UIColor.clear
				self.removeChoice(rowValue: (cell?.textLabel?.text!)!)
			}
		}
		
		if let indexNone = self.selectedStringValues.index(of: "None")
		{
			self.selectedStringValues.remove(at: indexNone)
		}
	}
	
	func deselectCells(exceptCellNamed:String)
	{
		for indexPath in self.tableView.indexPathsForSelectedRows!
		{
			let cell = self.tableView.cellForRow(at: indexPath)
			
			if (cell?.textLabel?.text! != exceptCellNamed)
			{
				self.tableView.deselectRow(at: indexPath, animated: false)
				cell?.accessoryType = .none
				cell?.backgroundColor = UIColor.clear
			}
		}
		
		self.selectedStringValues.removeAll()
		self.selectedStringValues.append(exceptCellNamed)
	}
	
	func obtainChoicesList()
	{
		if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 4)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfHistologies!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfHistologies!
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 8)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 9)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfMedications!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMedications!
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 10)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfAllergies!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAllergies!
		}
	}
	
	func obtainChoicesListForEditing()
	{
		if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 4)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfHistologies!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfHistologies!
			self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientHistology!
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 8)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!
			self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientOtherDiseases!
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 9)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfMedications!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMedications!
			self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientMedications!
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 10)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfAllergies!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAllergies!
			self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientAllergies!
		}
		
		if (self.questionList.count > 0)
		{
			for _ in 0...(self.questionList.count - 1)
			{
				self.selectedList.append(false)
			}
			
			self.compareQuestionWithSelection()
			
			for (key, _) in self.selectedDictionary
			{
				if (self.selectedDictionary[key] == true)
				{
					let value:String = self.questionDictionary[key]!
					let indexValue:Int = self.questionList.index(of: value)!
					self.selectedList[indexValue] = true
				}
			}
		}
		
		self.storedStartingObjects = answerObjects
		
		//self.tableView.reloadData()
	}
	
	func initialiseSelectionArray()
	{
		for choice in questionList
		{
			var choiceAttributes:MVTableEntry = MVTableEntry()
			choiceAttributes.codeID = self.questionDictionary.allKeys(forValue: choice)[0]
			choiceAttributes.description = choice
			choiceAttributes.isSelected = false
			
			tableEntries.append(choiceAttributes)
		}
	}
	
	func determineCheckedEntries()
	{
		var selectedAnswerIDs:[Int] = [Int]()
		
		
		if !selectedAnswerIDs.isEmpty{
			// Add Update the right button to Done/Next
			self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: nextButtonText, style: .done, target: self, action: #selector(self.nextButtonPressed(_:)))
		}
		
		
		for answer in self.answerObjects
		{
			selectedAnswerIDs.append(answer.codeID)
		}
		
		if(self.tableEntries.count > 0)
		{
			for index in 0...self.tableEntries.count - 1
			{
				if (selectedAnswerIDs.contains(self.tableEntries[index].codeID))
				{
					self.tableEntries[index].isSelected = true
				}
			}
		}
	}
	
	
	func writeChoicesSelected()
	{
		var choicesArray:[String] = [String]()
		
		if (self.selectedList.count > 0)
		{
			for index in 0...(self.selectedList.count - 1)
			{
				if (self.selectedList[index] == true)
				{
					choicesArray.append(self.questionList[index])
				}
			}
		}
		
		
		//let questionName:String = SharedResourceManager.sharedInstance.questionController.makePatientDataKeyFromQuestion(question: SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!)
		//SharedResourceManager.sharedInstance.activeReportStorage.updateInformationDictionary(key: questionName, value: choicesArray as AnyObject)
	}
	
	func writeOriginalValuesOnCancel()
	{
		print(self.storedStartingObjects)
		
		for answer in self.storedStartingObjects
		{
			var willClear:Bool = false
			
			if (answer.codeID == self.storedStartingObjects.first?.codeID)
			{
				willClear = true
			}
			
			if (answer.description == "None")
			{
				willClear = true
			}
			
			if (answer.description != "" && self.questionDictionary.count > 0)
			{
				self.writeChoice(rowValue: answer.description, clearState: false)
			}
		}
	}
	
	func writeChoice(rowValue:String, clearState:Bool)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: rowValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: clearState)
		
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	func removeChoice(rowValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: rowValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	func compareQuestionWithSelection()
	{
		var selectedArray:[Int] = [Int]()
		
		if (self.answerObjects.count > 0)
		{
			for index in (0...self.answerObjects.count - 1)
			{
				selectedArray.append(self.answerObjects[index].codeID)
			}
			
			for (key, _) in self.questionDictionary
			{
				
				if (selectedArray.contains(key))
				{
					self.selectedDictionary[key] = true
				}
					
				else
				{
					self.selectedDictionary[key] = false
				}
			}
		}
	}
	
	
	
	func loadNextScreen()
	{
		
		if isEditingQuestion
		{
			self.dismiss(animated: true, completion: nil)
		}
			
		else
		{
			
			if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 7 || SharedResourceManager.sharedInstance.questionController.nextQuestionID == 8 || SharedResourceManager.sharedInstance.questionController.nextQuestionID == 9)
			{
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView {
					self.navigationController?.pushViewController(resultController, animated: true)
				}
			}
				
			else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID  == -1)
			{
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "QuestionnaireFinished") as? VCQuestionnaireFinished {
					self.navigationController?.pushViewController(resultController, animated: true)
				}
			}
				
			else
			{
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
					self.navigationController?.pushViewController(resultController, animated: true)
				}
			}
		}
	}
	
	func customTextField(indexPath:IndexPath)
	{
		let myMessage:String = "Last value: " + self.setupTextField(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber)
		
		let alertView = UIAlertController(title: "Enter your data here", message: myMessage as String, preferredStyle: .alert)
		
		let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertView.textFields![0] as UITextField
			
			let textFieldInput:String = firstTextField.text!
			
			self.storeTextFieldValues(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber, userInput: textFieldInput)
			self.customChoice(userInput:textFieldInput)
			
			if (self.allowsMultipleSelection == false)
			{
				//self.dismiss(animated: true, completion: nil)
			}
			
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
			(action : UIAlertAction!) -> Void in
			self.tableView.deselectRow(at: indexPath, animated: false)
			self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
			self.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.clear
		})
		
		alertView.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = SharedResourceManager.sharedInstance.patientOtherInputs.lastEntry
		}
		
		alertView.addAction(saveAction)
		alertView.addAction(cancelAction)
		
		self.present(alertView, animated: true, completion: nil)
	}
	
	func storeTextFieldValues(questionNumber:Int, userInput:String)
	{
		switch questionNumber
		{
		case 3:
			SharedResourceManager.sharedInstance.patientOtherInputs.cancer = userInput
		case 4:
			SharedResourceManager.sharedInstance.patientOtherInputs.histology = userInput
		case 5:
			SharedResourceManager.sharedInstance.patientOtherInputs.stage = userInput
		case 6:
			SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers = userInput
		case 7:
			SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations = userInput
		case 8:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions = userInput
		case 9:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications = userInput
		case 10:
			SharedResourceManager.sharedInstance.patientOtherInputs.allergies = userInput
		case 11:
			SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity = userInput
		case 13:
			SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer = userInput
		default:
			break
		}
	}
	
	func setupTextField(questionNumber:Int) -> String
	{
		var theString = "Nothing selected"
		
		switch questionNumber
		{
		case 3:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.cancer
		case 4:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.histology
		case 5:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.stage
		case 6:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers
		case 7:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations
		case 8:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions
		case 9:
			theString  = SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications
		case 10:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.allergies
		case 11:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity
		case 13:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer
		default:
			theString = "Nothing selected"
			break
		}
		
		return theString
	}
}
