//
//  MVQuestion.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-14.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

struct MVQuestion
{
	var questionID:Int? = 0
	var questionNumber:Int? = 0
	var questionName:String? = ""
	var questionText:String? = ""
	var questionSubtext:String? = ""
	var questionSupportsMultipleAnswers:Bool? = false
	var questionLookupType:Int = 0
	var questionSortOrder:Int = 0
}
