//
//  MVPatientAnswers.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-28.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

struct MVPatientAnswers
{
	var questionID:Int = 0
	var lookupTypeID:Int = 0
	var codeID:Int = 0
	var customText:String = ""
	var description:String = ""
}
