//
//  VCSurveyStartView.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-17.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import UIKit

class VCSurveyStartView:UIViewController,UITextFieldDelegate
{
	@IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var isPatientSwitch: UISwitch!
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		//SharedResourceManager.sharedInstance.patientDataController.resetDictionary()
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func createButtonPressed(_ sender: AnyObject)
	{
		if (self.nameField.text! != "")
		{
//			if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
//				self.navigationController?.pushViewController(resultController, animated: true)
//			}
			
			VCOverlayController.show(self.view, loadingText: "Preparing questions")

			self.createNewUser(name: self.nameField.text!, forSelf: true){ () -> () in
				DispatchQueue.main.async
					{
						if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
							self.navigationController?.pushViewController(resultController, animated: true)
							VCOverlayController.hide()
						}
				}
			}
        }
		
		else
		{
			self.displayErrorMessage()
		}
	}
	
	override func viewDidLoad()
	{
		
		super.viewDidLoad()
		nameField.delegate = self
		nameField.becomeFirstResponder()

		// Disable the create patient button for now
		self.navigationItem.rightBarButtonItem?.isEnabled = false
		
		// Shrink the isPatient Switch
		isPatientSwitch.transform = CGAffineTransform(scaleX: 2/3, y: 2/3)
		
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VCSurveyStartView.dismissKeyboard))
		view.addGestureRecognizer(tap)
		
		SharedResourceManager.sharedInstance.patientOtherInputs.resetAll()
		
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(true)
		
		SharedResourceManager.sharedInstance.questionController.resetAll()
		
		SharedResourceManager.sharedInstance.questionController.getQuestions()
		
		SharedResourceManager.sharedInstance.questionController.getGenders()
		SharedResourceManager.sharedInstance.questionController.getGenomicAlterations()
		SharedResourceManager.sharedInstance.questionController.getOtherConditions()
		SharedResourceManager.sharedInstance.questionController.getMedications()
		SharedResourceManager.sharedInstance.questionController.getAllergies()
		SharedResourceManager.sharedInstance.questionController.getHealthInsurers()
		SharedResourceManager.sharedInstance.questionController.getEthnicities()
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	// Allows the "Done" button to hide the keyboard
	func textFieldShouldReturn(_ textField: UITextField) -> Bool
	{
		textField.resignFirstResponder()
		return true
	}
	
	// Called when the characters are about to change in the textfield
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
	{
		// Get the new text string
		let newText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
		
		// If the new text is empty disable the create button
		if(newText?.isEmpty)!{
			self.navigationItem.rightBarButtonItem?.isEnabled = false
		}
		
		// Otherwise if the old text is empty (and the next text wasn't empty from the previous statement) enable the create button
		else if(nameField.text?.isEmpty)!{
			self.navigationItem.rightBarButtonItem?.isEnabled = true
		}
		
		// Always return true to make sure we allow the user to update the text they type
		return true
	}
	
	func createNewUser(name:String, forSelf:Bool, onFinish:@escaping (() -> Void))
	{
	SharedResourceManager.sharedInstance.serverRequestController.createNewPatient(patientNamed: name, isForSelf: forSelf)
		{userID -> () in
			SharedResourceManager.sharedInstance.patientID = userID
			onFinish()
		}
	
		
	}
	
	// Displays error message if nothing is entered
	
	func displayErrorMessage()
	{
		let alertView = UIAlertController(title: "Nothing entered", message: "Please enter your name" as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	func dismissKeyboard()
	{
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
}
