//
//  MVCancerDictionaryController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-09.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

class MVCancerDictionaryController
{
	var cancerDictionary:NSDictionary? = [:]
	
	func prepareDictionary()
	{
		if let bundlePath = Bundle.main.path(forResource: "NCICancerDictionary", ofType: "plist")
		{
			cancerDictionary = NSDictionary(contentsOfFile: bundlePath)
		}
	}
	
}
