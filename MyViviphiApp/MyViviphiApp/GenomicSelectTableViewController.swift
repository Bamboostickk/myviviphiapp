//
//  GenomicSelectTableViewController.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-12-05.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

class GenomicSelectTableViewController: UITableViewController, UISearchResultsUpdating {
	
	
	lazy var slideInTransitioningDelegate = SlideInPresentationManager()
	
	var questionList:[String] = [String]()
	var tooltipList:[String:String] = [:]
	var questionDictionary:[Int:String] = [:]
	
	var tableEntries:[MVTableEntry] = [MVTableEntry]()
	var filteredEntries:[MVTableEntry] = [MVTableEntry]()
	
	var selectedDictionary:[Int:Bool] = [:]
	var selectedList:[Bool] = [Bool]()
	var pairsList:[Int] = [Int]()
	let searchController = UISearchController(searchResultsController: nil)
	
	// This is needed to adjust the background of the status bar for the table view
	var viewCover:UIView!
	
	let sections = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	//var options = [["Alpha","Arm"],["Bee","Blizard","Boom"],["Car","Caring","Close"],["Dog"],["Ear","Elf","Emo"],["Fancy","Fun",],["Great"],["Hair","Holiday"],["Igloo"],["Jolly"],["Kite"],["Lamp","Love"],["Magic","Money","Moon",],["Nana","Noon"],[],["Papaya"],[],["Raw","Rent","Rooster"],[],[],[],[],[],[],[],[]]
	
	var selectionDictionary = ["A":[MVTableEntry](),"B":[MVTableEntry](), "C":[MVTableEntry](), "D":[MVTableEntry](), "E":[MVTableEntry](), "F":[MVTableEntry](), "G":[MVTableEntry](), "H":[MVTableEntry](), "I":[MVTableEntry](), "J":[MVTableEntry](), "K":[MVTableEntry](), "L":[MVTableEntry](), "M":[MVTableEntry](), "N":[MVTableEntry](), "O":[MVTableEntry](), "P":[MVTableEntry](), "Q":[MVTableEntry](), "R":[MVTableEntry](), "S":[MVTableEntry](), "T":[MVTableEntry](), "U":[MVTableEntry](), "V":[MVTableEntry](), "W":[MVTableEntry](), "X":[MVTableEntry](), "Y":[MVTableEntry](), "Z":[MVTableEntry]()]

	
	var filteredOptions:[String] = []
	
	
	@IBAction func doneButtonPressed(_ sender: AnyObject)
	{
		//self.writeChoicesSelected()
		self.loadNextView()
	}
	
	func initialiseDictionary()
	{
		for entry in self.tableEntries
		{
			let characterArrayFromString:[Character] = Array(entry.description.characters)
			let firstCharacter:String = String(describing:characterArrayFromString[0])
			self.selectionDictionary[firstCharacter]?.append(entry)
		}
	}
	
	func showSelectedAlterations(){
		//guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
		//statusBar.backgroundColor = UIColor.clear
		performSegue(withIdentifier: "showSelectedSegue", sender: self)
	}
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Setup the Search Controller
		searchController.searchResultsUpdater = self
		searchController.hidesNavigationBarDuringPresentation = false
		searchController.dimsBackgroundDuringPresentation = false
		searchController.searchBar.sizeToFit()
		searchController.searchBar.searchBarStyle = .minimal
		self.tableView.tableHeaderView = searchController.searchBar
		
		// Make the Section Index Selector on the right have transparent background
		self.tableView.sectionIndexBackgroundColor = UIColor.clear
		
		// Allow multiple options to be selected from the table view.
		self.tableView.allowsMultipleSelection = true
		
		// Show toolbar at the bottom of the screen
		self.navigationController?.setToolbarHidden(false, animated: false)
		let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
		let showSelectedButton = UIBarButtonItem(title: "Show Choices Selected", style: .plain, target: self, action: #selector(showSelectedAlterations))
		self.setToolbarItems([flexibleSpace, showSelectedButton, flexibleSpace], animated: true)
		
		
		let nextBarButton = UIBarButtonItem(title: "Next", style: .done, target: self, action: #selector(self.doneButtonPressed(_:)))
		self.navigationItem.rightBarButtonItem = nextBarButton
		
		
		/* Uncomment to add a UI View with Title at the top of the list
		//var headerText = "Select Your Genomic Alterations"
		let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 210))
		headerLabel.text = headerText
		headerLabel.textAlignment = .center
		headerLabel.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightLight)
		headerLabel.numberOfLines = 3
		
		
		self.tableView.tableHeaderView = headerLabel
		*/
		
		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false
		
		// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		// self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		self.tableView.allowsMultipleSelection = true
		self.obtainChoicesList()
		self.initialiseSelectionArray()
		self.determinePairsInChoicesList()
		self.initialiseDictionary()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		viewCover = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: UIApplication.shared.statusBarFrame.height))
		viewCover.backgroundColor = UIColor.white
		self.navigationController?.view.addSubview(viewCover)
		self.navigationController?.navigationBar.backgroundColor = UIColor.white
		
	}
	
	
	override func viewWillDisappear(_ animated: Bool) {
		viewCover.removeFromSuperview()
		self.navigationController?.navigationBar.backgroundColor = TransparentNavigationController().navigationBar.backgroundColor
		self.navigationController?.setToolbarHidden(true, animated: true)
	}
	
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		
		if searchController.isActive && searchController.searchBar.text != "" {
			return 1
		}
		return sections.count
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if searchController.isActive && searchController.searchBar.text != "" {
			return filteredOptions.count
		}
		
		return (selectionDictionary[sections[section]]?.count)!
	}
	
	override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
		return sections
	}
	
	//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
	//        return sections[section]
	//    }
	//
	//
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		
		if searchController.isActive && searchController.searchBar.text != "" {
			cell.textLabel?.text = filteredOptions[indexPath.row]
		}
			
		else
		{
			let theSectionLetter:String = sections[indexPath.section]
			let theCellItem = selectionDictionary[theSectionLetter]
			
			cell.textLabel?.text = theCellItem![indexPath.row].description
			
			if (indexPath.row < (selectionDictionary[theSectionLetter]?.count)!)
			{
				
			}
		}
		
		cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold)
		
		cell.accessoryType = cell.isSelected ? .checkmark : .none
		cell.selectionStyle = .none
		
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
	{
		let theSectionLetter:String = sections[section]
		return theSectionLetter
	}
	
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 60
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		if let cell = tableView.cellForRow(at: indexPath)
		{
			if ((searchController.isActive) && (searchController.searchBar.text != ""))
			{
				
				if (self.filteredEntries[indexPath.row].isSelected == true)
				{
					cell.accessoryType = .none
					self.filteredEntries[indexPath.row].isSelected = false
					let indexFromAllEntries:Int = self.tableEntries.index {$0.description == filteredEntries[indexPath.row].description}!
					tableEntries[indexFromAllEntries].isSelected = false
					self.removeChoice(rowValue: (cell.textLabel?.text)!)
				}
					
				else
				{
					cell.accessoryType = .checkmark
					self.filteredEntries[indexPath.row].isSelected = true
					let indexFromAllEntries:Int = self.tableEntries.index {$0.description == filteredEntries[indexPath.row].description}!
					tableEntries[indexFromAllEntries].isSelected = true
					self.writeChoice(rowValue: (cell.textLabel?.text)!)
				}
				
				// In progress, need updates
				
				if (self.filteredEntries[indexPath.row].isPair)
				{
					let optionText = self.filteredEntries[indexPath.row].description
					let lastCharacter = optionText[optionText.index(before: optionText.endIndex)]
					
					let indexOfPair:Int = self.tableEntries.index {$0.description == filteredEntries[indexPath.row].pairedWith}!
					tableEntries[indexOfPair].isSelected = false
					
					if (self.determineIfPairInFiltered(thePair:filteredEntries[indexPath.row].pairedWith))
					{
						if (lastCharacter == "+")
						{
							filteredEntries[indexPath.row + 1].isSelected = false
							
							let rowBelow:IndexPath = IndexPath.init(row: indexPath.row + 1, section: indexPath.section)
							self.tableView.deselectRow(at: rowBelow, animated: true)
							self.tableView.cellForRow(at: rowBelow)?.accessoryType = .none
							self.removeChoice(rowValue: (self.tableView.cellForRow(at: rowBelow)?.textLabel?.text)!)
						}
							
						else if (lastCharacter == "-")
						{
							
							filteredEntries[indexPath.row - 1].isSelected = false
							
							let rowAbove:IndexPath = IndexPath.init(row: indexPath.row - 1, section: indexPath.section)
							self.tableView.deselectRow(at: rowAbove, animated: true)
							self.tableView.cellForRow(at: rowAbove)?.accessoryType = .none
							self.removeChoice(rowValue: (self.tableView.cellForRow(at: rowAbove)?.textLabel?.text)!)
						}
					}
				}
			}
				
			else
			{
				let theSectionLetter:String = sections[indexPath.section]
				
				if (self.selectionDictionary[theSectionLetter]?[indexPath.row].isSelected == true)
				{
					cell.accessoryType = .none
					self.selectionDictionary[theSectionLetter]?[indexPath.row].isSelected = false
					self.removeChoice(rowValue: (cell.textLabel?.text)!)
				}
					
				else
				{
					cell.accessoryType = .checkmark
					self.selectionDictionary[theSectionLetter]?[indexPath.row].isSelected = true
					self.writeChoice(rowValue: (cell.textLabel?.text)!)
				}
				
				if ((self.selectionDictionary[theSectionLetter]?[indexPath.row].isPair)! ==  true)
				{
					let optionText = self.questionList[indexPath.row]
					let lastCharacter = optionText[optionText.index(before: optionText.endIndex)]
					
					let sectionEntries = self.selectionDictionary[sections[indexPath.section]]
					
					let indexOfPair:Int = sectionEntries!.index {$0.description == tableEntries[indexPath.row].pairedWith}!
					tableEntries[indexOfPair].isSelected = false
					
					if (lastCharacter == "+")
					{
						//tableEntries[indexPath.row + 1].isSelected = false
						self.selectionDictionary[theSectionLetter]?[indexPath.row + 1].isSelected = false
						let rowBelow:IndexPath = IndexPath.init(row: indexPath.row + 1, section: indexPath.section)
						self.tableView.deselectRow(at: rowBelow, animated: true)
						self.tableView.cellForRow(at: rowBelow)?.accessoryType = .none
						self.removeChoice(rowValue: (self.tableView.cellForRow(at: rowBelow)?.textLabel?.text)!)
					}
						
					else if (lastCharacter == "-")
					{
						//tableEntries[indexPath.row - 1].isSelected = false
						self.selectionDictionary[theSectionLetter]?[indexPath.row - 1].isSelected = false
						let rowAbove:IndexPath = IndexPath.init(row: indexPath.row - 1, section: indexPath.section)
						self.tableView.deselectRow(at: rowAbove, animated: true)
						self.tableView.cellForRow(at: rowAbove)?.accessoryType = .none
						self.removeChoice(rowValue: (self.tableView.cellForRow(at: rowAbove)?.textLabel?.text)!)
					}
				}
				
			}
			
			self.tableView.deselectRow(at: indexPath, animated: true)
		}
	}
	
	override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		
		
		//self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
		
		//self.tableView.deselectRow(at: indexPath, animated: true)
		
	}
	
	// MARK: - Search
	
	
	func updateSearchResults(for searchController: UISearchController) {
		
		
		// TODO: need to add logic to update filteredOptions when searching
		
		/*
		var result = data.filter { (dataArray:[String]) -> Bool in
		return dataArray.filter({ (string) -> Bool in
		return string.containsString(searchString)
		}).count > 0
		}
		*/
		
		/*
		filteredOptions = options.filter{
		$0.filter{$0.lowercased().contains(searchController.searchBar.text!.lowercased()).count > 0
		}
		}
		*/
		
		
		tableView.reloadData()
		
		
		
	}
	
	/*
	// Override to support conditional editing of the table view.
	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
	// Return false if you do not want the specified item to be editable.
	return true
	}
	*/
	
	/*
	// Override to support editing the table view.
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
	if editingStyle == .delete {
	// Delete the row from the data source
	tableView.deleteRows(at: [indexPath], with: .fade)
	} else if editingStyle == .insert {
	// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
	}
	*/
	
	/*
	// Override to support rearranging the table view.
	override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
	
	}
	*/
	
	/*
	// Override to support conditional rearranging of the table view.
	override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
	// Return false if you do not want the item to be re-orderable.
	return true
	}
	*/
	
	
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		// Get the new view controller using segue.destinationViewController.
		// Pass the selected object to the new view controller.
		
		if(segue.identifier == "showSelectedSegue"){
			
			if let controller = segue.destination as? SelectedOptionsTableViewController {
				slideInTransitioningDelegate.direction = .bottom
				//slideInTransitioningDelegate.disableCompactHeight = false
				controller.transitioningDelegate = slideInTransitioningDelegate
				controller.modalPresentationStyle = .custom
			}
		}
		
		
	}
	
	
	func determinePairsInChoicesList()
	{
		
		if(self.tableEntries.count > 0)
		{
			for index in 0...self.tableEntries.count - 1
			{
				let theOption:String = self.tableEntries[index].description
				let lastCharacter = theOption[theOption.index(before: theOption.endIndex)]
				
				if (lastCharacter == "+")
				{
					self.tableEntries[index].pairedWith = self.tableEntries[index + 1].description
					self.tableEntries[index].isPair = true
				}
					
				else if (lastCharacter == "-")
				{
					self.tableEntries[index].pairedWith = self.tableEntries[index - 1].description
					self.tableEntries[index].isPair = true
				}
			}
		}
	}
	
	func determineIfPairInFiltered(thePair:String) -> Bool
	{
		for entry in self.filteredEntries
		{
			if (self.filteredEntries.contains{_ in entry.description == thePair})
			{
				return true
			}
		}
		
		return false
	}
	
	func isPair(value:Int, pairsList:[Int]) -> Bool
	{
		if (pairsList.contains(value))
		{
			return true
		}
			
		else
		{
			return false
		}
	}
	
	
	func obtainChoicesList()
	{
		if (SharedResourceManager.sharedInstance.questionController.activeQuestionID == 5)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfMarkers!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMarkers!
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionID == 6)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfAlterations!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAlterations!
		}
		
		if (self.questionList.count > 0)
		{
			
			for _ in 0...(self.questionList.count - 1)
			{
				self.selectedList.append(false)
			}
		}
	}
	
	func loadNextView()
	{
		
		if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 6)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "QuestionChecker") as? VCQuestionChecker {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
			// If the next question ID is negative, the end of the questionnaire is reached
			
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID  == -1)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "QuestionnaireFinished") as? VCQuestionnaireFinished {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 7)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
	}
	
	func writeChoicesSelected()
	{
		var choicesArray:[String] = [String]()
		
		if (self.selectedList.count > 0)
		{
			for index in 0...(self.selectedList.count - 1)
			{
				if (self.selectedList[index] == true)
				{
					choicesArray.append(self.questionList[index])
				}
			}
		}
		
		if (choicesArray.count == 0)
		{
			choicesArray.append("None")
		}
		
		//let questionName:String = SharedResourceManager.sharedInstance.questionController.makePatientDataKeyFromQuestion(question: SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!)
		//SharedResourceManager.sharedInstance.activeReportStorage.updateInformationDictionary(key: questionName, value: choicesArray as AnyObject)
	}
	
	func updateSearchResultsForSearchController(searchController: UISearchController)
	{
		filterContentForSearchText(searchText: searchController.searchBar.text!)
	}
	
	func filterContentForSearchText(searchText:String)
	{
		filteredEntries = tableEntries.filter{ terms in
			return terms.description.lowercased().contains(searchText.lowercased())
		}
		
		tableView.reloadData()
	}
	
	func initialiseSelectionArray()
	{
		for choice in questionList
		{
			var choiceAttributes:MVTableEntry = MVTableEntry()
			choiceAttributes.codeID = self.questionDictionary.allKeys(forValue: choice)[0]
			choiceAttributes.description = choice
			choiceAttributes.isSelected = false
			
			tableEntries.append(choiceAttributes)
		}
	}
	
	func writeChoice(rowValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: rowValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: false)
		
	}
	
	func removeChoice(rowValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: rowValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	
}
