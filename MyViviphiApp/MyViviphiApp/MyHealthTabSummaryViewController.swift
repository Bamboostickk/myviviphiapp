//
//  MyHealthTabSummaryViewController.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-12-11.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

class MyHealthTabSummaryViewController: UIViewController {

    var completionPercentage:Double = 0.80
    
    var titles = [["Johnny's Genomic Strategy"],[],["Gender","Age"],["Date of Birth","Postal Code"],["Health Care Ensurer"],[],["Cancer Type","Stage"],["Tumor Markers"],[],["Genomic Alterations"],[],["Health Problems"],[],["Medications","Allergies"]]
    
    var selectedChoices = [["View Strategy"],[],["Male","Unknown"],["Dec 3 1980","T9X 2J3"],["Great West Life"],[],["Colon","Stage 2"],["AR-, AFP, CA19-9, LDH, OVA1, PSA"],[],["ABL, AKT1, ARID1B, CD22, FAM123B"],[],["COPD, Diabetes, Other: Walking for long periods of time makes me uncomfortable."],[],["Pain Killers, Sulfa Drugs","Other: Bananas"]]
	
	var destinationIdentifiers = [["PDFViewer"],[],["?","EditAgeView"],["Date of Birth","Postal Code"],["Health Care Ensurer"],[],["Cancer Type","Stage"],["Tumor Markers"],[],["Genomic Alterations"],[],["Health Problems"],[],["Medications","Allergies"]]
	
	
    
    @IBOutlet weak var stackView: UIStackView!
    var stackItemHeight:CGFloat = 64
    
    override func viewDidLoad() {
        super.viewDidLoad()
	
        addQuestionsToStack()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func addQuestionsToStack(){
        

        stackView.addArrangedSubview(donutView(scale: 0.66, percentage: completionPercentage))
        
		
        for t in titles{
            switch (t.count) {
                
            // If the list is empty, then just add a line
            case 0:
                stackView.addArrangedSubview(lineView())
                break;
               
            // If the list contains one element then add a long section that stretches across the view
            case 1:
                
                let index = titles.index(where: {$0 == t})
                
                // Update Section Labels
                let sectionView = SummarySectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: stackItemHeight))
                sectionView.sectionTitle.text = t[0]
                sectionView.sectionLabel.text = selectedChoices[index!][0]
                
                // Update Section Status Image
                if (sectionView.sectionLabel.text == "Unknown"){
                    sectionView.status = .incomplete
                }
                
                // Add Constraints
                let heightConstraint = NSLayoutConstraint(item: sectionView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: stackItemHeight)
                sectionView.addConstraint(heightConstraint)
                
                let widthConstraint = NSLayoutConstraint(item: sectionView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width)
                sectionView.addConstraint(widthConstraint)
				
				// Add Custom Gesture Recognizer to View and attatch appropriate storyboard ID
				let gestureRec = SegueTapGestureRecognizer(target: self, action:  #selector(self.performSegueOnSectionTap(sender:)))
				gestureRec.theDestinationIdentifier = destinationIdentifiers[index!][0]
				sectionView.addGestureRecognizer(gestureRec)

                // Add the view to the stack
                stackView.addArrangedSubview(sectionView)
                
                break;
                
            // If the list contains two elements then add two sections, each half the width of the view
            case 2:
                
                let index = titles.index(where: {$0 == t})

                // Create a horizontal stackview to split the two questions
                let horizontalStackView = UIStackView()
                horizontalStackView.axis = .horizontal
                horizontalStackView.alignment = .fill
                
                // Update Section 1 Labels
                let sectionView1 = SummarySectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: stackItemHeight))
                sectionView1.sectionTitle.text = t[0]
                sectionView1.sectionLabel.text = selectedChoices[index!][0]
                
                // Update Section 1 Status Image
                if (sectionView1.sectionLabel.text == "Unknown"){
                    sectionView1.status = .incomplete
                }
				
				// Add Custom Gesture Recognizer to View 1 and attatch appropriate storyboard ID
				let gestureRec1 = SegueTapGestureRecognizer(target: self, action:  #selector(self.performSegueOnSectionTap(sender:)))
				gestureRec1.theDestinationIdentifier = destinationIdentifiers[index!][0]
				sectionView1.addGestureRecognizer(gestureRec1)
                
                // Update Section 2 Labels
                let sectionView2 = SummarySectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: stackItemHeight))
                sectionView2.sectionTitle.text = t[1]
                sectionView2.sectionLabel.text = selectedChoices[index!][1]
                
                // Update Section 2 Status Image
                if (sectionView2.sectionLabel.text == "Unknown"){
                    sectionView2.status = .incomplete
                }
				
				// Add Custom Gesture Recognizer to View 2 and attatch appropriate storyboard ID
				let gestureRec2 = SegueTapGestureRecognizer(target: self, action:  #selector(self.performSegueOnSectionTap(sender:)))
				gestureRec2.theDestinationIdentifier = destinationIdentifiers[index!][1]
				sectionView2.addGestureRecognizer(gestureRec2)
                
                // Add Constraints
                let heightConstraint1 = NSLayoutConstraint(item: sectionView1, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: stackItemHeight)
                sectionView1.addConstraint(heightConstraint1)

                let widthConstraint1 = NSLayoutConstraint(item: sectionView1, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width/2)
                sectionView1.addConstraint(widthConstraint1)

                let heightConstraint2 = NSLayoutConstraint(item: sectionView2, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: stackItemHeight)
                sectionView2.addConstraint(heightConstraint2)

                let widthConstraint2 = NSLayoutConstraint(item: sectionView2, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width/2)
                sectionView2.addConstraint(widthConstraint2)
                
                let heightConstraintStack = NSLayoutConstraint(item: horizontalStackView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: stackItemHeight)
                horizontalStackView.addConstraint(heightConstraintStack)
                
                // Add the views to the horizontal stack
                horizontalStackView.addArrangedSubview(sectionView1)
                horizontalStackView.addArrangedSubview(sectionView2)
                
                // Add the horizontal stack to the main stack view
                stackView.addArrangedSubview(horizontalStackView)
                break;

            default:
                break;
            }
        }
    }
    
    // Returns a UIView that contains a horizonal line in the center of it
    func lineView() -> UIView{
        
        // Create the line view
        let line = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*0.95, height: 1))
        line.layer.borderWidth = 1.0
        line.layer.borderColor = UIColor.lightGray.cgColor
        
        // Add constraints for line
        let lineHeightConstraint = NSLayoutConstraint(item: line, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 1)
        line.addConstraint(lineHeightConstraint)
        
        // Create a Holder view to place the line into, so there is space above and below the line
        let holder = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*0.95, height: 10))
        
        // Add constraints for holder
        let holderHeightConstraint = NSLayoutConstraint(item: holder, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 10)
        holder.addConstraint(holderHeightConstraint)
        
        // position the line in the center of the holder
        line.center = CGPoint(x: 0, y: holder.frame.height/2)
        
        // add the line to the holder view
        holder.addSubview(line)

        return holder
    }
    
    
    func donutView(scale:CGFloat,percentage:Double) -> UIView{
        
        // Create Donut View
        let donut = DonutView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*scale, height: self.view.frame.width*scale))
        donut.percentage = percentage
		
        // Add Donut View Constraints
        let donutHeightConstraint = NSLayoutConstraint(item: donut, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width*scale)
        donut.addConstraint(donutHeightConstraint)
        
        let donutWidthConstraint = NSLayoutConstraint(item: donut, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width*scale)
        donut.addConstraint(donutWidthConstraint)
        
        // Create a Holder view to place the line into, so there is space above and below the line
        let holder = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*0.95, height: self.view.frame.width*scale))
        
        // Add constraints for holder
        let holderHeightConstraint = NSLayoutConstraint(item: holder, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width*scale)
        holder.addConstraint(holderHeightConstraint)
        
        // position the line in the center of the holder
        donut.center = CGPoint(x: 0, y: holder.frame.height/2)
        
        // add the line to the holder view
        holder.addSubview(donut)
        
        return holder
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	
	func performSegueOnSectionTap(sender : SegueTapGestureRecognizer){
		//let controller = storyboard!.instantiateViewController(withIdentifier: sender.storyboardID!)
		

		
		// Switch the storyboard ID that we attatched to the gesture recognizer and load it's appropriate views
		switch sender.theDestinationIdentifier! {
		case "PDFViewer":
			VCOverlayController.show(self.view, loadingText: "Retrieving your report")
			SharedResourceManager.sharedInstance.serverRequestController.retrievePDFForPatient(patientID: String(describing: SharedResourceManager.sharedInstance.patientID)){fileURL -> () in
				DispatchQueue.main.async
					{
						let stringURL:String = "\(fileURL)"
						
						let localReportURL = stringURL
						
						if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewer") as? VCPDFViewController {
							resultController.thePDFPath = stringURL
							self.navigationController?.pushViewController(resultController, animated: true)
							VCOverlayController.hide()
						}
				}
			}
			break
			
		default:
			break
		}
		
		
		//self.navigationController?.pushViewController(controller, animated: true)
	}
	
	

}
