//
//  VCEditButtonComplex.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-05.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//


/*****************************************************************/
/*****************************************************************/
/*****************************************************************/
/***************************DEPRECIATED***************************/
/*****************************************************************/
/*****************************************************************/
/*****************************************************************/


import UIKit

class VCEditButtonComplex : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
	@IBOutlet weak var theNavigationBar: UINavigationBar!
	@IBOutlet weak var textLabel: UILabel!
	
	@IBOutlet weak var mainCollectionView: UICollectionView!
	@IBOutlet weak var otherCollectionView: UICollectionView!
	
	let viewControllerIdentifier:String? = ""
	
	let columnSize:Int = 3

	var questionsList:[String] = [String]()
	var questionDictionary:[Int:String] = [:]
	
	var answerObjects:[MVPatientAnswers] = [MVPatientAnswers]()
	var selectedDictionary:[Int:Bool] = [:]
	var buttonEntries:[MVButtonEntry] = [MVButtonEntry]()
	
	var shortOptionsList:[MVButtonEntry] = [MVButtonEntry]()
	var otherOptionsList:[MVButtonEntry] = [MVButtonEntry]()
	
	var genderObject:[MVPatientAnswers] = [MVPatientAnswers]()
	var cancerObject:[MVPatientAnswers] = [MVPatientAnswers()]
	
	var allowsMultipleSelection:Bool? = false
	
	var selectedList:[Bool] = [Bool]()
	
	var otherSelectedValue:String = ""

	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		//self.loadNextScreen()
		self.dismiss(animated: true, completion: nil)
	}
	
	
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
				
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()

		//self.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
				//theNavigationBar.topItem?.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		
		//self.textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
		//self.textLabel.numberOfLines = 3
		
		self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText!
		
		if let hasMultipleSupport = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionSupportsMultipleAnswers
		{
			self.allowsMultipleSelection = hasMultipleSupport
		}
		
		mainCollectionView.allowsMultipleSelection = self.allowsMultipleSelection!
		otherCollectionView.allowsMultipleSelection = self.allowsMultipleSelection!

		setupPageForQuestionType()
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		// Return the appropriate number of items for that collection view
		
		if(collectionView == mainCollectionView)
		{
			return self.shortOptionsList.count
		}
			
		else
		{
			return self.otherOptionsList.count
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QuestionCollectionViewCell

		if collectionView == mainCollectionView
		{
			if(self.shortOptionsList.count > 0)
			{
				cell.label.numberOfLines = 2
				cell.label.text = self.shortOptionsList[indexPath.row].description
				
				if (self.shortOptionsList[indexPath.item].isSelected == true)
				{
					//cell.contentView.backgroundColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 1.0)
					cell.backgroundColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 1.0)
					cell.isSelected = true
					self.mainCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
				}
			}
		}
			
		else
		{
			if (self.otherOptionsList.count > 0)
			{
				//cell.label.numberOfLines = 2
				cell.label.text = self.otherOptionsList[indexPath.row].description
				
				if (self.otherOptionsList[indexPath.item].isSelected == true)
				{
					//cell.contentView.backgroundColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 1.0)
					
					if (self.otherOptionsList[indexPath.item].description != "Other")
					{
						cell.isSelected = true
					}
					
					self.otherCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
					
					if (self.otherOptionsList[indexPath.item].description == "Other" && SharedResourceManager.sharedInstance.patientOtherInputs.lastEntry == "")
					{
						cell.isSelected = false
						self.otherCollectionView.deselectItem(at: indexPath, animated: false)
					}
				}
			}
		}
		
		return cell
	}

	override func viewDidAppear(_ animated: Bool)
	{
		if (SharedResourceManager.sharedInstance.patientOtherInputs.lastEntry == "")
		{
			self.otherCollectionView.reloadData()
		}
	}
	
	// MARK: - UICollectionViewDelegate protocol
	
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		// handle tap events
		//print("You selected cell #\(indexPath.item)!")
		
		let cell = collectionView.cellForItem(at: indexPath)
		let selectedCell:QuestionCollectionViewCell = cell as! QuestionCollectionViewCell
		let cellLabel:String = selectedCell.label.text!
		self.buttonSelectedAction(buttonNamed:cellLabel)
		cell?.isSelected = true
	}
	
	func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
		// handle tap events
		//print("You deselected cell #\(indexPath.item)!")
		let cell = collectionView.cellForItem(at: indexPath)
		let selectedCell:QuestionCollectionViewCell = cell as! QuestionCollectionViewCell
		let cellLabel:String = selectedCell.label.text!
		self.buttonDeselectedAction(buttonNamed: cellLabel)
		
		if (cellLabel == "Other")
		{
			SharedResourceManager.sharedInstance.patientOtherInputs.clearLast()
		}
		
		cell?.isSelected = false
	}
	
	// Sets up the page
	
	func setupPageForQuestionType()
	{
		let question:String = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		
		switch question
		{
		case "Gender":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfGenders!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfGenders!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientGender!
		case "Cancer Diagnosis":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfCancers!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfCancers!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientCancer!
		case "Histology":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfHistologies!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfHistologies!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientHistology!
		case "Cancer Stage":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfCancerStages!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfCancerStages!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientStage!
		case "Tumor Sequencing":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfMarkers!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMarkers!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientTumourMarkers!
		case "Genomic Alterations":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfAlterations!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAlterations!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientGenomicAlterations!
		case "Other Diagnosis":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientOtherDiseases!
		case "Other Medications":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfMedications!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMedications!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientMedications!
		case "Allergies":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfAllergies!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAllergies!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientAllergies!
		case "Ethnicity":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfEthnicities!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfEthnicities!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientEthnicity!
		case "Health Insurer":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfHealthInsurers!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfHealthInsurers!
			answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientInsuruer!
		default:
			break
		}
		
		self.initialiseSelectionArray()
		self.determineSelectedEntries()
		self.setQuestionArrays(totalArray:buttonEntries)
		self.findOtherSelectionInAnswer(patientAnswer: answerObjects)
		//addButtons(listOfOptions: questionsList)
	}
	
	func setQuestionArrays(totalArray:[MVButtonEntry])
	{
		for element in totalArray
		{
			if (element.description == "None" || element.description == "I'm not sure" || element.description == "I don't have cancer" || element.description == "Other" || element.description == "Recurrent")
			{
				self.otherOptionsList.append(element)
			}
			
			else
			{
				self.shortOptionsList.append(element)
			}
		}
	}
	
	// Actions to carry out when a button is pressed
	// When a button is pressed, grabs the button's title and then updates the questionnaire
	// Either by loading a new scene, or grabbing new data and then loading the new scene
	
	func buttonSelectedAction(buttonNamed:String)
	{
		if (self.allowsMultipleSelection == true)
		{
			if (buttonNamed == "None")
			{
				deselectCells(exceptCellNamed:"None")
				self.writeChoice(buttonValue: buttonNamed, willClear: true)
			}

			else if (buttonNamed == "Other")
			{
				deselectNone()
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditOtherView") as? VCEditOtherView {
					resultController.allowsMultipleSelection = self.allowsMultipleSelection
					present(resultController, animated: true, completion: nil)
				}
			}
				
			else
			{
				deselectNone()
				self.writeChoice(buttonValue: buttonNamed, willClear: false)
			}
		}
		
		else
		{
			if (buttonNamed == "None")
			{
				deselectCells(exceptCellNamed:"None")
				self.writeChoice(buttonValue: buttonNamed, willClear: true)
				self.loadNextScreen()
			}
				
			else if (buttonNamed == "I don't have cancer")
			{
				deselectCells(exceptCellNamed:"I don't have cancer")
				self.writeChoice(buttonValue: buttonNamed, willClear: true)
				self.loadNextScreen()
			}
				
			else if (buttonNamed == "Other")
			{
				deselectNone()
				self.customTextField()
//				if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditOtherView") as? VCEditOtherView {
//					resultController.allowsMultipleSelection = self.allowsMultipleSelection
//					present(resultController, animated: true, completion: nil)
//				}
			}
				
			else
			{
				deselectNone()
				self.writeChoice(buttonValue: buttonNamed, willClear: false)
				self.loadNextScreen()
			}
		}
	}
	
	func buttonDeselectedAction(buttonNamed:String)
	{
		self.removeChoice(buttonValue: buttonNamed)
	}
	
	func deselectNone()
	{
		for indexPath in self.otherCollectionView.indexPathsForSelectedItems!
		{
			let selectedCell:QuestionCollectionViewCell = self.otherCollectionView.cellForItem(at: indexPath) as! QuestionCollectionViewCell
			let cellLabel:String = selectedCell.label.text!
			
			if (cellLabel == "None" || cellLabel == "I don't have cancer")
			{
				self.otherCollectionView.deselectItem(at: indexPath, animated:false)
				self.removeChoice(buttonValue: cellLabel)
			}
		}
	}
	
	func deselectCells(exceptCellNamed:String)
	{
		for indexPath in self.mainCollectionView.indexPathsForSelectedItems!
		{
			self.mainCollectionView.deselectItem(at: indexPath, animated: false)
		}
		
		for indexPath in self.otherCollectionView.indexPathsForSelectedItems!
		{
			let selectedCell:QuestionCollectionViewCell = self.otherCollectionView.cellForItem(at: indexPath) as! QuestionCollectionViewCell
			let cellLabel:String = selectedCell.label.text!
			
			if (cellLabel != exceptCellNamed)
			{
				self.otherCollectionView.deselectItem(at: indexPath, animated:false)
			}
		}
	}
	
	// Determines what kind of view controller to load next based on the question ID
	// Only useful if the questions are in a particular order.
	// We can write an algorithm that forces a particualer scene to load based on what the question itself is
	// Needs to be smarter, i.e. checks for question name
	
	func loadNextScreen()
	{
		let patientIDValue = SharedResourceManager.sharedInstance.patientID
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
			DispatchQueue.main.async{
				VCOverlayController.hide()
				self.updateCancerForGender()
				self.updateCancerFieldsFromType()
				self.dismiss(animated: true, completion: nil)
			}
		}
	}
	
	// Displays error message if nothing is selected
	
	func displayErrorMessage()
	{
		let alertView = UIAlertController(title: "Nothing selected", message: "Please make a selection" as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	// Method only to be used if multiple selections are allowed. Checks if anything is selected
	
	func checkIfAnythingSelected() -> Bool
	{
		for value in self.selectedList
		{
			if (value == true)
			{
				return true
			}
		}
		
		return false
	}
	
	func writeChoice(buttonValue:String, willClear:Bool)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: willClear)
		
	}
	
	func removeChoice(buttonValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	func determineSelectedEntries()
	{
		var selectedAnswerIDs:[Int] = [Int]()
		
		for answer in self.answerObjects
		{
			selectedAnswerIDs.append(answer.codeID)
		}

		if (self.buttonEntries.count > 0)
		{
			for index in 0...self.buttonEntries.count - 1
			{
				if (selectedAnswerIDs.contains(self.buttonEntries[index].codeID))
				{
					self.buttonEntries[index].isSelected = true
				}
			}
		}
	}
	
	func updateCancerForGender()
	{
		let genderValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientGender![0].codeID
		
		if (genderValue != 0)
		{
			SharedResourceManager.sharedInstance.questionController.listOfCancers = [:]
			SharedResourceManager.sharedInstance.questionController.getCancersForGender(genderID: genderValue){ () -> () in
			}
		}
		
	}
	
	func updateCancerFieldsFromType()
	{
		if (SharedResourceManager.sharedInstance.patientDataController.patientCancer!.count > 0)
		{
			let cancerValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientCancer![0].codeID
			
			if (cancerValue != 0)
			{
				SharedResourceManager.sharedInstance.questionController.listOfHistologies = [:]
				SharedResourceManager.sharedInstance.questionController.listOfCancerStages = [:]
				SharedResourceManager.sharedInstance.questionController.listOfMarkers = [:]
				SharedResourceManager.sharedInstance.questionController.listOfAlterations = [:]
				
				SharedResourceManager.sharedInstance.questionController.getHistology(cancerID: cancerValue){() -> () in}
				SharedResourceManager.sharedInstance.questionController.getCancerStage(cancerID: cancerValue)
				SharedResourceManager.sharedInstance.questionController.getTumours(cancerID: cancerValue)
			}
		}
	}
	
	func findOtherSelectionInAnswer(patientAnswer:[MVPatientAnswers])
	{
		for answer in patientAnswer
		{
			if (answer.description == "Other" || answer.description == "")
			{
				self.otherSelectedValue = answer.customText
				break
			}
		}
	}
	
	func initialiseSelectionArray()
	{
		if (questionsList.count > 0)
		{
			for choice in questionsList
			{
				var choiceAttributes:MVButtonEntry = MVButtonEntry()
				choiceAttributes.codeID = self.questionDictionary.allKeys(forValue: choice)[0]
				choiceAttributes.description = choice
				choiceAttributes.isSelected = false
				
				buttonEntries.append(choiceAttributes)
			}
		}
	}
	
	func customTextField()
	{
		let myMessage:String = "Last value: " + self.setupTextField(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber)
		
		let alertView = UIAlertController(title: "Enter your data here", message: myMessage as String, preferredStyle: .alert)
		
		let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertView.textFields![0] as UITextField
			
			let textFieldInput:String = firstTextField.text!
			
			self.storeTextFieldValues(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber, userInput: textFieldInput)
			self.customChoice(userInput:textFieldInput)
			
			if (self.allowsMultipleSelection == false)
			{
				self.loadNextScreen()
			}
			
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
			(action : UIAlertAction!) -> Void in
		})
		
		alertView.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = SharedResourceManager.sharedInstance.patientOtherInputs.lastEntry
		}
		
		alertView.addAction(saveAction)
		alertView.addAction(cancelAction)
		
		self.present(alertView, animated: true, completion: nil)
	}
	
	func storeTextFieldValues(questionNumber:Int, userInput:String)
	{
		switch questionNumber
		{
		case 3:
			SharedResourceManager.sharedInstance.patientOtherInputs.cancer = userInput
		case 4:
			SharedResourceManager.sharedInstance.patientOtherInputs.histology = userInput
		case 5:
			SharedResourceManager.sharedInstance.patientOtherInputs.stage = userInput
		case 6:
			SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers = userInput
		case 7:
			SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations = userInput
		case 8:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions = userInput
		case 9:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications = userInput
		case 10:
			SharedResourceManager.sharedInstance.patientOtherInputs.allergies = userInput
		case 11:
			SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity = userInput
		case 13:
			SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer = userInput
		default:
			break
		}
	}
	
	func setupTextField(questionNumber:Int) -> String
	{
		var theString = "Nothing selected"
		
		switch questionNumber
		{
		case 3:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.cancer
		case 4:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.histology
		case 5:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.stage
		case 6:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers
		case 7:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations
		case 8:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions
		case 9:
			theString  = SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications
		case 10:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.allergies
		case 11:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity
		case 13:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer
		default:
			theString = "Nothing selected"
			break
		}
		
		return theString
	}
	
}
