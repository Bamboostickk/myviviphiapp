//
//  CheckListTableViewController.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-11-28.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

class CheckListTableViewController: UITableViewController {

    
    var titles = ["Option 1","Option 2","Option 3","Option 4","Option 5","Option 6","Option 7","Option 8","Option 9","Option 10","Option 11","Option 12"]
    var subtitles = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta."]
    
    var headerText = "Select an Option"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 210))
        headerLabel.text = headerText
        headerLabel.textAlignment = .center
        headerLabel.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightLight)
        headerLabel.numberOfLines = 3
        
        
        self.tableView.tableHeaderView = headerLabel
		
		self.tableView.allowsMultipleSelection = true

		
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max(titles.count, subtitles.count)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = titles[indexPath.row]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold)
        cell.detailTextLabel?.text = subtitles[indexPath.row]

        return cell
    }
    

    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark

    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

	
	
	
}
