//
//  MVRegistrationViewController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-10-19.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCRegistrationViewController: UIViewController,UITextFieldDelegate
{
    //let MyKeychainWrapper:KeychainWrapper?
	
	let registrationController:MVRegistrationController? = MVRegistrationController()
	
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var userRetypePassword: UITextField!
    
    // Do any additional setup after loading the view, typically from a nib.
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        userName.delegate = self
        userEmail.delegate = self
        userPassword.delegate = self
        userRetypePassword.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VCLoginScreenViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    // Dispose of any resources that can be recreated.
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    // So the done button hides the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func dismissKeyboard()
    {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func createAccount(_ sender: AnyObject)
    {
		let inputEmail:String = self.userEmail.text!
		let inputPassword:String = self.userPassword.text!
		let confirmPassword:String = self.userRetypePassword.text!
		let inputName:String = self.userName.text!
		
		registrationController?.createAccount(theEmail: inputEmail, thePassword: inputPassword, confirmPassword: confirmPassword, theUsername: inputName){ creationSuccessful -> () in
			
			if (creationSuccessful == true)
			{
				//made the account, so now, grab the token and send the user to the landing page

				print("CREATION OKAY")

				self.registrationController?.retrieveToken(usernameIn: inputEmail, passwordIn: inputPassword){token -> () in
				
					self.writeLoginDataToKeychain(thePassword: inputPassword, theUsername: inputEmail, theAccessToken: token)
					SharedResourceManager.sharedInstance.tokenAPI = token

					OperationQueue.main.addOperation
					{
						print("CHANGE SCENE")
						
						let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
						let nextViewController = storyboard.instantiateViewController(withIdentifier: "getStarted") as UIViewController
						//self.present(nextViewController, animated: true, completion: nil)
						self.navigationController?.pushViewController(nextViewController, animated: true)
					}
					
//					DispatchQueue.main.async
//					{
//						print("CHANGE SCENE")
//						
//						let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//						let nextViewController = storyboard.instantiateViewController(withIdentifier: "getStarted") as UIViewController
//						//self.present(nextViewController, animated: true, completion: nil)
//						self.navigationController?.pushViewController(nextViewController, animated: true)
//
//					}
				}
			}
			
			else
			{
				DispatchQueue.main.async
				{
					self.displayAlertWithMessage(theTitle: "Account creation unsuccessful", theMessage:  (self.registrationController?.errorMessage)!)
				}
			}
		}
			
		
        
//        let hasLoginKey = UserDefaults.standard.bool(forKey: "hasLoginKey")
//        
//        if hasLoginKey == false
//        {
//            UserDefaults.standard.setValue(self.userName.text, forKey: "username")
//        }
//        
//        SharedResourceManager.sharedInstance.MyKeychainWrapper.mySetObject(self.userPassword.text, forKey:kSecValueData)
//        SharedResourceManager.sharedInstance.MyKeychainWrapper.writeToKeychain()
//        
//        UserDefaults.standard.set(true, forKey:"hasLoginKey")
//        UserDefaults.standard.synchronize()
    }
    
    
    @IBAction func cancelLogin(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
	
	func handleErrors()
	{
		
	}
	
	func displayAlertWithMessage(theTitle:String, theMessage:String)
	{
		let alertView = UIAlertController(title: theTitle, message: theMessage as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "OK", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	// Writes the login data to the app's internal storage. The username is stored to the userDefaults, while the password is stored to the key chain
	
	func writeLoginDataToKeychain(thePassword:String, theUsername:String, theAccessToken:String)
	{
		SharedResourceManager.sharedInstance.MyKeychainWrapper.mySetObject(thePassword, forKey: kSecValueData)
		SharedResourceManager.sharedInstance.MyKeychainWrapper.writeToKeychain()
		
		let theAccessToken = "bearer " + theAccessToken
		
		UserDefaults.standard.set(theUsername as Any, forKey:"username")
		UserDefaults.standard.set(theAccessToken as Any, forKey: "accessToken")
		UserDefaults.standard.set(true, forKey: "hasLoginKey")
		UserDefaults.standard.synchronize()
	}

	// Separately writes the token to the keychain
	
	func writeTokenToKeyChain(accessToken:String)
	{
		let theAccessToken = "bearer " + accessToken
		UserDefaults.standard.set(theAccessToken as Any, forKey:"accessToken")
	}
	
}
