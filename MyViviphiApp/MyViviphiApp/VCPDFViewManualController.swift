//
//  VCPDFViewManualController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-23.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import UIKit

class VCPDFViewManualController: UIViewController, UIWebViewDelegate
{
	var thePDFPath:String = ""
	
	@IBOutlet weak var webView: UIWebView!
	
	@IBAction func shareButtonPushed(_ sender: Any)
	{
		let documentController = UIDocumentInteractionController(url: (URL(string:thePDFPath)!))
		documentController.presentOptionsMenu(from: self.view.frame, in: self.view, animated: true)
	}
	
	@IBAction func backButtonPushed(_ sender: Any)
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
	
		
		let pathToPDF:URL = URL(string: thePDFPath)!
		webView.loadRequest(URLRequest(url: pathToPDF))
		
		// Add share button to navigation bar
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(self.shareButtonPushed(_:)))
		
		
		self.edgesForExtendedLayout = [.top]
	}
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}
	
	
}

