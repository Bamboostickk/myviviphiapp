//
//  VCEditPostalCode.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-01.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//


/*****************************************************************/
/*****************************************************************/
/*****************************************************************/
/***************************DEPRECIATED***************************/
/*****************************************************************/
/*****************************************************************/
/*****************************************************************/


import UIKit



class  VCEditPostalCode : UIViewController
{
	// Do any additional setup after loading the view, typically from a nib.
	@IBOutlet weak var theNavigationBar: UINavigationBar!
	@IBOutlet weak var textLabel: UILabel!
	//@IBOutlet weak var subtextLabel: UILabel!
	@IBOutlet weak var postalCodeEntered: UITextField!
	
	@IBAction func doneButtonPressed(_ sender: AnyObject)
	{
		if (postalCodeEntered.text != nil)
		{			
			self.customChoice(userInput: self.postalCodeEntered.text!)
			let patientIDValue = SharedResourceManager.sharedInstance.patientID
			SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
				DispatchQueue.main.async{
					self.dismiss(animated: true, completion: nil)
				}
			}
			
		}
			
		else
		{
			self.displayErrorMessage()
		}
	}
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		//self.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName
		//theNavigationBar.topItem?.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName
		self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText
		self.postalCodeEntered.text = SharedResourceManager.sharedInstance.patientDataController.patientPostalCode?[0].customText
		
		self.postalCodeEntered.becomeFirstResponder()
		
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VCQuestionPostalCode.dismissKeyboard))
		view.addGestureRecognizer(tap)
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	// Displays error message if nothing is selected
	
	func displayErrorMessage()
	{
		let alertView = UIAlertController(title: "Nothing entered", message: "Please input your ZIP code" as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	func dismissKeyboard()
	{
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}

}
