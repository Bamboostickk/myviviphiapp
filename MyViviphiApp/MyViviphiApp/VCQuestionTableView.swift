//
//  VCQuestionTableView.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-17.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

protocol SendDataBack
{
	func didSendSelectionsBack(updatedSelections:[String:MVTableEntry])
}

class VCQuestionTableView:UITableViewController, UISearchBarDelegate, UISearchResultsUpdating, SendDataBack
{
	
	lazy var slideInTransitioningDelegate = SlideInPresentationManager()
	
	var isEditingQuestion = false
	var nextButtonText = "Next" // Set what text should be used in the top right corner

	var questionList:[String] = [String]()
	var tooltipList:[String:String] = [:]
	var questionDictionary:[Int:String] = [:]
	
	var answerObjects:[MVPatientAnswers] = [MVPatientAnswers]()
	var storedStartingObjects:[MVPatientAnswers] = [MVPatientAnswers]()
	
	var tableEntries:[MVTableEntry] = [MVTableEntry]()
	var filteredEntries:[MVTableEntry] = [MVTableEntry]()
	
	var selectedDictionary:[String:MVTableEntry] = [:]
	var selectedList:[String] = [String]()
	var pairsList:[Int] = [Int]()
	let searchController = UISearchController(searchResultsController: nil)
	
	//let selectedColour:UIColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 0.25)	// light purple
	let selectedColour:UIColor = UIColor(colorLiteralRed: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)	// light gray
	
	var searchTerms:String = ""
	
	// This is needed to adjust the background of the status bar for the table view
	var viewCover:UIView!
	
	var sections = ["SP","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	//var options = [["Alpha","Arm"],["Bee","Blizard","Boom"],["Car","Caring","Close"],["Dog"],["Ear","Elf","Emo"],["Fancy","Fun",],["Great"],["Hair","Holiday"],["Igloo"],["Jolly"],["Kite"],["Lamp","Love"],["Magic","Money","Moon",],["Nana","Noon"],[],["Papaya"],[],["Raw","Rent","Rooster"],[],[],[],[],[],[],[],[]]
	
	var selectionDictionary = ["SP": [MVTableEntry](), "A":[MVTableEntry](),"B":[MVTableEntry](), "C":[MVTableEntry](), "D":[MVTableEntry](), "E":[MVTableEntry](), "F":[MVTableEntry](), "G":[MVTableEntry](), "H":[MVTableEntry](), "I":[MVTableEntry](), "J":[MVTableEntry](), "K":[MVTableEntry](), "L":[MVTableEntry](), "M":[MVTableEntry](), "N":[MVTableEntry](), "O":[MVTableEntry](), "P":[MVTableEntry](), "Q":[MVTableEntry](), "R":[MVTableEntry](), "S":[MVTableEntry](), "T":[MVTableEntry](), "U":[MVTableEntry](), "V":[MVTableEntry](), "W":[MVTableEntry](), "X":[MVTableEntry](), "Y":[MVTableEntry](), "Z":[MVTableEntry]()]
	

	@IBAction func doneButtonPressed(_ sender: AnyObject)
	{
		//self.writeChoicesSelected()
		//self.dismiss(animated: true, completion: nil)
		
		// When we're editing from the MyHealth Tav
		if isEditingQuestion
		{
			let patientIDValue = SharedResourceManager.sharedInstance.patientID
			SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
				DispatchQueue.main.async{
					self.dismiss(animated: true, completion: nil)
				}
			}
		}
		
		// Otherwise, when we're in the questionnaire
		else
		{
			self.loadNextView()
		}
		
	}
	
	@IBAction func cancelButtonPressed(_ sender: AnyObject)
	{
		if (self.storedStartingObjects.count > 0)
		{
			self.writeOriginalValuesOnCancel()
		}
		
		self.dismiss(animated: true, completion: nil)
	}
	
	func showSelectedAlterations()
	{
		//guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
		//statusBar.backgroundColor = UIColor.clear
		performSegue(withIdentifier: "showSelectedSegue", sender: self)
	}
	
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		// Setup the Search Controller
		searchController.searchResultsUpdater = self
		searchController.hidesNavigationBarDuringPresentation = false
		searchController.dimsBackgroundDuringPresentation = false
		searchController.searchBar.sizeToFit()
		searchController.searchBar.searchBarStyle = .minimal
		searchController.searchBar.delegate = self
		definesPresentationContext = true
		self.tableView.tableHeaderView = searchController.searchBar
		
		// Make the Section Index Selector on the right have transparent background
		self.tableView.sectionIndexBackgroundColor = UIColor.clear
		
		// Allow multiple options to be selected from the table view.
		self.tableView.allowsMultipleSelection = true
		
		// Show toolbar at the bottom of the screen
		self.navigationController?.setToolbarHidden(false, animated: false)
		let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
		let showSelectedButton = UIBarButtonItem(title: "Show Selected Choices", style: .done, target: self, action: #selector(showSelectedAlterations))
		self.setToolbarItems([flexibleSpace, showSelectedButton, flexibleSpace], animated: true)
		
		
		/* Uncomment to add a UI View with Title at the top of the list
		//var headerText = "Select Your Genomic Alterations"
		let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 210))
		headerLabel.text = headerText
		headerLabel.textAlignment = .center
		headerLabel.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightLight)
		headerLabel.numberOfLines = 3
		
		
		self.tableView.tableHeaderView = headerLabel
		*/
		
		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false
		
		// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		// self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		if isEditingQuestion
		{
			//SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			
			// Add Cancel button to the left since we are in edit mode
			self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelButtonPressed(_:)))
			
		}
			
		else
		{
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber += 1
		}
		
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		
		self.tableView.allowsMultipleSelection = true
		
		self.obtainChoicesList()
		self.initialiseSelectionArray()
		self.determinePairsInChoicesList()

		if (isEditingQuestion == true)
		{
			self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(self.cancelButtonPressed(_:)))
			self.determineCheckedEntries()
			nextButtonText = "Done"
		}
		
		else
		{
			self.selectedDictionary.removeAll()
		}
		
		self.initialiseDictionary()
		
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: nextButtonText, style: .done, target: self, action: #selector(self.doneButtonPressed(_:)))
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		// Make the navigation bar white so the list doesn't scroll into it!
		viewCover = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: UIApplication.shared.statusBarFrame.height))
		viewCover.backgroundColor = UIColor.white
		self.navigationController?.view.addSubview(viewCover)
		self.navigationController?.navigationBar.backgroundColor = UIColor.white
		
		self.tableView.reloadData()
		
	}
	
	
	override func viewWillDisappear(_ animated: Bool)
	{
		// Make the navigation bar transparent again when we leave the view
		viewCover.removeFromSuperview()
		self.navigationController?.navigationBar.backgroundColor = TransparentNavigationController().navigationBar.backgroundColor
		self.navigationController?.setToolbarHidden(true, animated: true)
	}
	
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		
		if (searchController.isActive && searchController.searchBar.text != "")
		{
			return 1
		}
		return sections.count
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if (searchController.isActive && searchController.searchBar.text != "")
		{
			return filteredEntries.count
		}
		
		return (selectionDictionary[sections[section]]?.count)!
	}
	
	override func sectionIndexTitles(for tableView: UITableView) -> [String]?
	{
		return sections
	}
	
	//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
	//        return sections[section]
	//    }
	//
	//
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		
		//self.selectedList = SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.retrieveValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		
		let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
		
		if(self.tableEntries.count > 0)
		{
			if (searchController.isActive && searchController.searchBar.text != "")
			{
				//print(filteredOptions[indexPath.row])
				cell.textLabel?.text = filteredEntries[indexPath.row].description
				
				if (filteredEntries[indexPath.row].isSelected == true)
				{
					cell.backgroundColor = selectedColour
					cell.isSelected = true
					cell.accessoryType = .checkmark
				}
					
				else
				{
					cell.backgroundColor = UIColor.clear
					cell.isSelected = false
					cell.accessoryType = .none
				}
				
			}
				
			else
			{
				let theSectionLetter:String = sections[indexPath.section]
				let theCellItem = selectionDictionary[theSectionLetter]
				
				cell.textLabel?.text = theCellItem![indexPath.row].description
				
				if (self.isEditingQuestion == true)
				{
					if (self.selectionDictionary[theSectionLetter]?[indexPath.row].isSelected == true)
					{
						cell.accessoryType = .checkmark
						cell.backgroundColor = selectedColour
						cell.isSelected = true
					}
						
					else
					{
						cell.backgroundColor = UIColor.clear
						cell.accessoryType = .none
						cell.isSelected = false
					}
				}
					
				else
				{
					
					self.selectedList = SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.retrieveValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID)
					
					if (self.selectedList.contains((cell.textLabel?.text)!))
					{
						self.selectedDictionary[(cell.textLabel?.text)!] = self.selectionDictionary[theSectionLetter]?[indexPath.row]
						cell.accessoryType = .checkmark
						cell.backgroundColor = selectedColour
						//cell.isSelected = true
					}
						
					else
					{
						cell.backgroundColor = UIColor.clear
						cell.accessoryType = .none
						//cell.isSelected = false
					}
				}
			}
		}
		
		else
		{
			cell.textLabel?.text = "No options available for this cancer"
		}
		
		
		cell.textLabel?.backgroundColor = UIColor.clear
		cell.detailTextLabel?.backgroundColor = UIColor.clear
		//cell.accessoryType = cell.isSelected ? .checkmark : .none
		cell.selectionStyle = .none
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
	{
		let theSectionLetter:String = sections[section]
		return theSectionLetter
	}
	
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return 60
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		if let cell = tableView.cellForRow(at: indexPath)
		{
			
			if (cell.textLabel?.text == "Other")
			{
				self.deselectNone()
				self.selectedOther(indexPath: indexPath)
			}
				
			else
			{
				if ((searchController.isActive) && (searchController.searchBar.text != ""))
				{
					
					let objectNamed:String = self.filteredEntries[indexPath.row].description
					
					if (self.getOriginalInstance(objectName: objectNamed)?.isSelected == true)
					{
						cell.accessoryType = .none
						cell.backgroundColor = UIColor.clear
						self.filteredEntries[indexPath.row].isSelected = false
						self.updateOriginalInstance(objectName: objectNamed, state: false)
						self.updateTableInstance(objectName: objectNamed, state: false)
						let indexFromAllEntries:Int = self.tableEntries.index {$0.description == filteredEntries[indexPath.row].description}!
						tableEntries[indexFromAllEntries].isSelected = false
						self.removeChoice(rowValue: (cell.textLabel?.text)!)
						if let index = self.selectedList.index(of: self.filteredEntries[indexPath.row].description)
						{
							self.selectedList.remove(at: index)
						}
					}
						
					else
					{
						cell.accessoryType = .checkmark

						// Animate the tapped cell's background color on tap
						UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseOut], animations: {
							cell.backgroundColor = self.selectedColour
						}, completion: { (finished: Bool) in })
						
						self.filteredEntries[indexPath.row].isSelected = true
						self.updateOriginalInstance(objectName: objectNamed, state: true)
						self.updateTableInstance(objectName: objectNamed, state: true)
						let indexFromAllEntries:Int = self.tableEntries.index {$0.description == filteredEntries[indexPath.row].description}!
						tableEntries[indexFromAllEntries].isSelected = true
						self.writeChoice(rowValue: (cell.textLabel?.text)!, clearAnswers: false)
						self.selectedList.append((cell.textLabel?.text)!)
						self.selectedDictionary[(cell.textLabel?.text)!] = self.filteredEntries[indexPath.row]
						
						self.deselectNone()
					}
					
					// In progress, need updates
					
					if (self.filteredEntries[indexPath.row].isPair)
					{
						let optionText = self.filteredEntries[indexPath.row].description
						let lastCharacter = optionText[optionText.index(before: optionText.endIndex)]
						
						let indexOfPair:Int = self.tableEntries.index {$0.description == filteredEntries[indexPath.row].pairedWith}!
						tableEntries[indexOfPair].isSelected = false
						
						if (self.determineIfPairInFiltered(thePair:filteredEntries[indexPath.row].pairedWith))
						{
							if (lastCharacter == "+")
							{
								filteredEntries[indexPath.row + 1].isSelected = false
								self.updateOriginalInstance(objectName: filteredEntries[indexPath.row + 1].description, state: false)
								self.updateTableInstance(objectName: filteredEntries[indexPath.row + 1].description, state: false)
								let rowBelow:IndexPath = IndexPath.init(row: indexPath.row + 1, section: indexPath.section)
								self.tableView.deselectRow(at: rowBelow, animated: true)
								self.tableView.cellForRow(at: rowBelow)?.accessoryType = .none
								self.tableView.cellForRow(at: rowBelow)?.backgroundColor = UIColor.clear
								self.removeChoice(rowValue: (self.tableView.cellForRow(at: rowBelow)?.textLabel?.text)!)
								if let index = self.selectedList.index(of: self.filteredEntries[indexPath.row + 1].description)
								{
									self.selectedList.remove(at: index)
									self.selectedDictionary.removeValue(forKey:(self.filteredEntries[indexPath.row + 1].description))
								}
							}
								
							else if (lastCharacter == "-")
							{
								
								filteredEntries[indexPath.row - 1].isSelected = false
								self.updateOriginalInstance(objectName: filteredEntries[indexPath.row - 1].description, state: false)
								self.updateTableInstance(objectName: filteredEntries[indexPath.row - 1].description, state: false)
								let rowAbove:IndexPath = IndexPath.init(row: indexPath.row - 1, section: indexPath.section)
								self.tableView.deselectRow(at: rowAbove, animated: true)
								self.tableView.cellForRow(at: rowAbove)?.accessoryType = .none
								self.tableView.cellForRow(at: rowAbove)?.backgroundColor = UIColor.clear
								self.removeChoice(rowValue: (self.tableView.cellForRow(at: rowAbove)?.textLabel?.text)!)
								if let index = self.selectedList.index(of: self.filteredEntries[indexPath.row - 1].description)
								{
									self.selectedList.remove(at: index)
									self.selectedDictionary.removeValue(forKey:(self.filteredEntries[indexPath.row - 1].description))
								}
							}
						}
					}
				}
					
				else
				{
					let theSectionLetter:String = sections[indexPath.section]
					//let theCellItem = selectionDictionary[theSectionLetter]
					
					let objectNamed:String = (self.selectionDictionary[theSectionLetter]?[indexPath.row].description)!
					
					if (self.getOriginalInstance(objectName: objectNamed)?.isSelected == true)
					{
						cell.accessoryType = .none
						cell.backgroundColor = UIColor.clear
						self.selectionDictionary[theSectionLetter]?[indexPath.row].isSelected = false
						self.updateOriginalInstance(objectName: objectNamed, state: false)
						self.removeChoice(rowValue: (cell.textLabel?.text)!)

						if let index:Int = self.selectedList.index(of: (cell.textLabel?.text)!)
						{
							self.selectedList.remove(at: index)
							self.selectedDictionary.removeValue(forKey:(cell.textLabel?.text)!)
						}
					}
						
					else
					{
						cell.accessoryType = .checkmark
						
						// Animate the tapped cell's background color on tap
						UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseOut], animations: {
							cell.backgroundColor = self.selectedColour
						}, completion: { (finished: Bool) in })
						
						self.selectionDictionary[theSectionLetter]?[indexPath.row].isSelected = true
						self.updateOriginalInstance(objectName: objectNamed, state: true)
						self.selectedList.append((cell.textLabel?.text)!)
						self.selectedDictionary[(cell.textLabel?.text)!] = self.selectionDictionary[theSectionLetter]?[indexPath.row]
						
						if (self.selectionDictionary[theSectionLetter]?[indexPath.row].description == "None" || self.selectionDictionary[theSectionLetter]?[indexPath.row].description == "I'm not sure")
						{
							//print((self.selectionDictionary[theSectionLetter]?[indexPath.row].description)! + " selected.")
							self.deselectAll(exceptCellNamed: (self.selectionDictionary[theSectionLetter]?[indexPath.row].description)!)
							self.writeChoice(rowValue: (cell.textLabel?.text)!, clearAnswers: true)

						}
						
						else
						{
							self.deselectNone()
							self.writeChoice(rowValue: (cell.textLabel?.text)!, clearAnswers: false)
						}
					}
					
					if ((self.selectionDictionary[theSectionLetter]?[indexPath.row].isPair)! ==  true)
					{
						let optionText = self.selectionDictionary[theSectionLetter]?[indexPath.row].description
						let lastCharacter = optionText?[(optionText?.index(before: (optionText?.endIndex)!))!]
						
						let sectionEntries = self.selectionDictionary[sections[indexPath.section]]
						
						let indexOfPair:Int = sectionEntries!.index {$0.description == sectionEntries?[indexPath.row].pairedWith}!
						tableEntries[indexOfPair].isSelected = false
						
						if (lastCharacter == "+")
						{
							self.selectionDictionary[theSectionLetter]?[indexPath.row + 1].isSelected = false
							self.updateOriginalInstance(objectName: (self.selectionDictionary[theSectionLetter]?[indexPath.row + 1].description)!, state: false)
							//tableEntries[indexPath.row + 1].isSelected = false
							let rowBelow:IndexPath = IndexPath.init(row: indexPath.row + 1, section: indexPath.section)
							self.tableView.deselectRow(at: rowBelow, animated: true)
							self.tableView.cellForRow(at: rowBelow)?.accessoryType = .none
							self.tableView.cellForRow(at: rowBelow)?.backgroundColor = UIColor.clear
							self.removeChoice(rowValue: (self.tableView.cellForRow(at: rowBelow)?.textLabel?.text)!)
							
							if let index = self.selectedList.index(of: (self.tableView.cellForRow(at: rowBelow)?.textLabel?.text)!)
							{
								self.selectedList.remove(at: index)
								self.selectedDictionary.removeValue(forKey: (self.tableView.cellForRow(at: rowBelow)?.textLabel?.text)!)
							}
						}
							
						else if (lastCharacter == "-")
						{
							self.selectionDictionary[theSectionLetter]?[indexPath.row - 1].isSelected = false
							self.updateOriginalInstance(objectName: (self.selectionDictionary[theSectionLetter]?[indexPath.row - 1].description)!, state: false)
							//tableEntries[indexPath.row - 1].isSelected = false
							let rowAbove:IndexPath = IndexPath.init(row: indexPath.row - 1, section: indexPath.section)
							self.tableView.deselectRow(at: rowAbove, animated: true)
							self.tableView.cellForRow(at: rowAbove)?.accessoryType = .none
							self.tableView.cellForRow(at: rowAbove)?.backgroundColor = UIColor.clear
							self.removeChoice(rowValue: (self.tableView.cellForRow(at: rowAbove)?.textLabel?.text)!)
							if let index = self.selectedList.index(of: (self.tableView.cellForRow(at: rowAbove)?.textLabel?.text)!)
							{
								self.selectedList.remove(at: index)
								self.selectedDictionary.removeValue(forKey: (self.tableView.cellForRow(at: rowAbove)?.textLabel?.text)!)
							}
						}
					}
				}
			}
			
			
			cell.textLabel?.backgroundColor = UIColor.clear
			cell.detailTextLabel?.backgroundColor = UIColor.clear

			
			SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: self.selectedList)
			
			self.tableView.deselectRow(at: indexPath, animated: true)
		}
	}
	
	override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		
		
		self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
		
		//self.tableView.deselectRow(at: indexPath, animated: true)
		
	}
	
	// MARK: - Search
	
	
	//	func updateSearchResults(for searchController: UISearchController) {
	//
	//
	//		// TODO: need to add logic to update filteredOptions when searching
	//
	//		/*
	//		var result = data.filter { (dataArray:[String]) -> Bool in
	//		return dataArray.filter({ (string) -> Bool in
	//		return string.containsString(searchString)
	//		}).count > 0
	//		}
	//		*/
	//
	//		/*
	//		filteredOptions = options.filter{
	//		$0.filter{$0.lowercased().contains(searchController.searchBar.text!.lowercased()).count > 0
	//		}
	//		}
	//		*/
	//
	//
	//		tableView.reloadData()
	//
	//
	//
	//	}
	
	/*
	// Override to support conditional editing of the table view.
	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
	// Return false if you do not want the specified item to be editable.
	return true
	}
	*/
	
	/*
	// Override to support editing the table view.
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
	if editingStyle == .delete {
	// Delete the row from the data source
	tableView.deleteRows(at: [indexPath], with: .fade)
	} else if editingStyle == .insert {
	// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
	}
	*/
	
	/*
	// Override to support rearranging the table view.
	override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
	
	}
	*/
	
	/*
	// Override to support conditional rearranging of the table view.
	override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
	// Return false if you do not want the item to be re-orderable.
	return true
	}
	*/
	
	
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		// Get the new view controller using segue.destinationViewController.
		// Pass the selected object to the new view controller.
		
		if(segue.identifier == "showSelectedSegue"){
			
			if let controller = segue.destination as? SelectedOptionsTableViewController {
				controller.questionDictionary = self.selectedDictionary
				slideInTransitioningDelegate.direction = .bottom
				//slideInTransitioningDelegate.disableCompactHeight = false
				controller.transitioningDelegate = slideInTransitioningDelegate
				controller.modalPresentationStyle = .custom
				controller.delegate = self
			}
		}
	}

	func determineCheckedEntries()
	{
		var selectedAnswerIDs:[Int] = [Int]()
		
		for answer in self.answerObjects
		{
			selectedAnswerIDs.append(answer.codeID)
			
			if (answer.description != "")
			{
				selectedList.append(answer.description)
				selectedDictionary[answer.description] = self.tableEntries.filter {$0.description == answer.description}.first
			}
				
			else
			{
				selectedList.append("Other")
				selectedDictionary["Other"] = self.tableEntries.filter {$0.description == "Other"}.first
			}
			
		}
		
		if(self.tableEntries.count > 0)
		{
			for index in 0...self.tableEntries.count - 1
			{
				if (selectedAnswerIDs.contains(self.tableEntries[index].codeID))
				{
					self.tableEntries[index].isSelected = true
				}
			}
		}
	}
	
	func determinePairsInChoicesList()
	{
		
		if(self.tableEntries.count > 0)
		{
			for index in 0...self.tableEntries.count - 1
			{
				let theOption:String = self.tableEntries[index].description
				let lastCharacter = theOption[theOption.index(before: theOption.endIndex)]
				
				if (lastCharacter == "+")
				{
					self.tableEntries[index].pairedWith = self.tableEntries[index + 1].description
					self.tableEntries[index].isPair = true
				}
					
				else if (lastCharacter == "-")
				{
					self.tableEntries[index].pairedWith = self.tableEntries[index - 1].description
					self.tableEntries[index].isPair = true
				}
			}
		}
	}
	
	func determineIfPairInFiltered(thePair:String) -> Bool
	{
		for entry in self.filteredEntries
		{
			if (self.filteredEntries.contains{_ in entry.description == thePair})
			{
				return true
			}
		}
		
		return false
	}
	
	func isPair(value:Int, pairsList:[Int]) -> Bool
	{
		if (pairsList.contains(value))
		{
			return true
		}
			
		else
		{
			return false
		}
	}
	
	func selectedOther(indexPath:IndexPath)
	{
		let theSectionLetter:String = sections[indexPath.section]
		let myMessage:String = "Last value: " +  self.setupTextField(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber)
		
		let alertView = UIAlertController(title: "Enter your data here", message: myMessage as String, preferredStyle: .alert)
		
		let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertView.textFields![0] as UITextField
			
			let textFieldInput:String = firstTextField.text!
			
			//self.storeTextFieldValues(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber, userInput: textFieldInput)
			
			if (textFieldInput == "")
			{
				self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
				self.tableView.deselectRow(at: indexPath, animated: false)
				self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
				self.selectionDictionary[theSectionLetter]?[indexPath.row].isSelected = false
				if let index = self.selectedList.index(of: (self.tableView.cellForRow(at: indexPath)?.textLabel?.text)!)
				{
					self.selectedList.remove(at: index)
					self.selectedDictionary.removeValue(forKey: "Other")
				}
				self.removeChoice(rowValue: "Other")
			}
				
			else
			{
				self.tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
				self.storeTextFieldValues(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber, userInput: textFieldInput)
				self.selectedList.append("Other")
				self.selectedDictionary["Other"] = self.selectionDictionary[theSectionLetter]?[indexPath.row]
				self.customChoice(userInput:textFieldInput)
			}
			
			self.tableView.deselectRow(at: indexPath, animated: false)
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
			(action : UIAlertAction!) -> Void in
			self.tableView.deselectRow(at: indexPath, animated: false)
		})
		
		alertView.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = SharedResourceManager.sharedInstance.patientOtherInputs.lastEntry
		}
		
		alertView.addAction(saveAction)
		alertView.addAction(cancelAction)
		
		self.present(alertView, animated: true, completion: nil)
	}
	
	func setupTextField(questionNumber:Int) -> String
	{
		var theString = "Nothing selected"
		
		switch questionNumber
		{
		case 6:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers
		case 7:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations
		default:
			break
		}
		return theString
	}
	
	func storeTextFieldValues(questionNumber:Int, userInput:String)
	{
		switch questionNumber
		{
		case 6:
			SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers = userInput
		case 7:
			SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations = userInput
		default:
			break
		}
	}
	
	func deselectNone()
	{
		self.removeChoice(rowValue: "None")
		self.updateTableInstance(objectName: "None", state: false)
		if let index = self.selectedList.index(of: "None")
		{
			self.selectedList.remove(at: index)
			self.selectedDictionary.removeValue(forKey: "None")
		}
		
		self.removeChoice(rowValue: "I\'m not sure")
		self.updateTableInstance(objectName: "I'm not sure", state: false)
		if let index = self.selectedList.index(of: "I\'m not sure")
		{
			self.selectedList.remove(at: index)
			self.selectedDictionary.removeValue(forKey: "I'm not sure")
		}
		
		self.tableView.reloadData()
	}
	
	func deselectAll(exceptCellNamed:String)
	{
		for index in 0...self.tableEntries.count - 1
		{
			if (self.tableEntries[index].description != exceptCellNamed)
			{
				self.tableEntries[index].isSelected = false
				self.updateTableInstance(objectName: self.tableEntries[index].description, state: false)
			}
		}
		
		for (key, _) in self.selectedDictionary
		{
			if (key != exceptCellNamed)
			{
				self.selectedDictionary.removeValue(forKey: key)
			}
		}
		self.selectedList.removeAll()
		self.selectedList.append(exceptCellNamed)
		
		self.tableView.reloadData()
	}
	
	func obtainChoicesList()
	{
		if (SharedResourceManager.sharedInstance.questionController.activeQuestionID == 5)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfMarkers!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMarkers!
			
			if isEditingQuestion
			{
				self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientTumourMarkers!
			}
			
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionID == 6)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfAlterations!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAlterations!

			if isEditingQuestion
			{
				self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientGenomicAlterations!
			}
		}
		self.storedStartingObjects = answerObjects
	}
	
	func initialiseSelectionArray()
	{
		for choice in questionList
		{
			var choiceAttributes:MVTableEntry = MVTableEntry()
			choiceAttributes.codeID = self.questionDictionary.allKeys(forValue: choice)[0]
			choiceAttributes.description = choice
			choiceAttributes.isSelected = false
			
			tableEntries.append(choiceAttributes)
		}
	}
	
	func initialiseDictionary()
	{
		for entry in self.tableEntries
		{
			if (entry.description == "None" || entry.description == "Other" || entry.description == "I'm not sure")
			{
				self.selectionDictionary["SP"]?.append(entry)
			}
				
			else
			{
				let characterArrayFromString:[Character] = Array(entry.description.characters)
				let firstCharacter:String = String(describing:characterArrayFromString[0])
				self.selectionDictionary[firstCharacter]?.append(entry)
			}
		}
		
		self.cleanSelectionDictionary()
	}
	
	func cleanSelectionDictionary()
	{
		var temporaryDictionary:[String:[MVTableEntry]] = [:]
		
		let specialArray:[String] = ["SP"]
		var temporaryArray:[String] = [String]()
		
		for (key, value) in self.selectionDictionary
		{
			if ((self.selectionDictionary[key]?.count)! > 0)
			{
				temporaryDictionary[key] = value
			}
		}
		
		temporaryArray = Array(temporaryDictionary.keys.sorted())
		
		if let indexOfSpecial = temporaryArray.index(of: "SP")
		{
			temporaryArray.remove(at: indexOfSpecial)
		}
		
		self.sections = specialArray + temporaryArray
		self.selectionDictionary = temporaryDictionary
	}
	
	func loadNextView()
	{
		
		if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 6)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AlterationView") as? VCQuestionTableView
			{
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		// If the next question ID is negative, the end of the questionnaire is reached
			
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID  == -1)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "QuestionnaireFinished") as? VCQuestionnaireFinished
			{
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 13 || SharedResourceManager.sharedInstance.questionController.nextQuestionID == 7)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView
			{
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView
			{
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
	}
	
	func writeOriginalValuesOnCancel()
	{

		for answer in self.storedStartingObjects
		{
			var willClear:Bool = false
			
			if (answer.codeID == self.storedStartingObjects.first?.codeID)
			{
				willClear = true
			}
		
			if (answer.description != "" && self.questionDictionary.count > 0)
			{
				self.writeChoice(rowValue: answer.description, clearAnswers: willClear)
			}
		}
	}
	
	func writeChoice(rowValue:String, clearAnswers:Bool)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: rowValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)

		SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: clearAnswers)
		
	}
	
	func removeChoice(rowValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: rowValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	func getOriginalInstance(objectName:String) -> MVTableEntry?
	{
		let objectsNamed:[MVTableEntry] = self.tableEntries.filter(){entry in
			return entry.description == objectName
		}
		
		if(objectsNamed.count > 0)
		{
			let singleTableCell:MVTableEntry = objectsNamed[0]
			return singleTableCell
		}
		
		return nil
		
	}
	
	func updateOriginalInstance(objectName:String, state:Bool)
	{
		let indexOfTargetInOriginal:Int = (self.tableEntries.index{$0.description == objectName})!
		self.tableEntries[indexOfTargetInOriginal].isSelected = state
	}
	
	func updateTableInstance(objectName:String, state:Bool)
	{
		
		if (objectName == "None" || objectName == "I'm not sure" || objectName == "Other")
		{
			let indexToUpdate:Int = (self.selectionDictionary["SP"]?.index{$0.description == objectName})!
			self.selectionDictionary["SP"]?[indexToUpdate].isSelected = state
		}
			
		else
		{
			let characterArrayFromString:[Character] = Array(objectName.characters)
			let firstCharacter:String = String(describing:characterArrayFromString[0])
			let indexToUpdate:Int = (self.selectionDictionary[firstCharacter]?.index{$0.description == objectName})!
			self.selectionDictionary[firstCharacter]?[indexToUpdate].isSelected = state
		}
	}
	
	func reloadDictionary()
	{
		for (key, _) in self.selectionDictionary
		{
			for index in 0...((self.selectionDictionary[key]!.count) - 1)
			{
				self.selectionDictionary[key]![index].isSelected = false
			}
		}
	}
	
	func didSendSelectionsBack(updatedSelections:[String:MVTableEntry])
	{
		
		self.reloadDictionary()
		
		self.selectedDictionary = updatedSelections
		self.selectedList = []
		
		for (key, _) in self.selectedDictionary
		{
			let characterArrayFromString:[Character] = Array(key.characters)
			let firstCharacter:String = String(describing:characterArrayFromString[0])
			//self.selectionDictionary[firstCharacter]?.append(entry)
			
			if (key == "None" || key == "I'm not sure" || key == "Other")
			{
				for index in 0...((self.selectionDictionary["SP"]!.count) - 1)
				{
					if (self.selectionDictionary["SP"]![index].description == key)
					{
						self.selectionDictionary["SP"]![index].isSelected = true
						self.selectedList.append(key)
					}
				}
			}
			
			else
			{
				for index in 0...((self.selectionDictionary[firstCharacter]!.count) - 1)
				{
					if (self.selectionDictionary[firstCharacter]![index].description == key)
					{
						self.selectionDictionary[firstCharacter]![index].isSelected = true
						self.selectedList.append(key)
					}
				}
			}
		}

		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: self.selectedList)
		
		self.tableView.reloadData()
	}
	
	func updateSearchResultsForSearchController(searchController: UISearchController)
	{
		filterContentForSearchText(searchText: searchController.searchBar.text!)
	}
	
	func filterContentForSearchText(searchText:String)
	{
		filteredEntries = tableEntries.filter{ terms in
			return terms.description.lowercased().contains(searchText.lowercased())
		}
		
		tableView.reloadData()
	}
	
	// MARK: - UISearchBar Delegate
	func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int)
	{
		filterContentForSearchText(searchText: searchBar.text!)
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
	{
		self.searchTerms = searchText
	}
	
	func updateSearchResults(for searchController: UISearchController)
	{
		filterContentForSearchText(searchText: searchController.searchBar.text!)
	}
}

//extension VCQuestionTableView: UISearchBarDelegate
//{
//	// MARK: - UISearchBar Delegate
//	func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int)
//	{
//		filterContentForSearchText(searchText: searchBar.text!)
//	}
//}
//
//extension VCQuestionTableView: UISearchResultsUpdating
//{
//	// MARK: - UISearchResultsUpdating Delegate
//	func updateSearchResults(for searchController: UISearchController)
//	{
//		filterContentForSearchText(searchText: searchController.searchBar.text!)
//	}
//}

extension Dictionary where Value: Equatable
{
	func allKeys(forValue val: Value) -> [Key]
	{
		return self.filter { $1 == val }.map { $0.0 }
	}
}
