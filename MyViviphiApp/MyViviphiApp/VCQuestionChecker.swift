//
//  VCQuestionChecker.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-16.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCQuestionChecker:UIViewController
{
	
	var isEditingQuestion = false
	var nextButtonText = "Next" // Set what text should be used in the top right corner

	
	var questionsList:[String] = [String]()
	var questionDictionary:[Int:String] = [:]
	
	//@IBOutlet weak var theNavigationBar: UINavigationBar!
	@IBOutlet weak var textLabel: UILabel!
	
	@IBOutlet weak var ChooseButton: FilledButton!
	@IBOutlet weak var NoneButton: UIButton!
	@IBOutlet weak var NotSureButton: UIButton!
	
	
	// not currently implemented, should add this button in the future.
	@IBAction func SkipButtonPressed(_ sender: AnyObject)
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func cancelButtonPressed(_ sender: AnyObject){
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func YesButtonPressed(_ sender: AnyObject)
	{
		self.loadSelectionView()
	}
	
	@IBAction func NoneButtonPressed(_ sender: AnyObject)
	{
		self.writeChoice(buttonValue: "None")
		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: ["None"])
		
		// Dismiss only if we're editing from MyHealthTab
		if isEditingQuestion
		{
			let patientIDValue = SharedResourceManager.sharedInstance.patientID
			SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
				DispatchQueue.main.async{
					self.dismiss(animated: true, completion: nil)
				}
			}
		}
	
		else
		{
			self.loadNextLanding()
		}
	}
	
	
	@IBAction func NotSureButtonPressed(_ sender: AnyObject)
	{
		self.writeChoice(buttonValue: "I\'m not sure")
		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: ["I'm not sure"])
		
		// Dismiss only if we're editing from MyHealthTab
		if isEditingQuestion
		{
			let patientIDValue = SharedResourceManager.sharedInstance.patientID
			SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
				DispatchQueue.main.async{
					self.dismiss(animated: true, completion: nil)
				}
			}
		}
			
		else
		{
			self.loadNextLanding()
		}
		
	}
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{		
		SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()

		self.dismiss(animated: true, completion: nil)
	}
	
	override func willMove(toParentViewController parent: UIViewController?)
	{
		super.willMove(toParentViewController: parent)
		
		
		if parent == nil && !isEditingQuestion
		{
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		}
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(true)
//		if (questionDictionary.count == 0)
//		{
//			self.ChooseButton.isEnabled = false
//			self.NoneButton.isEnabled = false
//			self.NotSureButton.isEnabled = false
//		}
//		
//		else
//		{
//			self.ChooseButton.isEnabled = true
//			self.NoneButton.isEnabled = true
//			self.NotSureButton.isEnabled = true
//		}
		
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		
		if isEditingQuestion
		{
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			
			// Add Cancel button to the left since we are in edit mode
			self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelButtonPressed(_:)))
		}
		
		else
		{
		
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber += 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		}
		
		self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText!
		
		self.obtainIDValuesForNoneOrNotSure()
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	

	func loadSelectionView()
	{
		if (SharedResourceManager.sharedInstance.questionController.activeQuestionID == 5 || SharedResourceManager.sharedInstance.questionController.activeQuestionID  == 6)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AlterationView") as? VCQuestionTableView {
				
				// Tell the controller if we are just editing the question
				resultController.isEditingQuestion = isEditingQuestion

				
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		// It looks like this will never be called so it was commented out for now
		/*else
		{
			
			
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonView") as? VCQuestionButtonView {
				
				// Tell the controller if we are just editing the question
				//resultController.isEditingQuestion = isEditingQuestion

				
				resultController.hasCheckController = true
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}*/
	}
	
	func loadNextLanding()
	{
		
		if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 4 || SharedResourceManager.sharedInstance.questionController.nextQuestionID == 5 || SharedResourceManager.sharedInstance.questionController.nextQuestionID  == 6 || SharedResourceManager.sharedInstance.questionController.nextQuestionID  == 13)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "QuestionChecker") as? VCQuestionChecker {
				
				// Tell the controller if we are just editing the question
				resultController.isEditingQuestion = isEditingQuestion
				
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
		
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID  == 7)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView {
				
				// Tell the controller if we are just editing the question
				resultController.isEditingQuestion = isEditingQuestion
				
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
				
				// Tell the controller if we are just editing the question
				resultController.isEditingQuestion = isEditingQuestion
				
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
	}
	
	func obtainIDValuesForNoneOrNotSure()
	{
		let question:String = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		
		switch question
		{
			case "Tumor Sequencing":
				questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfMarkers!.values).sorted(by: <)
				questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMarkers!
			case "Genomic Alterations":
				questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfAlterations!.values).sorted(by: <)
				questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAlterations!
			default:
				break
		}
	}
	
	func writeChoice(buttonValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)

		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: true)
	}
}
