//
//  VCReportLandingViewController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-10-28.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import UIKit
import QuickLook

/*	This view controller handles displaying available reports.


*/

private let reuseIdentifier = "cell"

class VCReportLandingViewController: UICollectionViewController,QLPreviewControllerDelegate,QLPreviewControllerDataSource
{
	
	var patientsList:[MVPatient] = []
	
	var localReportURL:String = ""
	
	//var patientNames = ["Billy Bob", "Jim James", "Suzy Santiago", "Lilly Miller"]
	
	var patientImages:[UIImage] = [UIImage]()
	
	// Do any additional setup after loadinget the view, typically from a nib.
	
	override func viewDidLoad()
	{
		if (SharedResourceManager.sharedInstance.checkIfAppOnline() == true)
		{
			populateReportsView()
		}
			
		else
		{
			let halfWidth:CGFloat = UIScreen.main.bounds.width * CGFloat(0.5) - CGFloat(200)
			let halfHeight:CGFloat = UIScreen.main.bounds.height * CGFloat(0.5)
			let message:UILabel = UILabel(frame: CGRect(x:halfWidth, y:halfHeight, width:400, height:45))
			
			message.textAlignment =  .center
			message.text = "Please connect to WiFi to access your reports"
			self.view.addSubview(message)
		}
		
		super.viewDidLoad()
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		self.patientsList = SharedResourceManager.sharedInstance.patientDataController.patientsList
		SharedResourceManager.sharedInstance.patientOtherInputs.resetAll()
		self.collectionView?.reloadData()
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}
	
	// Return the number of sections

	override func numberOfSections(in collectionView: UICollectionView) -> Int
	{
		return 1
	}
	
	
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		// #warning Incomplete implementation, return the number of items
		return patientsList.count
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ReportsCollectionViewCell
		
		cell.label.text = patientsList[indexPath.row].patientName
		
		cell.imageView.image = patientImages[indexPath.row]
		cell.imageView.layer.cornerRadius = cell.imageView.frame.width/2
		cell.imageView.clipsToBounds = true
		
		return cell
	}
	
	// MARK: UICollectionViewDelegate
	
	override func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath)
	{
		let cell = collectionView.cellForItem(at: indexPath) as! ReportsCollectionViewCell
		cell.imageView.layer.opacity = 0.5
	}
	
	override func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath)
	{
		let cell = collectionView.cellForItem(at: indexPath) as! ReportsCollectionViewCell
		cell.imageView.layer.opacity = 1.0
	}
	
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		let cell = collectionView.cellForItem(at: indexPath) as! ReportsCollectionViewCell
		showOpenOrUpdateActionSheet(thePatientName: cell.label.text!)
	}
	
	func showOpenOrUpdateActionSheet(thePatientName:String)
	{
		let addSectionController = UIAlertController(title: thePatientName + "'s Genomic Strategy", message: nil, preferredStyle: .actionSheet)
		
		let patientIDValue:Int = (self.patientsList.filter({$0.patientName == thePatientName}).first?.patientID)!
		
		let viewAction = UIAlertAction(title: "View Strategy", style: .default) { (action) in
			// set the segue to the next view like this
			//self.performSegue(withIdentifier: "segue", sender: nil)
			
			VCOverlayController.show(self.view, loadingText: "Retrieving your report")
			/*
			SharedResourceManager.sharedInstance.reportDownloader.retrievePDFForPatient(patientID: String(describing: SharedResourceManager.sharedInstance.patientID)){fileURL -> () in
				DispatchQueue.main.async
				{
					let stringURL:String = "\(fileURL)"
					
					self.localReportURL = stringURL
					
					if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewer") as? VCPDFViewController {
						resultController.thePDFPath = stringURL
						self.present(resultController, animated: true, completion: nil)
						VCOverlayController.hide()
					}
				}
			}*/
			
			SharedResourceManager.sharedInstance.serverRequestController.retrievePDFForPatient(patientID: String(describing: SharedResourceManager.sharedInstance.patientID)){fileURL -> () in
				DispatchQueue.main.async
				{					
					let stringURL:String = "\(fileURL)"
					
					self.localReportURL = stringURL
					
					if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewer") as? VCPDFViewController {
						resultController.thePDFPath = stringURL
						self.present(resultController, animated: true, completion: nil)
						VCOverlayController.hide()
					}
					
				
//					let theURL = NSURL(fileURLWithPath: stringURL)
//					let documentController = UIDocumentInteractionController(url: theURL as URL)
//					documentController.presentOptionsMenu(from: self.view.frame, in: self.view, animated: true)

					let preview = QLPreviewController()
					preview.dataSource = self
					self.present(preview, animated: true, completion: nil)

				}
		}
	}
		
		let updateAction = UIAlertAction(title: "Update Patient Information", style: .default) { (action) in
			// set the segue to the next view like this
			if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "QuestionEditNavigationController") as? UINavigationController
			{
				VCOverlayController.show(self.view, loadingText: "Loading data")
				SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
					
					DispatchQueue.main.async{
						self.present(resultController, animated: true, completion: nil)
						VCOverlayController.hide()
					}
				}
			}
		}
		
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
		{ (action) in
			// ...
			
		}
		
		addSectionController.addAction(viewAction)
		addSectionController.addAction(updateAction)
		addSectionController.addAction(cancelAction)
		
		self.present(addSectionController, animated: true) {
			// ...
		}
	}
	
	
	@IBAction func newReportButtonPressed(_ sender: AnyObject)
	{
		//SharedResourceManager.sharedInstance.activeReportStorage.resetInstancePatientData()

		if (SharedResourceManager.sharedInstance.checkIfAppOnline() == true)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "RootQuestionaireNavigationController") as? UINavigationController {
				present(resultController, animated: true, completion: nil)
			}
		}
			
		else
		{
			self.displayAlertWithMessage(theTitle: "No connection", theMessage: "Please connect to WiFi to create a new report")
		}
		
	}
	
	@IBAction func logoutButtonPressed(_ sender: AnyObject)
	{
		//UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
		UserDefaults.standard.removeObject(forKey: "accessToken")
		UserDefaults.standard.removeObject(forKey: "hasLoginKey")
		UserDefaults.standard.synchronize()
		
		if let resultController = storyboard!.instantiateViewController(withIdentifier: "LoginScene") as? VCLoginScreenViewController
		{
			present(resultController, animated: true, completion: nil)
		}
	}
	
	
	// MARK: Quicklook
	
	func numberOfPreviewItems(in controller: QLPreviewController) -> Int
	{
		return 1
	}
	
	func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem
	{
		let url = NSURL(fileURLWithPath: self.localReportURL)
		return url
	}
	
	// Displays warning message with text
	
	func displayAlertWithMessage(theTitle:String, theMessage:String)
	{
		let alertView = UIAlertController(title: theTitle, message: theMessage as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	func generatePatientNames()
	{
		var unsortedPatients:[String] = [String]()
		
//		for (_, value) in self.patientDictionary
//		{
//			unsortedPatients.append(value)
//			self.patientImages.append(UIImage(named: "male-3.png")!)
//		}
//		
//		self.patientNames = unsortedPatients.sorted(by: <)
//		
	}
	
	func updateQuestionsAndChoices()
	{
		SharedResourceManager.sharedInstance.questionController.getQuestions()
		
		SharedResourceManager.sharedInstance.questionController.getGenders()
		SharedResourceManager.sharedInstance.questionController.getGenomicAlterations()
		SharedResourceManager.sharedInstance.questionController.getOtherConditions()
		SharedResourceManager.sharedInstance.questionController.getMedications()
		SharedResourceManager.sharedInstance.questionController.getAllergies()
		SharedResourceManager.sharedInstance.questionController.getHealthInsurers()
		SharedResourceManager.sharedInstance.questionController.getEthnicities()
		
		SharedResourceManager.sharedInstance.questionController.resetQuestionNumbers()
	}
	
	func populateReportsView()
	{
		self.updateQuestionsAndChoices()
		
		if (self.patientsList.count == 0)
		{
			VCOverlayController.show(self.view, loadingText: "Loading your reports...")
		}
		
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfPatientsFromServerWithDelay(){patientData -> () in
			self.patientsList = patientData
			self.generatePatientNames()
						
			DispatchQueue.main.async{
				self.collectionView?.reloadData()
				VCOverlayController.hide()
			}
		}
	}
}
