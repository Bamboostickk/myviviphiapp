//
//  VCOtherFieldView.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-22.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit
import Foundation

class VCOtherFieldView:UIViewController
{
	
	var allowsMultipleSelection:Bool? = false
	var replies:[String] = [String]()
	
	@IBOutlet weak var userTextField: UITextView!
	
	@IBAction func SaveButtonPressed(_ sender: AnyObject)
	{
		if (userTextField.text == "")
		{
			self.displayErrorMessage(theTitle: "Nothing entered", theMessage: "Please enter something or press back to go back")
		}
		
		else
		{
			self.storeTextFieldValues()
			self.customChoice(userInput: userTextField.text)
			self.storeTextFieldValues(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber)
			
			if (allowsMultipleSelection == false)
			{
				self.loadNextScreen()
			}
				
			else
			{
				self.dismiss(animated: true, completion:nil)
			}
		}
	}
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		
		userTextField.becomeFirstResponder()
		let nextBarButton = UIBarButtonItem(title: "Next", style: .done, target: self, action: #selector(self.SaveButtonPressed(_:)))
		self.navigationItem.rightBarButtonItem = nextBarButton
		
		
		super.viewDidLoad()
		self.setupTextField(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber)
		self.userTextField.becomeFirstResponder()
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VCQuestionAge.dismissKeyboard))
		view.addGestureRecognizer(tap)
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	// Determines what kind of view controller to load next based on the question ID
	// Only useful if the questions are in a particular order.
	// We can write an algorithm that forces a particualr scene to load based on what the question itself is
	
	func loadNextScreen()
	{
		if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 2)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AgeView") as? VCQuestionAge {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID  == 5 || SharedResourceManager.sharedInstance.questionController.nextQuestionID == 6)
		{
			
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "TableLanding") as? UINavigationController {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
			
			//			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AlterationLanding") as? UINavigationController {
			//				present(resultController, animated: true, completion: nil)
			//			}
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID   == 10)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonView") as? VCEditButtonView {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID   == 11)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "PostalCodeView") as? VCQuestionPostalCode {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
			// If the next question number is larger than the total number of questions, the next question doesn't exist.
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber + 1 > SharedResourceManager.sharedInstance.questionController.maximumQuestionNumber)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "QuestionnaireFinished") as? VCQuestionnaireFinished {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
			
		else
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
				self.navigationController?.pushViewController(resultController, animated: true)
			}
		}
	}
	
	//Fix bug where question jumps when using this
	
	func storeTextFieldValues()
	{
		let userInputText:String = userTextField.text
		self.replies = userInputText.components(separatedBy: ",")
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	func dismissKeyboard()
	{
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
	
	// Displays error message if nothing is entered
	
	func displayErrorMessage(theTitle:String, theMessage:String)
	{
		let alertView = UIAlertController(title: theTitle, message: theMessage as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	func setupTextField(questionNumber:Int)
	{
		switch questionNumber
		{
		case 3:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.cancer
		case 4:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.histology
		case 5:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.stage
		case 6:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers
		case 7:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations
		case 8:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions
		case 9:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications
		case 10:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.allergies
		case 11:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity
		case 13:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer
		default:
			self.userTextField.text = ""
			break
		}
	}
	
	func storeTextFieldValues(questionNumber:Int)
	{
		//let userInputText:String = userTextField.text
		//self.replies = userInputText.components(separatedBy: ",")
		switch questionNumber
		{
		case 3:
			SharedResourceManager.sharedInstance.patientOtherInputs.cancer = self.userTextField.text
		case 4:
			SharedResourceManager.sharedInstance.patientOtherInputs.histology = self.userTextField.text
		case 5:
			SharedResourceManager.sharedInstance.patientOtherInputs.stage = self.userTextField.text
		case 6:
			SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers = self.userTextField.text
		case 7:
			SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations = self.userTextField.text
		case 8:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions = self.userTextField.text
		case 9:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications = self.userTextField.text
		case 10:
			SharedResourceManager.sharedInstance.patientOtherInputs.allergies = self.userTextField.text
		case 11:
			SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity = self.userTextField.text
		case 13:
			SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer = self.userTextField.text
		default:
			self.userTextField.text = ""
			break
		}
	}

	
}
