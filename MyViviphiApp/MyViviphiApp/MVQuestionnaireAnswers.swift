//
//  MVQuestionnaireAnswers.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-29.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

class MVQuestionnaireAnswers
{
	var gender:[String] = [String]()
	var age:[String] = [String]()
	var cancer:[String] = [String]()
	var histology:[String] = [String]()
	var stage:[String] = [String]()
	var tumourMarkers:[String] = [String]()
	var genomicAlterations:[String] = [String]()
	var otherDiseases:[String] = [String]()
	var medications:[String] = [String]()
	var allergies:[String] = [String]()
	var ethnicity:[String] = [String]()
	var postalCode:[String] = [String]()
	var insurer:[String] = [String]()
	
	func retrieveValueForQuestion(questionID:Int) -> [String]
	{
		var answersToObtain:[String] = [String]()
		
		switch questionID
		{
		case 1:
			answersToObtain = self.gender
		case 2:
			answersToObtain = self.age
		case 3:
			answersToObtain = self.cancer
		case 13:
			answersToObtain = self.histology
		case 4:
			answersToObtain = self.stage
		case 5:
			answersToObtain = self.tumourMarkers
		case 6:
			answersToObtain = self.genomicAlterations
		case 7:
			answersToObtain = self.otherDiseases
		case 8:
			answersToObtain = self.medications
		case 9:
			answersToObtain = self.allergies
		case 10:
			answersToObtain = self.ethnicity
		case 11:
			answersToObtain = self.postalCode
		case 12:
			answersToObtain = self.insurer
		default:
			break
		}
		
		return answersToObtain
	}
	
	func updateValueForQuestion(questionID:Int, values:[String])
	{
		switch questionID
		{
		case 1:
			self.gender = values
		case 2:
			self.age = values
		case 3:
			self.cancer = values
		case 13:
			self.histology = values
		case 4:
			self.stage = values
		case 5:
			self.tumourMarkers = values
		case 6:
			self.genomicAlterations = values
		case 7:
			self.otherDiseases = values
		case 8:
			self.medications = values
		case 9:
			self.allergies = values
		case 10:
			self.ethnicity = values
		case 11:
			self.postalCode = values
		case 12:
			self.insurer = values
		default:
			break
		}
	}
	
	func resetQuestionnaireInstance()
	{
		self.gender = []
		self.age = []
		self.cancer = []
		self.histology = []
		self.stage = []
		self.tumourMarkers = []
		self.genomicAlterations = []
		self.otherDiseases = []
		self.medications = []
		self.allergies = []
		self.ethnicity = []
		self.postalCode = []
		self.insurer = []
	}
}
