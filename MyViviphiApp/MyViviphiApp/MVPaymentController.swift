//
//  MVPaymentController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-09.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import Stripe

class MVPaymentController
{
	// Token to use for testing
	
	var transactionWasSuccessful:Bool = false
	
	let token = "bearer jLIExJYDYcAamSGKQ9sncuudySafHQLOss85uARBfGaVFqMZMO4T5iPEWDRU9MSdCv3NTJP0dd2ONY6xSi9Hi0iLzdq1AosP6hcuh_HHdGM69r5ElQtCWrXojJ2hfEUT7dpjCxFh2vzfQDAbGA2UnR3Aa7CRGQxYUcbmjD4VrxQqHGSUUzqN87MaZk-xRSHspyI8r4flP3dwjQA3yDQVyERQEQ4UyyUMwE2lSUX5GdaPxEpUuF0FgBc4iZvFQokXmPlek7eE886RaXofqriR07XowJv3VXNIspxzUA47H9aOYX2uTPfoBgGl8vDSXAAz9iAc_x7_l68cqZ6vl76PtZm6d4mAkqDRM3kPkTbbFkMOr9oXXQAKV_BQnVSX_FTMOHsn2gC3R9vIVvs_oRGqMWEDZjWfLPTlQaWM3h_uZV_3aS3beRc0Jj2QBAHnTL4mSdVAbY1s3sKutMJJPQyOWgSN6LiiCKpcRU3ooBAbandosA-qbnovVojoyF6iZ1r2CFBt-PB2HG4ys_bHNCq2KFNfH_PqFhZJgsNd0QT2RLOwkMG1bs67KybADECykqi2cIx8fOZDGGhuV_2fAQKpMo9xxJDbBTmfvBNThJyJDyw"
	
	func makeHTTPRequest(requestURL:String) -> URLRequest
	{
		//let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Patient/GetPatients"
		//var request = URLRequest(url: URL(string: urlString)!)
		
		var request = URLRequest(url: URL(string:requestURL)!)
		request.addValue(token, forHTTPHeaderField: "Authorization")
		//request.addValue(UserDefaults.standard.value(forKey: "accessToken") as! String, forHTTPHeaderField: "Authorization")
		
		request.httpMethod = "GET"
		
		return request

	}
	
	func postStripeRequestWithToken(theToken:STPToken, onCompletion:@escaping (Bool) -> Void)
	{	
		//let authToken = token //String(SharedResourceManager.sharedInstance.tokenAPI!)
		let headers:Dictionary = ["Authorization": token, "content-type": "application/x-www-form-urlencoded"]
		
		let stripeToken = "stripetoken=" + theToken.tokenId
		let postData:NSMutableData =  NSMutableData.init(data: stripeToken .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)

		let paymentType = "&paymentType=MONTH_812"
		let patientName = "&patientName=Miporin"
		let promoCode = "&promoCode=none"
		let patientRelation = "&patientRelation=true"
		
		//clean this code up
		
		postData.append(paymentType.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		postData.append(patientName.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		postData.append(promoCode.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		postData.append(patientRelation.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Purchase/Post"
		
		let requestURL = URL(string:urlString)

		let container = HTTPCookieStorage.shared
		let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers, for: requestURL!)
		container.setCookies(cookies, for: requestURL, mainDocumentURL: requestURL)
		
		var request = URLRequest(url: requestURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
		request.addValue(token, forHTTPHeaderField: "Authorization")
		request.httpMethod = "POST"
		request.httpBody = postData as Data
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
			
			else
			{
				let httpResponse = (response as! HTTPURLResponse)

				if (httpResponse.statusCode == 201)
				{
					self.transactionWasSuccessful  = true
					
					do
					{
						let resultingData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
						let JSONObject = JSON(resultingData)
		
						print (JSONObject)
					}
						
					catch let error as NSError
					{
						print(error.localizedDescription)
					}
				}
				onCompletion(self.transactionWasSuccessful)
			}
		}
		
		dataTask.resume()
	}
	
	func getJSONFromServer(request:URLRequest, onFinish:@escaping ([[String:AnyObject]]) -> Void)
	{
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)

				if (httpResponse.statusCode == 201)
				{
					
					do
					{
						if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String:AnyObject]]
						{
							//print(convertedJsonIntoDict)
							onFinish(convertedJsonIntoDict)
						}
					}
						
					catch let error as NSError
					{
						print(error.localizedDescription)
					}
				}
			}
		}
		
		dataTask.resume()
	}

	func compareCreditCardTokens(creditCardTokens:[String], tokenResult:String) -> Bool
	{
		if (creditCardTokens.contains(tokenResult))
		{
			return true
		}
			
		else
		{
			return false
		}
	}
}
