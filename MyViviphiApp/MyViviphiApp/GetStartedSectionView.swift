//
//  GetStartedSectionView.swift
//  MyViviphiApp
//
//  Created by Sasha Ivanov on 2016-12-30.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class GetStartedSectionView: UIView {
	@IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UITextView!


	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
		//let contentSize = self.content.textContainer.size
		self.content.autoresizesSubviews = true
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
		content.sizeToFit()
		self.autoresizesSubviews = true
	}
	
	func setup(){
		let view = Bundle.main.loadNibNamed("GetStartedSectionView", owner: self, options: nil)?[0] as! UIView
		self.addSubview(view)
		view.frame = self.bounds
		content.textContainer.lineFragmentPadding = 0
	}
	
	
	
}
