//
//  VCEditChecklist.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-19.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//


/*****************************************************************/
/*****************************************************************/
/*****************************************************************/
/***************************DEPRECIATED***************************/
/*****************************************************************/
/*****************************************************************/
/*****************************************************************/


import UIKit

class VCEditChecklist:UITableViewController
{
	var questionList:[String] = [String]()
	var tooltipList:[String:String] = [:]
	var questionDictionary:[Int:String] = [:]
	
	var answerObjects:[MVPatientAnswers] = [MVPatientAnswers]()
	var tableEntries:[MVTableEntry] = [MVTableEntry]()
	var filteredEntries:[MVTableEntry] = [MVTableEntry]()
	
	var selectedDictionary:[Int:Bool] = [:]
	var selectedList:[Bool] = [Bool]()
	
	var allowsMultipleSelection:Bool = false
	
	let selectedColour:UIColor = UIColor(colorLiteralRed: 144/255, green: 19/255, blue: 254/255, alpha: 0.25)
	
	var titles = ["Option 1","Option 2","Option 3","Option 4","Option 5","Option 6","Option 7","Option 8","Option 9","Option 10","Option 11","Option 12"]
	var subtitles = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo nulla ligula, at tempor mauris vehicula eget. Ut ornare leo est. Praesent condimentum nec risus eu sollicitudin. Vivamus auctor at odio eu porta."]
	
	var headerText = "Select an Option"
	
	@IBOutlet weak var navigationBar: UINavigationItem!
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		let patientIDValue = SharedResourceManager.sharedInstance.patientID
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
			DispatchQueue.main.async{
				self.dismiss(animated: true, completion: nil)
			}
		}
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()

		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		
		// Some questions should only allow single selections
		
		if (SharedResourceManager.sharedInstance.questionController.activeQuestionID == 13)
		{
			self.tableView.allowsMultipleSelection = false
		}
		
		else
		{
			self.tableView.allowsMultipleSelection = true
		}
		
		self.allowsMultipleSelection = self.tableView.allowsMultipleSelection
		
		self.obtainChoicesList()
		self.initialiseSelectionArray()
		self.determineCheckedEntries()
		
		// Generate header
		
		let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width - 50, height: 210))
		headerLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText
		headerLabel.textAlignment = .center
		headerLabel.numberOfLines = 3
		headerLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
		headerLabel.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightLight)
		
		
		self.tableView.tableHeaderView = headerLabel
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int
	{
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return tableEntries.count //max(tableEntries.count, subtitles.count)
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		cell.textLabel?.text = tableEntries[indexPath.row].description
		cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold)
		cell.textLabel?.backgroundColor = UIColor.clear
		cell.detailTextLabel?.backgroundColor = UIColor.clear
		//cell.detailTextLabel?.text = subtitles[indexPath.row]
		
		if (tableEntries[indexPath.row].isSelected == true)
		{
			self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .middle)
			cell.accessoryType = .checkmark
			cell.backgroundColor = selectedColour
		}
		
		else
		{
			cell.accessoryType = .none
		}
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return 60
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		self.tableEntries[indexPath.row].isSelected = true
		self.tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
		self.tableView.cellForRow(at: indexPath)?.backgroundColor = selectedColour
		self.tableView.cellForRow(at: indexPath)?.textLabel?.backgroundColor = UIColor.clear
		self.tableView.cellForRow(at: indexPath)?.detailTextLabel?.backgroundColor = UIColor.clear
		self.tableEntries[indexPath.row].isSelected = true
		
		if (self.allowsMultipleSelection == true)
		{
			if (self.tableEntries[indexPath.row].description == "None")
			{
				self.deselectCells(exceptCellNamed: "None")
				self.writeChoice(rowValue: self.tableEntries[indexPath.row].description, clearState: true)
			}
				
			else if (self.tableEntries[indexPath.row].description == "Other")
			{
				self.deselectNone()
				customTextField(indexPath: indexPath)
			}
				
			else
			{
				self.deselectNone()
				self.writeChoice(rowValue: self.tableEntries[indexPath.row].description, clearState: false)
			}
		}
		
		else
		{
			self.writeChoice(rowValue: self.tableEntries[indexPath.row].description, clearState: false)
			
			let patientIDValue = SharedResourceManager.sharedInstance.patientID
			SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
				DispatchQueue.main.async{
					self.dismiss(animated: true, completion: nil)
				}
			}
		}
		
		//self.tableView.deselectRow(at: indexPath, animated: true)
	}
	
	override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
	{
		self.tableEntries[indexPath.row].isSelected = false
		self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
		self.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.clear
		self.tableEntries[indexPath.row].isSelected = false
		
		self.removeChoice(rowValue: self.tableEntries[indexPath.row].description)
	}
	
	func deselectNone()
	{
		for indexPath in self.tableView.indexPathsForSelectedRows!
		{
			let cell = self.tableView.cellForRow(at: indexPath)
			
			if (cell?.textLabel?.text! == "None")
			{
				self.tableView.deselectRow(at: indexPath, animated: false)
				cell?.accessoryType = .none
				cell?.backgroundColor = UIColor.clear
				self.removeChoice(rowValue: (cell?.textLabel?.text!)!)
			}
		}
	}
	
	func deselectCells(exceptCellNamed:String)
	{
		for indexPath in self.tableView.indexPathsForSelectedRows!
		{
			let cell = self.tableView.cellForRow(at: indexPath)
			
			if (cell?.textLabel?.text! != exceptCellNamed)
			{
				self.tableView.deselectRow(at: indexPath, animated: false)
				cell?.accessoryType = .none
				cell?.backgroundColor = UIColor.clear
			}
		}
	}
	
	func obtainChoicesList()
	{
		if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 4)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfHistologies!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfHistologies!
			self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientHistology!
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 8)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfOtherConditions!
			self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientOtherDiseases!
		}
		
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 9)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfMedications!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMedications!
			self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientMedications!
		}
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber == 10)
		{
			self.questionList = Array(SharedResourceManager.sharedInstance.questionController.listOfAllergies!.values).sorted(by: <)
			self.questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAllergies!
			self.answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientAllergies!
		}
		
		if (self.questionList.count > 0)
		{
			for _ in 0...(self.questionList.count - 1)
			{
				self.selectedList.append(false)
			}
			
			self.compareQuestionWithSelection()
			
			for (key, _) in self.selectedDictionary
			{
				if (self.selectedDictionary[key] == true)
				{
					let value:String = self.questionDictionary[key]!
					let indexValue:Int = self.questionList.index(of: value)!
					self.selectedList[indexValue] = true
				}
			}
		}
		
		//self.tableView.reloadData()
	}
	
	func initialiseSelectionArray()
	{
		for choice in questionList
		{
			var choiceAttributes:MVTableEntry = MVTableEntry()
			choiceAttributes.codeID = self.questionDictionary.allKeys(forValue: choice)[0]
			choiceAttributes.description = choice
			choiceAttributes.isSelected = false
			
			tableEntries.append(choiceAttributes)
		}
	}
	
	func determineCheckedEntries()
	{
		var selectedAnswerIDs:[Int] = [Int]()
		
		for answer in self.answerObjects
		{
			selectedAnswerIDs.append(answer.codeID)
		}
		
		if(self.tableEntries.count > 0)
		{
			for index in 0...self.tableEntries.count - 1
			{
				if (selectedAnswerIDs.contains(self.tableEntries[index].codeID))
				{
					self.tableEntries[index].isSelected = true
				}
			}
		}
	}
	
	func writeChoicesSelected()
	{
		var choicesArray:[String] = [String]()
		
		if (self.selectedList.count > 0)
		{
			for index in 0...(self.selectedList.count - 1)
			{
				if (self.selectedList[index] == true)
				{
					choicesArray.append(self.questionList[index])
				}
			}
		}
		
		
		//let questionName:String = SharedResourceManager.sharedInstance.questionController.makePatientDataKeyFromQuestion(question: SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!)
		//SharedResourceManager.sharedInstance.activeReportStorage.updateInformationDictionary(key: questionName, value: choicesArray as AnyObject)
	}
	
	func writeChoice(rowValue:String, clearState:Bool)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: rowValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)

		SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: clearState)
		
	}
	
	func removeChoice(rowValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: rowValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	func compareQuestionWithSelection()
	{
		var selectedArray:[Int] = [Int]()
		
		if (self.answerObjects.count > 0)
		{
			for index in (0...self.answerObjects.count - 1)
			{
				selectedArray.append(self.answerObjects[index].codeID)
			}
			
			for (key, _) in self.questionDictionary
			{
				
				if (selectedArray.contains(key))
				{
					self.selectedDictionary[key] = true
				}
					
				else
				{
					self.selectedDictionary[key] = false
				}
			}
		}
	}
	
	func customTextField(indexPath:IndexPath)
	{
		let myMessage:String = "Last value: " + self.setupTextField(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber)
	
		let alertView = UIAlertController(title: "Enter your data here", message: myMessage as String, preferredStyle: .alert)
		
		let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
			alert -> Void in
			
			let firstTextField = alertView.textFields![0] as UITextField
			
			let textFieldInput:String = firstTextField.text!
			
			self.storeTextFieldValues(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber, userInput: textFieldInput)
			self.customChoice(userInput:textFieldInput)

			if (self.allowsMultipleSelection == false)
			{
				self.dismiss(animated: true, completion: nil)
			}
			
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
			(action : UIAlertAction!) -> Void in
			self.tableView.deselectRow(at: indexPath, animated: false)
			self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
			self.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.clear
		})
		
		alertView.addTextField { (textField : UITextField!) -> Void in
			textField.placeholder = SharedResourceManager.sharedInstance.patientOtherInputs.lastEntry
		}
		
		alertView.addAction(saveAction)
		alertView.addAction(cancelAction)
		
		self.present(alertView, animated: true, completion: nil)
	}
	
	func storeTextFieldValues(questionNumber:Int, userInput:String)
	{
		switch questionNumber
		{
		case 3:
			SharedResourceManager.sharedInstance.patientOtherInputs.cancer = userInput
		case 4:
			SharedResourceManager.sharedInstance.patientOtherInputs.histology = userInput
		case 5:
			SharedResourceManager.sharedInstance.patientOtherInputs.stage = userInput
		case 6:
			SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers = userInput
		case 7:
			SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations = userInput
		case 8:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions = userInput
		case 9:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications = userInput
		case 10:
			SharedResourceManager.sharedInstance.patientOtherInputs.allergies = userInput
		case 11:
			SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity = userInput
		case 13:
			SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer = userInput
		default:
			break
		}
	}
	
	func setupTextField(questionNumber:Int) -> String
	{
		var theString = "Nothing selected"
		
		switch questionNumber
		{
		case 3:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.cancer
		case 4:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.histology
		case 5:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.stage
		case 6:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers
		case 7:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations
		case 8:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions
		case 9:
			theString  = SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications
		case 10:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.allergies
		case 11:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity
		case 13:
			theString = SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer
		default:
			theString = "Nothing selected"
			break
		}
		
		return theString
	}

}
