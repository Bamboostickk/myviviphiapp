//
//  VCCreditCardEntryViewController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-08.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import Stripe
import UIKit

class VCCreditCardEntryViewController: UIViewController
{
	// Do any additional setup after loading the view, typically from a nib.
	
	@IBOutlet weak var cardholderName: UITextField!
	@IBOutlet weak var creditCardNumber: UITextField!
	@IBOutlet weak var emailAddress: UITextField!
	@IBOutlet weak var expirationYear: UITextField!
	@IBOutlet weak var expirationMonth: UITextField!
	@IBOutlet weak var CVCField: UITextField!
	@IBOutlet weak var paymentAmount: UITextField!
	
	@IBOutlet weak var transactionButton: UIButton!
	
	let creditCardParameters = STPCardParams()
	let paymentController = MVPaymentController()
	
	var allTokensForUser = [String]()
	var allAvailablePrices = [String:String]()
	var paymentAmountValue:Float = 0.0
	
	
	
	@IBAction func makePayment(_ sender: AnyObject)
	{
		
		// Send the card info to Strip to get the token
		creditCardParameters.number = self.creditCardNumber.text
		creditCardParameters.cvc = self.CVCField.text
		
		if (self.expirationYear.text != "")
		{
			creditCardParameters.expYear = UInt(self.expirationYear.text!)!
		}
		
		if (self.expirationMonth.text != "")
		{
			creditCardParameters.expMonth = UInt(self.expirationMonth.text!)!
		}
		
		if (self.validateCardInformation() == true)
		{
			self.performStripeOperation()
		}
	}
	
	func validateCardInformation() -> Bool
	{
		if ((self.cardholderName.text == "") || (self.emailAddress.text == ""))
		{
			self.displayAlertBasicMessage(contents: "Please enter name or email")
			return false
		}
				
		if (STPCardValidator.validationState(forCard: creditCardParameters) == .invalid)
		{
			self.displayAlertBasicMessage(contents: "Input credit card information invalid")
			return false
		}
			
		else
		{
			return true
		}
	}

	func performStripeOperation()
	{
		self.transactionButton.isEnabled = false
		
		STPAPIClient.shared().createToken(withCard: creditCardParameters) { (theToken, theError) in
			if (theError != nil)
			{
				self.handleStripeError(errorNamed: theError as! NSError)
			}
				
			else
			{
				self.postStripeToken(theToken: theToken!)
			}
		}
	}
	
	func handleStripeError(errorNamed:NSError)
	{
		if (errorNamed.domain == "StripeDomain")
		{
			self.displayAlertErrorMessage(error: errorNamed)
		}
		
		else
		{
			self.displayAlertBasicMessage(contents: "Please try again")
		}
		
		self.transactionButton.isEnabled = true
	}
	
	func postStripeToken(theToken:STPToken)
	{
		paymentController.postStripeRequestWithToken(theToken: theToken){transactionSucceeded in
			if (transactionSucceeded == true)
			{
				self.transactionDidSucceed()
			}
				
			else
			{
				self.transactionDidNotSucceed()
			}
		}
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/CreditCard/GetAllCards"
		
		paymentController.getJSONFromServer(request: paymentController.makeHTTPRequest(requestURL: urlString)) {resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject[].count - 1
			{
				self.allTokensForUser.append(JSONObject[index]["CardToken"].string!)
			}
		}
		
		let urlStringCharge = SharedResourceManager.sharedInstance.URL_PATH + "api/Charge/Get"
		
		paymentController.getJSONFromServer(request: paymentController.makeHTTPRequest(requestURL: urlStringCharge)) {resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			
			for index in 0...JSONObject[].count - 1
			{
				self.allAvailablePrices[String(JSONObject[index]["Id"].int!)] = String(JSONObject[index]["Price"].int!)
				
				//self.allAvailablePrices.append(String(JSONObject[index]["Price"].int!))
			}
		}
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}

	func transactionDidSucceed()
	{
		let alertView = UIAlertController(title: "Transaction complete", message: "", preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Finish",style: .default)
		{
			(alertView: UIAlertAction!) in
			self.dismiss(animated: true, completion: nil)
		}

		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	func transactionDidNotSucceed()
	{
		let alertView = UIAlertController(title: "Transaction unsuccessful", message: "", preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay",style: .default)
		self.transactionButton.isEnabled = true
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	func displayAlertBasicMessage(contents:String)
	{
		let alertView = UIAlertController(title: "Please try again", message: contents, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	func displayAlertErrorMessage(error:NSError)
	{
		let alertView = UIAlertController(title: "Please try again", message: error.localizedDescription, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
}
