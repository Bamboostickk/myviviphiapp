//
//  MVPatientDataController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-10-20.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

/*		This class obtains a list of all the patients and handles information for the active patient
 */

class MVPatientDataController
{
	
	var patientsList:[MVPatient] = []		//Patient ID and name
	var patientDataDisplay:Dictionary = [Int: String]()	//Question ID and descriptions
	
	var activePatientID:Int = 0
	var activePatientName:String = ""
	var activePatientPercentComplete:Int = 0
	
	var patientGender:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientAge:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientCancer:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientHistology:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientStage:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientTumourMarkers:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientGenomicAlterations:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientOtherDiseases:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientMedications:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientAllergies:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientEthnicity:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientPostalCode:[MVPatientAnswers]? = [MVPatientAnswers]()
	var patientInsuruer:[MVPatientAnswers]? = [MVPatientAnswers]()
	
	func obtainListOfPatientsFromServer()
	{
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Patient"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlString)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			self.patientsList = [] // Reset Array if called
			
			if(JSONObject.count > 0)
			{
				for index in 0...JSONObject.count - 1
				{
					var patientObject:MVPatient = MVPatient()
					
					patientObject.patientID = JSONObject[index]["PatientId"].intValue
					patientObject.patientName = JSONObject[index]["Name"].string!
					patientObject.patientGender = JSONObject[index]["Name"].string!
					patientObject.patientAge = JSONObject[index]["Age"].intValue
					patientObject.patientCancer = JSONObject[index]["Diagnosis"].string!
					patientObject.patientPercent = JSONObject[index]["PercentComplete"].intValue
					
					self.patientsList.append(patientObject)
				}
			}
		}
	}
	
	func obtainListOfPatientsFromServerWithDelay(onFinish:@escaping (([MVPatient]) -> Void))
	{
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Patient"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlString)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)

			self.patientsList = [] // Reset Array if called

			if(JSONObject.count > 0)
			{
				for index in 0...JSONObject.count - 1
				{
					var patientObject:MVPatient = MVPatient()
					
					patientObject.patientID = JSONObject[index]["PatientId"].intValue
					patientObject.patientName = JSONObject[index]["Name"].string!
					patientObject.patientGender = JSONObject[index]["Name"].string!
					patientObject.patientAge = JSONObject[index]["Age"].intValue
					patientObject.patientCancer = JSONObject[index]["Diagnosis"].string!
					patientObject.patientPercent = JSONObject[index]["PercentComplete"].intValue
					
					self.patientsList.append(patientObject)
				}
			}
			onFinish(self.patientsList)
		}
	}
	
	func obtainPatientCountForUser(onFinish:@escaping ((Int) -> Void))
	{
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Patient"
		var patientCount:Int = 0
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlString)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			patientCount = JSONObject.count
		}
		
		onFinish(patientCount)
	}
	
	func obtainListOfAnswersFromServer(patientID:Int, onFinish:@escaping (() -> Void))
	{
		
		let urlWithPatientID = SharedResourceManager.sharedInstance.URL_PATH + "api/Survey/GetForPatient?patientId=" + String(describing: patientID)

	//	let localQuestionDictionary:[Int:MVQuestion] = SharedResourceManager.sharedInstance.questionController.listOfQuestions!
		//let localKey:Int = 0
		
		
		SharedResourceManager.sharedInstance.serverRequestController.getBasicJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL:urlWithPatientID)){resultJSON -> () in
						
			let JSONObject = JSON(resultJSON)
			
			self.obtainSingleUnwrappedAnswers(jsonObject:JSONObject)
			
			// Note: corresponding IDs are hardcoded now because there is a disconnect between the question name and answer name values. We'll need a converter that can handle this.
			
			for (key, _) in JSONObject
			{
				switch key
				{
					case "Gender":
						self.patientGender = self.convertSingleAnswerTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[1] = self.makeDescriptionString(answerArray: self.patientGender!)
					case "Age":
						self.patientAge = self.convertSingleAnswerTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[2] = self.makeCustomTextString(answerArray:self.patientAge!)
					case "Disease":
						self.patientCancer = self.convertSingleAnswerTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[3] = self.makeDescriptionString(answerArray: self.patientCancer!)
						SharedResourceManager.sharedInstance.patientOtherInputs.cancer = self.makeCustomTextString(answerArray: self.patientCancer!)
					case "Histology":
						self.patientHistology = self.convertSingleAnswerTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[13] = self.makeDescriptionString(answerArray: self.patientHistology!)
						SharedResourceManager.sharedInstance.patientOtherInputs.histology = self.makeCustomTextString(answerArray: self.patientHistology!)
					case "Stage":
						self.patientStage = self.convertSingleAnswerTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[4] = self.makeDescriptionString(answerArray: self.patientStage!)
						SharedResourceManager.sharedInstance.patientOtherInputs.stage = self.makeCustomTextString(answerArray: self.patientStage!)
					case "TumorMarkers":
						self.patientTumourMarkers = self.convertMultipleAnswersTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[5] = self.makeDescriptionString(answerArray: self.patientTumourMarkers!)
						SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers = self.makeCustomTextString(answerArray: self.patientTumourMarkers!)
					case "GenomicAlterations":
						self.patientGenomicAlterations = self.convertMultipleAnswersTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[6] = self.makeDescriptionString(answerArray: self.patientGenomicAlterations!)
						SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations = self.makeCustomTextString(answerArray: self.patientGenomicAlterations!)
					case "HealthProblems":
						self.patientOtherDiseases = self.convertMultipleAnswersTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[7] = self.makeDescriptionString(answerArray: self.patientOtherDiseases!)
						SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions = self.makeCustomTextString(answerArray: self.patientOtherDiseases!)
					case "Medications":
						self.patientMedications = self.convertMultipleAnswersTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[8] = self.makeDescriptionString(answerArray: self.patientMedications!)
						SharedResourceManager.sharedInstance.patientOtherInputs.cancer = self.makeCustomTextString(answerArray: self.patientMedications!)
					case "Allergies":
						self.patientAllergies = self.convertMultipleAnswersTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[9] = self.makeDescriptionString(answerArray: self.patientAllergies!)
						SharedResourceManager.sharedInstance.patientOtherInputs.allergies = self.makeCustomTextString(answerArray: self.patientAllergies!)
					case "Ethnicity":
						self.patientEthnicity = self.convertSingleAnswerTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[10] = self.makeDescriptionString(answerArray: self.patientEthnicity!)
						SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity = self.makeCustomTextString(answerArray: self.patientEthnicity!)
					case "PostalZipCode":
						self.patientPostalCode = self.convertSingleAnswerTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[11] = self.makeCustomTextString(answerArray: self.patientPostalCode!)
					case "HealthInsurer":
						self.patientInsuruer = self.convertSingleAnswerTo(jsonObject: JSONObject, fieldKey: key)
						self.patientDataDisplay[12] = self.makeDescriptionString(answerArray: self.patientInsuruer!)
						SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer = self.makeCustomTextString(answerArray: self.patientInsuruer!)
					default:
						break
				}
			}
			onFinish()
		}
	}
	
	func obtainSingleUnwrappedAnswers(jsonObject:JSON)
	{
		self.activePatientID = jsonObject["PatientId"].intValue
		self.activePatientName = jsonObject["Name"].string!
		self.activePatientPercentComplete = jsonObject["PercentComplete"].intValue
	}
	
	func convertSingleAnswerTo(jsonObject:JSON, fieldKey:String) -> [MVPatientAnswers]
	{
		var answerArray:[MVPatientAnswers] = [MVPatientAnswers]()

		var answer:MVPatientAnswers = MVPatientAnswers()
		
		answer.questionID = jsonObject[fieldKey][0]["QuestionId"].intValue
		answer.lookupTypeID = jsonObject[fieldKey][0]["LookupTypeId"].intValue
		answer.codeID = jsonObject[fieldKey][0]["CodeID"].intValue
		answer.customText = jsonObject[fieldKey][0]["CustomText"].stringValue
		answer.description = jsonObject[fieldKey][0]["Description"].stringValue
		
		answerArray.append(answer)
		
		return answerArray
	}

	func convertMultipleAnswersTo(jsonObject:JSON, fieldKey:String) -> [MVPatientAnswers]
	{
		var answerArray:[MVPatientAnswers] = [MVPatientAnswers]()
		
		if(jsonObject[fieldKey].array!.count - 1 >= 0)
		{
			for index in (0...jsonObject[fieldKey].array!.count - 1)
			{
				var answer:MVPatientAnswers = MVPatientAnswers()
				
				answer.questionID = jsonObject[fieldKey][index]["QuestionId"].intValue
				answer.lookupTypeID = jsonObject[fieldKey][index]["LookupTypeId"].intValue
				answer.codeID = jsonObject[fieldKey][index]["CodeID"].intValue
				answer.customText = jsonObject[fieldKey][index]["CustomText"].stringValue
				answer.description = jsonObject[fieldKey][index]["Description"].stringValue
				answerArray.append(answer)
			}
		}
		return answerArray
	}
	
	func makeDescriptionString(answerArray:[MVPatientAnswers]) -> String
	{

		var descriptionString = ""
	
		if (answerArray.count > 2)
		{
			let numberOfSelections:Int = answerArray.count - 1
			
			if (answerArray[0].description != "")
			{
				if (answerArray[0].description == "Other")
				{
					descriptionString = answerArray[0].customText + " and " + String(describing: numberOfSelections) + " others."
				}
				
				else
				{
					descriptionString = answerArray[0].description + " and " + String(describing: numberOfSelections) + " others."
				}
			}
		}
			
		else if (answerArray.count == 2)
		{
			if (answerArray[0].description != "")
			{
				descriptionString = answerArray[0].description + " and one other."
			}
				
			else if (answerArray[0].description == "Other" && answerArray[0].customText != "")
			{
				descriptionString = answerArray[0].customText + " and one other."
			}
		}
		
		// For answers with only one value
			
		else if (answerArray.count == 1)
		{
			// If the question hasn't been answered, its ID values are zero

			if ((answerArray[0].description == "Other" || answerArray[0].description == "") &&  answerArray[0].customText != "") // Temporary workaround
			{
				descriptionString = answerArray[0].customText
			}
			
			else if (answerArray[0].description != "")
			{
				descriptionString = answerArray[0].description
			}
			
			else if (answerArray[0].description == "")
			{
				descriptionString = "Not answered"
			}
		}
		
		else
		{
			descriptionString = "Not answered"
		}
		
		return descriptionString
	}
	
	func makeCustomTextString(answerArray:[MVPatientAnswers]) -> String
	{
		var descriptionString = ""
		
		// If multiple selections
		
		if (answerArray.count > 1)
		{
			for element in answerArray
			{
				if (element.description == "Other" && element.customText != "")
				{
					descriptionString = element.customText
				}
			}
		}
		
		// If single selection
		
		else if (answerArray.count == 1)
		{
			if (answerArray[0].customText != "" && answerArray[0].description == "Other")
			{
				descriptionString = answerArray[0].customText
			}
			
			else if (answerArray[0].customText == "" && answerArray[0].description == "")
			{
				descriptionString = "Not answered"
			}

			else
			{
				descriptionString = "None"
			}
		}
		
		return descriptionString

	}
}
