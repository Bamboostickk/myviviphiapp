//
//  ReportsCollectionViewCell.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-12-03.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

class ReportsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var label: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
