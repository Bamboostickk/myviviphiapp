//
//  MVRegistrationController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-10-19.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

class MVRegistrationController
{
	
	var errorMessage:String = ""
	
	// Unverified, need to test
	
	func createAccount(theEmail:String, thePassword:String, confirmPassword:String, theUsername:String, onFinish:@escaping (Bool) -> Void)
	{
		let appToken = "appToken=" + SharedResourceManager.sharedInstance.APP_TOKEN
		let postData:NSMutableData =  NSMutableData.init(data: appToken .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		let userEmail = "&Email=" + theEmail
		let userPassword = "&Password=" + thePassword
		let userConfirmPassword = "&ConfirmPassword=" + confirmPassword
		let userName = "&Name=" + theUsername
		
		postData.append(userEmail.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(userPassword.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(userConfirmPassword.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		postData.append(userName.data(using: String.Encoding(rawValue:  String.Encoding.utf8.rawValue))!)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/User"
		
		let fileURL = URL(string:urlString)
		
		var request = URLRequest(url: fileURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 100.0)
		request.httpMethod = "POST"
		request.httpBody = postData as Data
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				
				let httpResponse = (response as! HTTPURLResponse)
				
				if (httpResponse.statusCode == 200)
				{
					
					onFinish(true)
				}
					
				else
				{
					if (httpResponse.statusCode == 502)
					{
						self.errorMessage = "This email has already been taken"
					}
					
					else
					{
						let theErrorMessage:String = String(data: data!, encoding: String.Encoding.utf8)!
						self.errorMessage = theErrorMessage.replacingOccurrences(of: "\"", with: "")
					}
					
					onFinish(false)
				}
			}
		}
		
		dataTask.resume()
	}
	
	func retrieveToken(usernameIn:String, passwordIn:String, onFinish:@escaping (String) -> Void)
	{
		let postData:NSMutableData = NSMutableData.init(data: "grant_type=password" .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		var username = "&username="
		var password = "&password="
		
		username += usernameIn
		password += passwordIn
		
		postData.append(username.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		postData.append(password.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		print("TRYING REGISTER")
		print(username + password)
		
		let dataString:NSString = NSString(data: postData as Data, encoding: String.Encoding.utf8.rawValue)!
		
		let results:String = dataString as String
		
		print(results)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "token"
		
		let fileURL = URL(string:urlString)
		
		var request = URLRequest(url: fileURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
		request.httpMethod = "POST"
		request.httpBody = postData as Data
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)
				
				if (httpResponse.statusCode == 200)
				{
					do
					{
						let resultingData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
						let JSONObject = JSON(resultingData)
						
						SharedResourceManager.sharedInstance.tokenAPI = JSONObject["access_token"].string!
						
						onFinish(JSONObject["access_token"].string!)
					}
						
					catch let error as NSError
					{
						print(error.localizedDescription)
					}
				}
					
				else
				{
					print("ERROR " + String(describing: httpResponse.statusCode))
				}
			}
		}
		dataTask.resume()
	}
}
