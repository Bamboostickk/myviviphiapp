//
//  VCQuestionPostalCode.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-16.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCQuestionPostalCode:UIViewController,UITextFieldDelegate
{
	
	var isEditingQuestion = false
	var nextButtonText = "Next" // Set what text should be used in the top right corner
	
	// Do any additional setup after loading the view, typically from a nib.
	@IBOutlet weak var theNavigationBar: UINavigationBar!
	@IBOutlet weak var textLabel: UILabel!
	@IBOutlet weak var postalCodeEntered: UITextField!
	
	@IBAction func nextButtonPressed(_ sender: AnyObject)
	{
		if (postalCodeEntered.text != nil)
		{
			self.customChoice(userInput: self.postalCodeEntered.text!)
			
			// Called if we're just updating our answer from MyHealth
			if (isEditingQuestion == true)
			{
				let patientIDValue = SharedResourceManager.sharedInstance.patientID
				SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
					DispatchQueue.main.async{
						self.dismiss(animated: true, completion: nil)
					}
				}
			}
				
			// Called if we're in the questionnaire
			else
			{
				
				SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: [self.postalCodeEntered.text!])
				
				if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView
				{
					self.navigationController?.pushViewController(resultController, animated: true)
				}
			}
		}
			
		else
		{
			// TODO: Probably need to get rid of this for handling skipping the question.
			self.displayErrorMessage()
		}
	}
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func cancelButtonPressed(_ sender: AnyObject){
		self.dismiss(animated: true, completion: nil)
	}
	
	override func willMove(toParentViewController parent: UIViewController?)
	{
		super.willMove(toParentViewController: parent)
		
		if parent == nil
		{
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		}
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		// If we're just editing the question answer
		if (isEditingQuestion == true)
		{
			nextButtonText = "Done"
			
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			
			// Set the title label to the current question
			self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText
			
			// Get the previous answer
			let previousText = SharedResourceManager.sharedInstance.patientDataController.patientPostalCode?[0].customText
			
			// Check if the previous answer is empty
			if (previousText?.isEmpty)!{
				// Add Skip button if the previous answer was empty
				self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.nextButtonPressed(_:)))
				
			}
				
				// Otherwise, if the question was answered previously
			else
			{
				// Set the field to contain previous answer
				let postalArray:[String] = SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.retrieveValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID)
				
				if (postalArray.count > 0)
				{
					self.postalCodeEntered.text = postalArray[0]
				}
				
				//Set the next button to Done
				self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: nextButtonText, style: .done, target: self, action: #selector(self.nextButtonPressed(_:)))
			}
			
			// Add Cancel button to the left since we are in edit mode
			self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelButtonPressed(_:)))
		}
			
			// Otherwise, if we're in the questionnaire
		else{
			// Add Skip button by default
			self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.nextButtonPressed(_:)))
			
			// Update Question
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber += 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			
			
			let postalArray:[String] = SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.retrieveValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID)
			
			if (postalArray.count > 0)
			{
				self.postalCodeEntered.text = postalArray[0]
			}
			
		}
		
		postalCodeEntered.delegate = self
		postalCodeEntered.becomeFirstResponder()
		
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VCQuestionPostalCode.dismissKeyboard))
		view.addGestureRecognizer(tap)
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	// Displays error message if nothing is selected
	
	func displayErrorMessage()
	{
		let alertView = UIAlertController(title: "Nothing entered", message: "Please input your ZIP code" as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	// Allows the "Done" button to hide the keyboard
	func textFieldShouldReturn(_ textField: UITextField) -> Bool
	{
		textField.resignFirstResponder()
		return true
	}
	
	// Called when the characters are about to change in the textfield
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
	{
		// Get the new text string
		let newText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
		
		// If the new text is empty add the "Skip" button to the top right corner
		if(newText?.isEmpty)!{
			let nextButton = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.nextButtonPressed(_:)))
			self.navigationItem.setRightBarButton(nil, animated: true)
			self.navigationItem.setRightBarButton(nextButton, animated: true)
			return true // return should update when we have an empty string
		}
			
			// Otherwise if the old text is empty (and the next text wasn't empty from the previous statement) add the "Next" button to the top right corner
		else if(postalCodeEntered.text?.isEmpty)!{
			let nextButton = UIBarButtonItem(title: nextButtonText, style: .done, target: self, action: #selector(self.nextButtonPressed(_:)))
			self.navigationItem.setRightBarButton(nil, animated: true)
			self.navigationItem.setRightBarButton(nextButton, animated: true)
		}
		
		
		// Accept any input
		return true
	}
	
	
	
	func dismissKeyboard()
	{
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
}
