//
//  MVNewsArticle.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-21.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

struct MVNewsArticle
{
	var articleID:Int = 0
	var articleTitle:String = ""
	var articleLink:String = ""
	var articleDate:String = "" // Should be NSDate or equivalent, using string now for simplicity
	var articleSummary:String = ""
	var articleImageLink:String = "" //Links to image, doesn't need to work for now
}
