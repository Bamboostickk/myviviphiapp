//
//  VCEditChecker.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-06.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//


/*****************************************************************/
/*****************************************************************/
/*****************************************************************/
/***************************DEPRECIATED***************************/
/*****************************************************************/
/*****************************************************************/
/*****************************************************************/


import UIKit

class VCEditChecker:UIViewController
{
	var questionsList:[String] = [String]()
	var questionDictionary:[Int:String] = [:]
	
	@IBOutlet weak var theNavigationBar: UINavigationBar!
	@IBOutlet weak var textLabel: UILabel!
	
	@IBAction func YesButtonPressed(_ sender: AnyObject)
	{
		if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditTableLanding") as? UINavigationController {
			present(resultController, animated: true, completion: nil)
		}
	}
	
	@IBAction func NoneButtonPressed(_ sender: AnyObject)
	{
		self.writeChoice(buttonValue: "None")
		let patientIDValue = SharedResourceManager.sharedInstance.patientID
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
			DispatchQueue.main.async{
				self.dismiss(animated: true, completion: nil)
			}
		}
	}
	
	
	@IBAction func NotSureButtonPressed(_ sender: AnyObject)
	{
		self.writeChoice(buttonValue: "I\'m not sure")
		let patientIDValue = SharedResourceManager.sharedInstance.patientID
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
			DispatchQueue.main.async{
				self.dismiss(animated: true, completion: nil)
			}
		}
	}
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()

		self.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		theNavigationBar.topItem?.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText!
		
		self.obtainIDValuesForNoneOrNotSure()
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	func obtainIDValuesForNoneOrNotSure()
	{
		let question:String = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!

		switch question
		{
		case "Tumor Sequencing":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfMarkers!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfMarkers!
		case "Genomic Alterations":
			questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfAlterations!.values).sorted(by: <)
			questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfAlterations!
		default:
			break
		}
	}
	
	func writeChoice(buttonValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
			
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: true)
	}
}
