//
//  MVPatient.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-21.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import UIKit

struct MVPatient
{
	var patientID:Int = 0
	var patientName:String = ""
	var patientGender:String = ""
	var patientAge:Int = 0
	var patientCancer:String = ""
	var patientPercent = 0
	var patientImage:UIImage = #imageLiteral(resourceName: "UserIcon")
}
