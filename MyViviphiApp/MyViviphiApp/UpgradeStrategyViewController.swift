//
//  UpgradeStrategyViewController.swift
//  MyViviphiApp
//
//  Created by Sasha Ivanov on 2017-01-06.
//  Copyright © 2017 Wellness Computational. All rights reserved.
//

import UIKit
import StoreKit

class UpgradeStrategyViewController: UIViewController,SKProductsRequestDelegate, SKPaymentTransactionObserver {

	
	var product: SKProduct?	// The In-App Purchase "Product"
	var productID = "The in app purchase product ID"		// TODO: Create an itunesconnect in app purchase and put the id here
	
	
	@IBOutlet weak var unlockButton: FilledButton!
	
    @IBAction func unlockButtonPressed(_ sender: Any) {
		
		// If the user cannot make in app purchases (for example parental controls could prevent this)
		if !SKPaymentQueue.canMakePayments(){
			// Show an alert, asking the user to enable in-app purchases
			let alert = UIAlertController(title: "In-App Purchases Disabled", message: "Please enable In-App Purchases in Settings and try again.", preferredStyle: .alert)
			
			self.present(alert, animated: true, completion: nil)
		}
		
		// Otherwise create the payment popup for the product if it exists
		else if (product != nil){
			// Add a payment process to the payment queue
			// Note: at this point, no purchase has been made yet
			let payment = SKPayment(product: product!)
			SKPaymentQueue.default().add(payment)
		}
    }
	
	// If the user needs to "restore" their in app strategy purchase
	// try to automatically restore it
	// NOTE: THIS BUTTON IS REQUIRED BY APPLE FOR APP SUBMISSION
	@IBAction func restorePurchaseButtonPressed(_ sender: Any) {
		SKPaymentQueue.default().restoreCompletedTransactions()
	}
	
	@IBAction func cancelButtonPressed(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}

	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		// Add this class as a payment observer
		SKPaymentQueue.default().add(self)
		
		/*
		// Try to restore the in app purchase automatically if needed? (commented out for now)
		if tryInAppPurchaseRestore{
			SKPaymentQueue.default().restoreCompletedTransactions()
		}
		*/
		
		getProductInfo()

	}
	
	// Creates a request to get the treatment strategy in-app purchase from itunesconnect
	func getProductInfo(){
		// Check if the user's in app purchasing is enabled
		if SKPaymentQueue.canMakePayments() {
			
			// Request the in app purchase from iTunes connect
			let request = SKProductsRequest(productIdentifiers: NSSet(objects: self.productID) as! Set<String>)
			request.delegate = self
			request.start()
		}
	}
	
	// The request response when we request the in app purchase from itunesconnect
	func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
		var products = response.products
		
		// Save the in app purchase "product"
		if !products.isEmpty{
			product = products[0]	// Save the product
			unlockButton.titleLabel?.text = "Unlock Now for $" + String(describing: product?.price)	// Update the price label
		}
		
		// A simple for loop that prints any invalid In App Purchase Product IDs
		for product in response.invalidProductIdentifiers
		{
			print("Product not found: \(product)")
		}
	}
	
	// Called after a user has decided to pay, or not
	func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
		
		// Go through each transaction made (will only be one for the report unlock)
		for transaction in transactions {
			
			switch transaction.transactionState {
			
			// The user ended up purchasing the full strategy
			case SKPaymentTransactionState.purchased:
				unlockFullStrategy()
				SKPaymentQueue.default().finishTransaction(transaction)
				
			// The user did not end up purchasing the strategy
			case SKPaymentTransactionState.failed:
				SKPaymentQueue.default().finishTransaction(transaction)
			default:
				break
			}
		}
	}

	
	
	// The code to actually unlock the full strategy in the app
	func unlockFullStrategy(){
		// TODO: Do something to unlock the strategy
		self.dismiss(animated: true, completion: nil)
	}
	
}
