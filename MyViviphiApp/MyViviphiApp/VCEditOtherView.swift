//
//  VCEditOtherView.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-05.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

/*****************************************************************/
/*****************************************************************/
/*****************************************************************/
/***************************DEPRECIATED***************************/
/*****************************************************************/
/*****************************************************************/
/*****************************************************************/


import UIKit
import Foundation

class VCEditOtherView:UIViewController
{
	
	var allowsMultipleSelection:Bool? = false
	var replies:[String] = [String]()
	var forActiveQuestionID:Int = SharedResourceManager.sharedInstance.questionController.activeQuestionID
	var answerFromPatient:[MVPatientAnswers] = []
	
	@IBOutlet weak var userTextField: UITextView!
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		//SharedResourceManager.sharedInstance.patientOtherInputs.lastEntry = self.userTextField.text
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func doneButtonPressed(_ sender: AnyObject)
	{
		if (userTextField.text == "")
		{
			self.displayErrorMessage(theTitle: "Nothing entered", theMessage: "Please enter something")
		}
			
		else
		{
			self.storeTextFieldValues(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber)
			self.customChoice(userInput: userTextField.text)
		
			SharedResourceManager.sharedInstance.patientOtherInputs.lastEntry = self.userTextField.text
			
			self.dismiss(animated: true, completion:nil)
			
			if (self.allowsMultipleSelection == false)
			{
				let presentingVC = self.presentingViewController
				self.dismiss(animated: true, completion: {presentingVC?.dismiss(animated: true, completion: nil)})
			}
			
			else
			{
				self.dismiss(animated: true, completion: nil)
			}
		}
	}
	
	
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		self.setupTextField(questionNumber: SharedResourceManager.sharedInstance.questionController.activeQuestionNumber)
		
		userTextField.becomeFirstResponder()

		//let colourGrey = UIColor(white: 0.85, alpha: 1)
		//userTextField.layer.backgroundColor = colourGrey.cgColor
		
		super.viewDidLoad()
		
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VCQuestionAge.dismissKeyboard))
		self.userTextField.becomeFirstResponder()
		view.addGestureRecognizer(tap)
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	// Determines what kind of view controller to load next based on the question ID
	// Only useful if the questions are in a particular order.
	// We can write an algorithm that forces a particualr scene to load based on what the question itself is
	
	func loadNextScreen()
	{
		if (SharedResourceManager.sharedInstance.questionController.nextQuestionID == 2)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AgeView") as? VCQuestionAge {
				present(resultController, animated: true, completion: nil)
			}
		}

		else if (SharedResourceManager.sharedInstance.questionController.nextQuestionID   == 11)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "PostalCodeView") as? VCQuestionPostalCode {
				present(resultController, animated: true, completion: nil)
			}
		}
			
			// If the next question number is larger than the total number of questions, the next question doesn't exist.
			
		else if (SharedResourceManager.sharedInstance.questionController.activeQuestionNumber + 1 > SharedResourceManager.sharedInstance.questionController.maximumQuestionNumber)
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "QuestionnaireFinished") as? VCQuestionnaireFinished {
				present(resultController, animated: true, completion: nil)
			}
		}
			
		else
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonView") as? VCQuestionButtonView {
				present(resultController, animated: true, completion:nil)
			}
		}
	}
	
	//Fix bug where question jumps when using this
	
	func storeTextFieldValues(questionNumber:Int)
	{
		//let userInputText:String = userTextField.text
		//self.replies = userInputText.components(separatedBy: ",")
		switch questionNumber
		{
		case 3:
			 SharedResourceManager.sharedInstance.patientOtherInputs.cancer = self.userTextField.text
		case 4:
			SharedResourceManager.sharedInstance.patientOtherInputs.histology = self.userTextField.text
		case 5:
			SharedResourceManager.sharedInstance.patientOtherInputs.stage = self.userTextField.text
		case 6:
			SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers = self.userTextField.text
		case 7:
			SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations = self.userTextField.text
		case 8:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions = self.userTextField.text
		case 9:
			SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications = self.userTextField.text
		case 10:
			SharedResourceManager.sharedInstance.patientOtherInputs.allergies = self.userTextField.text
		case 11:
			SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity = self.userTextField.text
		case 13:
			SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer = self.userTextField.text
		default:
			self.userTextField.text = ""
			break
		}
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	func dismissKeyboard()
	{
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
	
	// Displays error message if nothing is entered
	
	func displayErrorMessage(theTitle:String, theMessage:String)
	{
		let alertView = UIAlertController(title: theTitle, message: theMessage as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	func setupTextField(questionNumber:Int)
	{
		switch questionNumber
		{
		case 3:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.cancer
		case 4:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.histology
		case 5:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.stage
		case 6:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.tumourMarkers
		case 7:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.geneticAlterations
		case 8:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.otherConditions
		case 9:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.otherMedications
		case 10:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.allergies
		case 11:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.ethnicity
		case 13:
			self.userTextField.text = SharedResourceManager.sharedInstance.patientOtherInputs.healthInsurer
		default:
			self.userTextField.text = ""
			break
		}
	}
}
