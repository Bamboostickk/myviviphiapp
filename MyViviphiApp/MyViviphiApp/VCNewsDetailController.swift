//
//  VCNewsDetailController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-21.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCNewsDetailController: UIViewController
{
	var articlePath:String = ""
	
	@IBOutlet weak var webView: UIWebView!
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		let linkToArticle:URL = URL(string: articlePath)!
		webView.loadRequest(URLRequest(url: linkToArticle))
		
		// Add share button to navigation bar
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: nil, action: nil)
		
		
		self.edgesForExtendedLayout = [.top]
		
	}
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
    
    @IBAction func backButtonPressed(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
