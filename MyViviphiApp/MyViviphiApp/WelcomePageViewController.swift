//
//  WelcomePageViewController.swift
//  MyViviphiApp
//
//  Created by Sasha Ivanov on 2016-12-11.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class WelcomePageViewController: UIPageViewController, UIPageViewControllerDataSource,UIPageViewControllerDelegate {


	var pages:[UIViewController] = []
	
    override func viewDidLoad() {
        super.viewDidLoad()

		self.dataSource = self
		self.delegate = self

		let page1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WelcomeContent1")
		let page2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WelcomeContent2")
		//let page3 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WelcomeContent3")

		pages.append(page1)
		pages.append(page2)
		//pages.append(page3)
		

		setViewControllers([pages[0]], direction: .forward, animated: true, completion: nil)
		
		
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	
	// MARK: - DataSource
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		
		let currentIndex = pages.index(of: viewController)!
		
		if currentIndex == 0{
			return nil
		}
		
		return pages[currentIndex-1]
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
		
		let currentIndex = pages.index(of: viewController)!
		
		if currentIndex == pages.count-1{
			return nil
		}
		
		return pages[currentIndex+1]
	}
	
	
	// MARK: - Delegate
	
	func presentationCount(for pageViewController: UIPageViewController) -> Int {
		return self.pages.count
	}
	
	func presentationIndex(for pageViewController: UIPageViewController) -> Int {
		return 0
	}
	

	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
	
	
}
