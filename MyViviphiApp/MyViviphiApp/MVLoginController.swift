//
//  MVLoginController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-10-19.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

/**		This class handles the logic for connecting to the Viviphi server and logging in.
 		The VCLoginScreenViewController will have one instance of this class
 */

class MVLoginController
{
	var attributeDictionary:NSDictionary? = ["":""]	// Empty dictionary to return if login fails

	var connectionSuccessful:Bool?						// Could the server be reached?
	var loginSuccessful:Bool?									// Was the user information correct?
	var registrationSuccessful:Bool?						// Was registration successful?
	
	// Makes the HTTP request to the server. This value is hard-coded, so if the server URL changes, the name should be updated, as well
	// This method constructs the request string as NSData, which passes in the username and password
	// This information is then sent to a URL request, which the method returns.
	
	func makeHTTPRequestWith(theUsername:String, thePassword:String) -> URLRequest
	{
		let headers:Dictionary = ["content-type": "application/x-www-form-urlencoded", "cache-control": "no-cache", "postman-token": "f42d8693-3a9b-2d8b-9583-717ed7ed2895"]
		
		let postData:NSMutableData =  NSMutableData.init(data: "grant_type=password" .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)

		var username = "&username="
		var password = "&password="
		
		username += theUsername
		password += thePassword
		
		postData.append(username.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		postData.append(password.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "token"
		
		let fileURL = URL(string:urlString)
		let container = HTTPCookieStorage.shared
		let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers, for: fileURL!)
		container.setCookies(cookies, for: fileURL, mainDocumentURL: fileURL)
		
		var request = URLRequest(url: fileURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
		request.httpMethod = "POST"
		request.httpBody = postData as Data
		
		return request
		
	}
	
	// Starts the URL session with the URL request, and uses a completion handler closure to return a dictionary
	// If the connection is successful based on the login credentials, a dictionary carrying API token is returned, plus the username
	// Otherwise, an empty dictionary is returned.
	// Boolean flags are also altered to determine whether or not a connection was successful, as well as if a login was successful
	
	func startSessionWith(request:URLRequest, onFinish:@escaping (NSDictionary) -> Void)
	{
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
				self.connectionSuccessful = false;
			}
				
			else
			{
				
				self.connectionSuccessful = true;
				let httpResponse = (response as! HTTPURLResponse)
				
				if (httpResponse.statusCode == 200)
				{	
					do
					{
						if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
						{
							self.loginSuccessful = true
							print(convertedJsonIntoDict)
							onFinish(convertedJsonIntoDict)
						}
					}
						
					catch let error as NSError
					{
						print(error.localizedDescription)
					}
				}
				
				else
				{
					self.loginSuccessful = false
					onFinish(self.attributeDictionary!)
				}
			}
		}
		
		dataTask.resume()
	}
	
	// Updates the local dictionary. The VCLoginScreenViewController can call this method after the connection is made.
	
	func updateLocalDictionary(theDictionary:NSDictionary)
	{
		attributeDictionary = theDictionary
	}
}
