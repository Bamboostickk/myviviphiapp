//
//  VCOverlayProgressBar.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-13.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import UIKit

class VCOverlayProgressBar
{
	static var progressBar:UIProgressView?
	static var currentOverlay:UIView?
	
	static var session:URLSession?
	
	static var taskBytesWritten = 0
	static var taskBytesTotal = 0
	static var percentage:Float = 0.0
	
	
	
	static func progressIndicator(_ overlayTarget:UIView, loadingText:String?)
	{
		// Clear it first in case it was already shown
		hide()
		
		// Create the overlay
		let overlay = UIView(frame: overlayTarget.frame)
		overlay.center = overlayTarget.center
		overlay.alpha = 0
		overlay.backgroundColor = UIColor.black
		overlayTarget.addSubview(overlay)
		overlayTarget.bringSubview(toFront: overlay)
		
		// Create and animate the activity indicator
		progressBar?.setProgress(0.0, animated: true)
		progressBar?.center = overlay.center
		overlay.addSubview(progressBar!)
		
		// Create label
		if let textString = loadingText
		{
			let label = UILabel()
			label.text = textString
			label.textColor = UIColor.white
			label.sizeToFit()
			label.center = CGPoint(x: (progressBar?.center.x)!, y: (progressBar?.center.y)! + 30)
			overlay.addSubview(label)
		}
	}
	
	static func hide()
	{
		if (currentOverlay != nil)
		{
			currentOverlay?.removeFromSuperview()
			currentOverlay =  nil
		}
	}
}
