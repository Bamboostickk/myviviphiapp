//
//  VCPDFViewController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-14.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCPDFViewController: UIViewController, UIWebViewDelegate
{
	var thePDFPath:String = ""
	var hasPaid:Bool = false		// for storing if the user has paid / upgraded to a full strategy
	
	
	@IBOutlet weak var webView: UIWebView!
	
	@IBAction func shareButtonPushed(_ sender: Any)
	{
		let documentController = UIDocumentInteractionController(url: (URL(string:thePDFPath)!))
		documentController.presentOptionsMenu(from: self.view.frame, in: self.view, animated: true)
	}
	
	@IBAction func upgradeButtonPushed(_ sender: Any)
	{
		// Show the upgrade features page
		if let resultController = storyboard!.instantiateViewController(withIdentifier: "upgradeNavigationController") as? TransparentNavigationController {
		
			// Present the navigation controller (with upgrade feature page inside)
			present(resultController, animated: true, completion: nil)
		}
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
        
		let pathToPDF:URL = URL(string: thePDFPath)!
		webView.loadRequest(URLRequest(url: pathToPDF))
		
		// Add share button to navigation bar
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(self.shareButtonPushed(_:)))
		
		// If the user has not upgraded the strategy
		if(!hasPaid){
			
			// Show toolbar at the bottom of the screen
			self.navigationController?.setToolbarHidden(false, animated: false)
			let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
			let showSelectedButton = UIBarButtonItem(title: "Upgrade Treatment Strategy", style: .done, target: self, action: #selector(upgradeButtonPushed))
			self.setToolbarItems([flexibleSpace, showSelectedButton, flexibleSpace], animated: true)
			
			self.edgesForExtendedLayout = [.top,.bottom]
		}
		
		// Otherwise when the user has paid for the strategy
		else{
			self.edgesForExtendedLayout = [.top]
		}
	}

	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		
	}

	
}

