//
//  MyHealthTabCollectionViewController.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-12-03.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit
import QuickLook

private let reuseIdentifier = "cell"

class MyHealthTabCollectionViewController: UICollectionViewController,QLPreviewControllerDelegate,QLPreviewControllerDataSource {
    
    
    var patientNames = ["Billy Bob", "Jim James", "Suzy Santiago", "Lilly Miller"]
    
    var patientImages = [#imageLiteral(resourceName: "Image"),#imageLiteral(resourceName: "Image"),#imageLiteral(resourceName: "Image"),#imageLiteral(resourceName: "Image")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return patientNames.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ReportsCollectionViewCell
    
        cell.label.text = patientNames[indexPath.row]

        cell.imageView.image = patientImages[indexPath.row]
        cell.imageView.layer.cornerRadius = cell.imageView.frame.width/2
        cell.imageView.clipsToBounds = true
        
        return cell
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ReportsCollectionViewCell
        cell.imageView.layer.opacity = 0.5
    }
    
    override func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ReportsCollectionViewCell
        cell.imageView.layer.opacity = 1.0
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ReportsCollectionViewCell
        showOpenOrUpdateActionSheet(patientName: cell.label.text!)
    }
    
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

    
    func showOpenOrUpdateActionSheet(patientName:String){
        let addSectionController = UIAlertController(title: patientName + "'s Genomic Strategy", message: nil, preferredStyle: .actionSheet)
        
        let viewAction = UIAlertAction(title: "View Strategy", style: .default) { (action) in
            // set the segue to the next view like this
            //self.performSegue(withIdentifier: "segue", sender: nil)
            
            
            //let url = NSURL(fileURLWithPath: "http://www.cic.gc.ca/english/pdf/kits/forms/IMM5476E.pdf")
            //let documentController = UIDocumentInteractionController(url: url as URL)
            //documentController.presentOptionsMenu(from: self.view.frame, in: self.view, animated: true)
            
            let preview = QLPreviewController()
            preview.dataSource = self
            self.present(preview, animated: true, completion: nil)
            
        }
        
        let updateAction = UIAlertAction(title: "Update Patient Information", style: .default) { (action) in
            // set the segue to the next view like this
            //self.performSegue(withIdentifier: "segue", sender: nil)
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // ...
            
        }
        
        addSectionController.addAction(viewAction)
        addSectionController.addAction(updateAction)
        addSectionController.addAction(cancelAction)
        
        self.present(addSectionController, animated: true) {
            // ...
        }
    }
    
    

    
    // MARK: Quicklook
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        let url = NSURL(fileURLWithPath: "http://www.cic.gc.ca/english/pdf/kits/forms/IMM5476E.pdf")
        return url
    }
    
    
    
}
