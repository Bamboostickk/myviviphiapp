//
//  DonutView.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-11-16.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

class DonutView: UIView {
    
    var percentage:Double = 0.25    // The percentage of the donut that is complete
    var lineWidth:CGFloat = 6      // The width of the donut

    var incompleteColor:CGColor = UIColor(colorLiteralRed: 221/255, green: 221/255, blue: 221/255, alpha: 1.0).cgColor
    var completeColor:CGColor = UIColor(colorLiteralRed: 221/255, green: 221/255, blue: 221/255, alpha: 1.0).cgColor
    var textColor:CGColor = UIColor.gray.cgColor
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isOpaque = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func draw(_ rect: CGRect) {
        
        // Calculate the start and end position of the donut in rads
        let start = CGFloat(M_PI_2 + M_PI)
        let end = start + CGFloat(M_PI*2*percentage)
        
        // Draw a full circle of the incomplete color
        drawRing(from: 0, to: CGFloat(M_PI*2), color: incompleteColor, withAnimation: false)
        
        // Draw the complete color arc
        drawRing(from: start, to: end, color: completeColor, withAnimation: true)
        
        // Draw the percentage complete text
        drawPercentageText(size:45, color:textColor)

        // Make Background transparent
        //self.backgroundColor = UIColor.clear
    }
    
    override func tintColorDidChange() {
        completeColor = self.tintColor.cgColor
    }
    
    
    func drawRing(from:CGFloat,to:CGFloat, color:CGColor, withAnimation:Bool){
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: self.frame.width/2,y: self.frame.height/2), radius: self.frame.width/2, startAngle: from, endAngle: to, clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        // Hide the Ring's Fill
        shapeLayer.fillColor = UIColor.clear.cgColor
        
        // Add the Ring's Stroke
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = lineWidth
        
        
        // Animate the Ring View
        if withAnimation {
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.duration = 0.6
            animation.repeatCount = 1.0
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            animation.fromValue = 0.0
            animation.toValue = 1.0
            //animation.beginTime = CACurrentMediaTime() + 0.3
            shapeLayer.add(animation, forKey: "drawCircleAnimation")
        }
        
        // Add the Ring to the View
        self.layer.addSublayer(shapeLayer)
        

    }
    
    func drawPercentageText(size:CGFloat, color:CGColor){
        
        // Create the font
        let font = UIFont.systemFont(ofSize: size, weight: UIFontWeightLight)

        // Setup the Text Layer
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: font.lineHeight)
        textLayer.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        
        // Set the String's Formatting in Text Layer
        textLayer.string = String(Int(percentage*100)) + "%"
        textLayer.font = font
        textLayer.fontSize = font.pointSize
        textLayer.alignmentMode = kCAAlignmentCenter
        textLayer.foregroundColor = color
        
        // IMPORTANT: Set the scale of the text so it isn't blurry on retina displays!
        textLayer.contentsScale = UIScreen.main.scale

        // Add the Text to the View
        self.layer.addSublayer(textLayer)
		
		drawText(text: "Strategy", size: size/3, color: color, position: CGPoint(x: self.frame.width/2, y: self.frame.height/2 - font.lineHeight/2 - 2))
		drawText(text: "Complete", size: size/3, color: color, position: CGPoint(x: self.frame.width/2, y: self.frame.height/2 + font.lineHeight/2 + 2))
		

    }
	
	func drawText(text:String, size:CGFloat, color:CGColor, position:CGPoint){
		
		// Create the font
		let font = UIFont.systemFont(ofSize: size)
		
		// Setup the Text Layer
		let textLayer = CATextLayer()
		textLayer.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: font.lineHeight)
		textLayer.position = position
		
		// Set the String's Formatting in Text Layer
		textLayer.string = text
		textLayer.font = font
		textLayer.fontSize = font.pointSize
		textLayer.alignmentMode = kCAAlignmentCenter
		textLayer.foregroundColor = color
		
		// IMPORTANT: Set the scale of the text so it isn't blurry on retina displays!
		textLayer.contentsScale = UIScreen.main.scale
		
		// Add the Text to the View
		self.layer.addSublayer(textLayer)
		
	}


}
