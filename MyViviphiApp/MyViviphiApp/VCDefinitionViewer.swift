//
//  VCDefinitionViewer.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-09.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import UIKit

class VCDefinitionViewer: UIViewController
{
	
	var termText:String = ""
	var definitionText:String = ""
	
	@IBOutlet weak var TermLabel: UILabel!
	@IBOutlet weak var DefinitionTextView: UITextView!
	
	override func viewDidLoad()
	{
        super.viewDidLoad()

		TermLabel.text = termText
        TermLabel.sizeToFit()
        
        
		DefinitionTextView.text = definitionText
        
        // Ensures the text is aligned at the top
        self.automaticallyAdjustsScrollViewInsets = false
        
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
}
