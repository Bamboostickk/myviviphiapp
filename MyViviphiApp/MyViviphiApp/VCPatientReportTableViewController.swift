//
//  VCPatientReportTableViewController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-10-25.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

/*	This class controls the table that depicts all of the patient reports available for the logged in account

*/

class VCPatientReportTableViewController: UITableViewController
{
	var patientList:[MVPatient] = []
	var patientNames:[String] = [String]()
	var patientImages:[UIImage] = [UIImage]()
	 let keychain = KeychainWrapper()
	
	@IBAction func addButtonPressed(_ sender: Any)
	{
		showNamePatientAlert()
	}
	
	
	@IBAction func backButtonPressed(_sender: AnyObject)
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func logoutButtonPressed(_ sender: Any) {
		
		// From Original Collection View Controller (Currently not working?)
		UserDefaults.standard.removeObject(forKey: "accessToken")
		UserDefaults.standard.removeObject(forKey: "hasLoginKey")
		UserDefaults.standard.removeObject(forKey: "ActivePatientID")
		UserDefaults.standard.synchronize()
		
		SharedResourceManager.sharedInstance.patientID = 0
		SharedResourceManager.sharedInstance.activePatient = 0
		SharedResourceManager.sharedInstance.tokenAPI = ""
		
		SharedResourceManager.sharedInstance.questionController.resetAll()
		
		if let resultController = storyboard!.instantiateViewController(withIdentifier: "LoginScene") as? TransparentNavigationController
		{
			present(resultController, animated: true, completion: nil)
		}
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		// Add "Done" button as left bar button
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.backButtonPressed(_sender:)))
		
		// make toolbar non translucent
		self.navigationController?.toolbar.isTranslucent = false
		
		// Add + button as right bar button
		//self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed(_:)))
		
		// Make sure the bottom toolbar is visible
		self.navigationController?.isToolbarHidden = false
		
		// From original Collection View Controller
		if (SharedResourceManager.sharedInstance.checkIfAppOnline() == true)
		{
			populateReportsView()
		}
			
		else
		{
			let halfWidth:CGFloat = UIScreen.main.bounds.width * CGFloat(0.5) - CGFloat(200)
			let halfHeight:CGFloat = UIScreen.main.bounds.height * CGFloat(0.5)
			let message:UILabel = UILabel(frame: CGRect(x:halfWidth, y:halfHeight, width:400, height:45))
			
			message.textAlignment =  .center
			message.text = "Please connect to WiFi to access your reports"
			self.view.addSubview(message)
		}		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		return patientList.count
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PatientManagerTableViewCell
		
		if (patientList.count > 0)
		{
			cell.patientName.text = patientList[indexPath.row].patientName
			cell.userImage.image = patientList[indexPath.row].patientImage //patientImages[indexPath.row]
			
			// Configure the cell...
		}
		
		else
		{
			cell.patientName.text = "No reports available."
		}
		
		return cell
	}
	
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		VCOverlayController.show(UIApplication.shared.keyWindow!, loadingText: "Loading data")
		// Reload the landing view
		SharedResourceManager.sharedInstance.patientID = patientList[indexPath.row].patientID
		UserDefaults.standard.setValue(patientList[indexPath.row].patientID as Any, forKey: "ActivePatientID")
		UserDefaults.standard.synchronize()
		let patientIDValue:Int = patientList[indexPath.row].patientID
		
		SharedResourceManager.sharedInstance.questionController.resetQuestionNumbers()
		
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
			
			DispatchQueue.main.async{
				//self.present(resultController, animated: true, completion: nil)
				self.dismiss(animated: true, completion: nil)	
				VCOverlayController.hide()
			}
		}
		
		
	}
	
	
	/*
	// Override to support conditional editing of the table view.
	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
	// Return false if you do not want the specified item to be editable.
	return true
	}
	*/
	
	/*
	// Override to support editing the table view.
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
	if editingStyle == .delete {
	// Delete the row from the data source
	tableView.deleteRows(at: [indexPath], with: .fade)
	} else if editingStyle == .insert {
	// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
	}
	*/
	
	/*
	// Override to support rearranging the table view.
	override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
	
	}
	*/
	
	/*
	// Override to support conditional rearranging of the table view.
	override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
	// Return false if you do not want the item to be re-orderable.
	return true
	}
	*/
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
	// MARK: - Original Collection View Controller Functions
	// (Adapted slightly for tableview)
	
	override func viewDidAppear(_ animated: Bool)
	{
		self.patientList = SharedResourceManager.sharedInstance.patientDataController.patientsList
		SharedResourceManager.sharedInstance.patientOtherInputs.resetAll()
		self.tableView?.reloadData()
	}
	
	
	func generatePatientNames()
	{
		var unsortedPatients:[String] = [String]()
		
		for patient in self.patientList
		{
			unsortedPatients.append(patient.patientName)
			//self.patientImages.append(#imageLiteral(resourceName: "UserIcon"))
		}
		
		self.patientNames = unsortedPatients.sorted(by: <)
		
	}
		
	func populateReportsView()
	{
		if (self.patientList.count == 0)
		{
			VCOverlayController.show(UIApplication.shared.keyWindow!, loadingText: "Loading your reports...")
		}
		
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfPatientsFromServerWithDelay(){patientData -> () in
			self.patientList = patientData
			self.generatePatientNames()
			DispatchQueue.main.async
				{
					print("reloading")
					self.tableView.reloadData()
					VCOverlayController.hide()
			}
		}
	}
	
	func showNamePatientAlert()
	{
		
		// Create the elements in the alert
		var projectNameField: UITextField?
		let nameProjectAlert = UIAlertController(title: "Patient Name", message: nil, preferredStyle: UIAlertControllerStyle.alert)
		nameProjectAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
		nameProjectAlert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { (action) -> Void in
			
			// Set the default name of a alert
			if(projectNameField?.text == nil){
				projectNameField?.text = "New Patient"
			}
			// ************************************************************************************
			// TODO Create a new patient then go to the donut view
			// ************************************************************************************
		}))
		
		// Add the elements to the alert
		nameProjectAlert.addTextField(configurationHandler: {(textField: UITextField!) in
			textField.placeholder = "New Patient"
			projectNameField = textField
		})
		
		// Show the alert
		present(nameProjectAlert, animated: true, completion: nil)
		
	}
}
