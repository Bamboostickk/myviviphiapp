//
//  MVNewsController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-21.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

class MVNewsController
{
	var listOfArticles:[MVNewsArticle] = []
	
	func getListOfArticles(onFinish:@escaping ([MVNewsArticle]) -> Void)
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/ArticleSummary?startAt=0&range=20&filtered=true"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				//self.listOfCancers![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
				var newsObject:MVNewsArticle = MVNewsArticle()
				
				newsObject.articleID = JSONObject[index]["Id"].intValue
				newsObject.articleTitle = JSONObject[index]["Title"].string!
				newsObject.articleLink = JSONObject[index]["CitationLink"].string!
				newsObject.articleDate = JSONObject[index]["FormatDate"].string!
				newsObject.articleSummary = JSONObject[index]["PreviewText"].string!
				
				self.listOfArticles.append(newsObject)
			}
			
			onFinish(self.listOfArticles)
		}
	}
}
