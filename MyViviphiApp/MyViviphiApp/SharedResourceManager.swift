//
//  SharedResourceManager.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-10-27.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

class SharedResourceManager
{
	static let sharedInstance = SharedResourceManager()
	let URL_PATH = "https://viviphibeta.azurewebsites.net/"
	let APP_TOKEN:String = "APP_BBC58B0D403C4831852D8A2463CE9ED4"
	
	let MyKeychainWrapper  = KeychainWrapper()
	let internetChecker = MVConnectionCheckController()
	let serverRequestController = MVServerCommunicationController()
	let questionController = MVQuestionController()
	let cancerDictionary = MVCancerDictionaryController()
	let reportDownloader = MVReportDownloader()
	var patientDataController = MVPatientDataController() //This is a temporary class for holding onto patient data while a questionnaire is being completed or modified
	var patientQuestionnaireAnswers = MVQuestionnaireAnswers() // For holding onto answers while a new questionnaire is made
	var patientOtherInputs = MVOtherEntries()
	
	var newsController = MVNewsController()
	
	var tokenAPI:String?
	var username:String?
	
	var patientID:Int =  0 //2363 // Value for active patient ID will be overwritten when either new questionnaire created or when editing
	var activePatient:Int = 0
	
	// For automatically showing the treatment strategy after the questionnaire has finished
	var shouldAutomaticallyShowStrategy = false
	
	func prepareCancerDictionary()
	{
		self.cancerDictionary.prepareDictionary()
	}
	
	func performStartupActions()
	{
		//self.activeReportStorage.updatePListFromServer()
		self.patientDataController.obtainListOfPatientsFromServer()
		self.internetChecker.retrieveToken()
		if let patientIDFromStorage = (UserDefaults.standard.value(forKey: "ActivePatientID") as? Int)
		{
			patientID = patientIDFromStorage
			print("USING ID " + String(describing: patientID))
		}
		
		
	}
	
	func checkIfAppOnline() -> Bool
	{
		return self.internetChecker.isConnectionAvailble()
	}
}
