//
//  MVTableEntry.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-09.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

struct MVTableEntry
{
	var codeID:Int = 0
	var description:String = ""
	var isSelected:Bool = false
	var isPair:Bool = false
	var pairedWith:String = ""
}
