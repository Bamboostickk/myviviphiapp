//
//  VCQuestionnaireFinished.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-21.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit
import QuickLook


class VCQuestionnaireFinished:UIViewController, QLPreviewControllerDelegate,QLPreviewControllerDataSource
{
	
	var localReportURL:String = ""
	
	@IBAction func makeStandardOfCare(_ sender: AnyObject)
	{
	}
	
	@IBAction func makePersonalisedCare(_ sender: AnyObject)
	{
		
		/*
		VCOverlayController.show(self.view, loadingText: "Loading your report...")
		
		UserDefaults.standard.set(SharedResourceManager.sharedInstance.patientID, forKey: "ActivePatientID")
		UserDefaults.standard.synchronize()
		
		SharedResourceManager.sharedInstance.serverRequestController.retrievePDFForPatient(patientID: String(describing: SharedResourceManager.sharedInstance.patientID)){fileURL -> () in
			DispatchQueue.main.async
				{
					let stringURL:String = "\(fileURL)"
					
					self.localReportURL = stringURL
					
					if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewerManual") as? VCPDFViewManualController {
						resultController.thePDFPath = stringURL
						self.present(resultController, animated: true, completion: nil)
						VCOverlayController.hide()
					}
			}
		}
		*/
		
		SharedResourceManager.sharedInstance.shouldAutomaticallyShowStrategy = true
		SharedResourceManager.sharedInstance.questionController.resetQuestionNumbers()
		
		let patientIDValue:Int = SharedResourceManager.sharedInstance.patientID
		
		UserDefaults.standard.set(SharedResourceManager.sharedInstance.patientID, forKey: "ActivePatientID")
		UserDefaults.standard.synchronize()
		
		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.resetQuestionnaireInstance()
		
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
			DispatchQueue.main.async{
				self.dismiss(animated: true, completion: { _ in
				})
			}
		}
		/*
		self.dismiss(animated: true, completion: { _ in
			
			/*
			//if let presentingController = self.presentingViewController as? VCPatientSummaryLanding{
				VCOverlayController.show(UIApplication.shared.keyWindow!, loadingText: "Retrieving your report")
				SharedResourceManager.sharedInstance.serverRequestController.retrievePDFForPatient(patientID: String(describing: SharedResourceManager.sharedInstance.patientID)){fileURL -> () in
					DispatchQueue.main.async
						{
							let stringURL:String = "\(fileURL)"
							
							let localReportURL = stringURL
							
							if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewer") as? VCPDFViewController
							{
								resultController.thePDFPath = stringURL
								//UIApplication.shared.keyWindow?.rootViewController?.navigationController?.pushViewController(resultController, animated: true)
								VCOverlayController.hide()
								
								self.navigationController?.popToRootViewController(animated: true)
								//self.present(resultController, animated: true, completion: nil)
								//self.navigationController?.pushViewController(resultController, animated: true)
							}
					}
				}

			//}*/
			
			})
		*/
	}
	
	@IBAction func returnToApp(_ sender: AnyObject)
	{
		SharedResourceManager.sharedInstance.questionController.resetQuestionNumbers()
		
		let patientIDValue:Int = SharedResourceManager.sharedInstance.patientID
		
		UserDefaults.standard.set(SharedResourceManager.sharedInstance.patientID, forKey: "ActivePatientID")
		UserDefaults.standard.synchronize()
		
		SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.resetQuestionnaireInstance()
		
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
			DispatchQueue.main.async{
				self.dismiss(animated: true, completion: nil)
			}
		}
		
//		self.dismiss(animated: true, completion: nil)
		//self.navigationController?.popViewController(animated: true)
	}
	
	// Do any additional setup after loading the view, typically from a nib.

	
	override func viewWillDisappear(_ animated: Bool)
	{
		// Make the navigation bar transparent again when we leave the questionnaire
		self.navigationController?.setToolbarHidden(true, animated: true)
	}
	
	override func viewDidLoad()
	{
		
		
		super.viewDidLoad()
	}

	// Dispose of any resources that can be recreated.

	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}

	// MARK: Quicklook
	
	func numberOfPreviewItems(in controller: QLPreviewController) -> Int
	{
		return 1
	}
	
	func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem
	{
		let url = NSURL(fileURLWithPath: self.localReportURL)
		return url
	}
}
