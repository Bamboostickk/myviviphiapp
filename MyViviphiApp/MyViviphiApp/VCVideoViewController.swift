//
//  VCVideoViewController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-07.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit

class VCVideoViewController: UIViewController
{
	@IBOutlet weak var youtubeView: UIWebView!
	
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}
	
	@IBAction func videoSelected(_ sender: AnyObject)
	{
		playVideo()
	}
	
	@IBAction func youtubeSelected(_ sender: AnyObject)
	{
		playYouTube()
	}
	
	func playVideo()
	{
		let videoURL = NSURL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
		let player = AVPlayer(url: videoURL! as URL)
		let playerViewController = AVPlayerViewController()
		playerViewController.player = player
		self.present(playerViewController, animated: true) {
			
			playerViewController.player!.play()
			
		}
	}
	
	func playYouTube()
	{
		let myURL : URL = URL(string: "https://www.youtube.com/embed/H9jHWvBlJxU")!
		//Note: use the "embed" address instead of the "watch" address.
		let myURLRequest : URLRequest = URLRequest(url: myURL)
		self.youtubeView.loadRequest(myURLRequest)
	}

}
