 //
//  MVQuestionController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-14.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

/*		This class handles the acquisition and storage of questions from the server
 		It retrieves the question list, and then displays them along with options for the questions
		In addition, this class also handles creating the buttons for the question. Depending on
 		the question and answer picked, this class will display different options in response.

 		This flexibility allows the app to update in response to changes server-side for the questions
 		In other words, changing questions on the Viviphi site won't require the app be updated
		and then reuploaded to the app store every time such changes occur.
 */

class MVQuestionController
{
	
	var listOfQuestions:[Int:MVQuestion]? = [:]
	var listOfQuestionsOrderedBySort: [Int:Int]? = [:]
	
	var listOfGenders:[Int:String]? = [:]
	
	var listOfCancers:[Int:String]? = [:]
	var listOfCancerStages:[Int:String]? = [:]
	var listOfMarkers:[Int:String]? = [:]
	var listOfAlterations:[Int:String]? = [:]
		
	var listOfOtherConditions:[Int:String]? = [:]
	var listOfMedications:[Int:String]? = [:]
	var listOfAllergies:[Int:String]? = [:]
	
	var listOfHistologies:[Int:String]? = [:]
	
	var listOfEthnicities:[Int:String]? = [:]
	var listOfHealthInsurers:[Int:String]? = [:]
	
	var orderedQuestionPosititions:[Int] = [Int]()
	var orderedQuestionIDs:[Int] = [Int]()
	var activeQuestionNumber:Int = 0				// Position in ordered array, does not refer to the actual question ID
	var activeQuestionArrayPosition = 0
	var activeQuestionID = 0
	var nextQuestionID = 0
	var previousQuestionID = 0
	
	var maximumQuestionNumber:Int = 0
	
	var genderID:Int = 0
	var cancerID:Int = 0
	
	// Obtains a list of all the questions from the server on startup. The list is a JSON object
	// Each question is a question object
	
	func getQuestions()
	{
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Survey"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlString)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)

			for index in 0...JSONObject.count - 1
			{
				var question = MVQuestion()
				
				question.questionID = JSONObject[index]["id"].intValue
				question.questionNumber = JSONObject[index]["question_number"].intValue
				question.questionName = JSONObject[index]["question_name"].string!
				question.questionText = JSONObject[index]["question_text"].string!
				question.questionSubtext = JSONObject[index]["question_subtext"].string!
				question.questionSupportsMultipleAnswers = JSONObject[index]["allow_multiple_answers"].bool
				question.questionLookupType = JSONObject[index]["lookup_type_id"].intValue
				question.questionSortOrder = JSONObject[index]["sort_order"].intValue
				
				self.listOfQuestions![question.questionID!] = question
				self.listOfQuestionsOrderedBySort![question.questionSortOrder] = question.questionID
				self.orderedQuestionPosititions.append(question.questionSortOrder)
			}
			
			self.orderedQuestionPosititions.sort()
			
			for number in self.orderedQuestionPosititions
			{
				self.orderedQuestionIDs.append(self.listOfQuestionsOrderedBySort![number]!)
			}
			
			self.maximumQuestionNumber = JSONObject.count
		}
	}
	
	// Note, call this only if there is a list of questions to work with!
	
	func returnQuestions() -> [Int:MVQuestion]
	{
		return self.listOfQuestions!
	}
	
	func updateAllQuestionValues()
	{
		self.activeQuestionArrayPosition = self.activeQuestionNumber - 1
		self.activeQuestionID = self.orderedQuestionIDs[self.activeQuestionArrayPosition]
		
		print("ACTIVE QUESTION ID: " + String(describing: self.activeQuestionID))
		print("QUESTION NAMED: " + self.returnActiveQuestion().questionName!)
		print("LOCAL QUESTION NUMBER: " + String(describing: self.activeQuestionNumber))
		print("ACTIVE POSITION IN ARRAY: " + String(describing: self.activeQuestionArrayPosition))

		if (activeQuestionArrayPosition + 1 < self.orderedQuestionIDs.count)
		{
			self.nextQuestionID = self.orderedQuestionIDs[activeQuestionArrayPosition + 1]
		}
			
		else
		{
			self.nextQuestionID = -1
		}
	}
	
	// Return the active question object
	
	func returnActiveQuestion() -> MVQuestion
	{
		let sortedValue:Int = self.orderedQuestionPosititions[activeQuestionArrayPosition]
		let keyFromSorted:Int = self.listOfQuestionsOrderedBySort![sortedValue]!

		return self.listOfQuestions![keyFromSorted]!
	}
	
	func returnSelectedQuestion(pickedQuestionID:Int) -> MVQuestion
	{
		return self.listOfQuestions![pickedQuestionID]!
	}
	
	// Obtains the genders available on the server. The gender picked affects which cancers are available
	
	func getGenders()
	{
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "api/Gender"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlString)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfGenders![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
		}
	}
	
	// Obtains a list of cancers for the user to pick based on what gender they picked. 
	// The cancer picked affects which stages, tumour markers and gene alterations are available.
	// This method uses a completion handler, and only allows the calling method to fire after the JSON is retrieved
	
	func getCancersForGender(genderID:Int, onFinish:@escaping (() -> Void))
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "/api/Disease/Gender?codeId=" + String(genderID)
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfCancers![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
			
			onFinish()
		}
	}
	
	// Obtains the list of cancer stages for a cancer with ID.
	// This method uses a completion handler, and only allows the calling method to fire after the JSON is retrieved
	
	func getCancerStage(cancerID:Int)
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/Stage/" + String(cancerID)
	
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfCancerStages![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
		}
	}
	
	// Obtains the list of cancer stages for a cancer with ID.
	// This method uses a completion handler, and only allows the calling method to fire after the JSON is retrieved
	
	func getCancerStageWithDelay(cancerID:Int, onFinish:@escaping (() -> Void))
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/Stage/" + String(cancerID)
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfCancerStages![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
			
			onFinish()
		}
	}
	
	// Obtains the list of histology for a cancer with ID.
	// This method uses a completion handler, and only allows the calling method to fire after the JSON is retrieved
	
	func getHistology(cancerID:Int, onFinish:@escaping (() -> Void))
	{		
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/Histology/" + String(cancerID)
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			if ((JSONObject.array?.count)! > 0)
			{
				for index in 0...JSONObject.count - 1
				{
					self.listOfHistologies![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
				}
			}

			onFinish()
		}

	}
	
	// Obtains the list of tumour markers for a cancer with ID.
	
	func getTumours(cancerID:Int)
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/TumorMarker/GetSortedList?id=" + String(cancerID)
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			if ((JSONObject.array?.count)! > 0)
			{
				for index in 0...JSONObject.count - 1
				{
					self.listOfMarkers![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
				}
			}
		}
	}
	
	// Obtains the list of tumour markers for a cancer with ID and only allows a function to continue if the markers have been obtained
	
	func getTumoursWithDelay(cancerID:Int, onFinish:@escaping (() -> Void))
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/TumorMarker/GetSortedList?id=" + String(cancerID)
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			if ((JSONObject.array?.count)! > 0)
			{
				for index in 0...JSONObject.count - 1
				{
					self.listOfMarkers![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
				}
			}
			onFinish()
		}
	}	// Obtains the list of genomic alterations.
	
	func getGenomicAlterations()
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/gene/getall"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfAlterations![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
		}
	}
	
	// Obtains the list of other conditions that the user might have
	
	func getOtherConditions()
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/OtherConditions"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfOtherConditions![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
		}
	}
	
	// Obtains the list of medications that the user might be on
	
	func getMedications()
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/Medications"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfMedications![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
		}
	}

	// Obtains the list of allergies that the user might have
	
	func getAllergies()
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/Allergies"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfAllergies![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
		}
	}
	
	// Obtains the list of ethnicities that the user might be
	
	func getEthnicities()
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/Ethnicity"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfEthnicities![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
		}
	}
	
	// Obtains the list of health insurers that the user might be with
	
	func getHealthInsurers()
	{
		let urlToUse = SharedResourceManager.sharedInstance.URL_PATH + "api/Insurer"
		
		SharedResourceManager.sharedInstance.serverRequestController.getJSONFromServer(request: SharedResourceManager.sharedInstance.serverRequestController.createURLRequest(theURL: urlToUse)){resultJSON -> () in
			
			let JSONObject = JSON(resultJSON)
			
			for index in 0...JSONObject.count - 1
			{
				self.listOfHealthInsurers![JSONObject[index]["CodeId"].int!] = JSONObject[index]["Description"].string!
			}
		}
	}
	
	// This method creates a key corresponding to the question
	
	func makePatientDataKeyFromQuestion(question:String) -> String
	{
		var correspondingKey:String = ""
		
		switch question
		{
			case "Gender":
				correspondingKey = "Gender"
			case "Age":
				correspondingKey = "Age"
			case "Cancer Diagnosis":
				correspondingKey = "Diagnosis"
			case "Cancer Stage":
				correspondingKey = "StageRoman"
			case "Tumor Sequencing":
				correspondingKey = "TumorSequencingMarkers"
			case "Genomic Alterations":
				correspondingKey = "GenomicAlterations"
			case "Other Diagnosis":
				correspondingKey = "Comorbidity"
			case "Other Medications":
				correspondingKey = "OtherMedications"
			case "Allergies":
				correspondingKey = "Allergies"
			case "Ethnicity":
				correspondingKey = "Ethnicity"
			case "Health Insurer":
				correspondingKey = "HealthInsurer"
			default:
				break
		}
		
		return correspondingKey
	}
	
	// This method clears out the shared instance of the question, allowing a new questionnaire to be made
	
	func resetAll()
	{
		self.listOfCancers = [:]
		self.listOfCancerStages = [:]
		self.listOfMarkers = [:]
		
		self.listOfHistologies = [:]

		self.orderedQuestionPosititions = []
		self.orderedQuestionIDs = []
		
		self.activeQuestionNumber = 0
		self.activeQuestionArrayPosition = 0
		self.activeQuestionID = 0
		self.nextQuestionID = 0
		self.previousQuestionID = 0
		
		self.genderID = 0
		self.cancerID = 0
	}
	
	func resetQuestionNumbers()
	{
		self.activeQuestionNumber = 0
		self.activeQuestionArrayPosition = 0
		self.activeQuestionID = 0
		self.nextQuestionID = 0
		self.previousQuestionID = 0
	}
	
	// Required to clean up list of histology when cancer has changed in questionnaire
	
	func resetHistology()
	{
		self.listOfHistologies = [:]
	}
	
	func resetStages()
	{
		self.listOfCancerStages = [:]
	}
}
