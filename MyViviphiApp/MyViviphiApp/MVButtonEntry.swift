//
//  MVButtonEntry.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-15.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation

struct MVButtonEntry
{
	var codeID:Int = 0
	var description:String = ""
	var isSelected:Bool = false
}
