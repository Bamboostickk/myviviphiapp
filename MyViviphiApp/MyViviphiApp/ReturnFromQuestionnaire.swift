//
//  QuestionnaireFinnishedDelegate.swift
//  MyViviphiApp
//
//  Created by Sasha Ivanov on 2017-01-01.
//  Copyright © 2017 Wellness Computational. All rights reserved.
//

import Foundation


protocol QuestionnaireFinnishedDelegate
{
	func sendValue(shouldAutomaticallyGoToReport : Bool)
}


class ReturnFromQuestionnaire{
	var questionnaireFinnishedDelegate:QuestionnaireFinnishedDelegate!
}
