//
//  SummarySectionView.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-12-11.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

enum statusType {
    case complete
    case incomplete
}

@IBDesignable class SummarySectionView: UIView {
    
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var sectionTitle: UILabel!
    @IBOutlet weak var sectionLabel: UILabel!

    var status:statusType = .complete {
        
        didSet{
            if(status == .complete){
                statusImage.image = #imageLiteral(resourceName: "Check")
            }
                
            else{
                statusImage.image = #imageLiteral(resourceName: "Question")
            }
        }
        
    }
    
    var isAnimating = false
	
	/*
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
		/*
		UIView.animateWithDuration(2.0, delay: 1.0, options:UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
		dispatch_async(dispatch_get_main_queue()) {
		
		bigImageView.frame = CGRectMake(0, 0, 320, 500)
		}
		
		}) { (completed:Bool) -> Void in
		
		} }
		*/
		
		/*
        UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseInOut], animations: {
            self.backgroundColor = UIColor(colorLiteralRed: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
        }, completion: nil)
		*/
        //self.backgroundColor = UIColor(colorLiteralRed: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
		
		/*
		let animation = CABasicAnimation(keyPath: "backgroundColor")
		animation.duration = 0.2
		animation.repeatCount = 1.0
		animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
		animation.fromValue = UIColor.clear
		animation.toValue = UIColor(colorLiteralRed: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
		//animation.beginTime = CACurrentMediaTime() + 0.3
		self.layer.add(animation, forKey: "fadeInAnimation")
		*/
		
		
    }

	
	
	
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		/*
		let animation = CABasicAnimation(keyPath: "backgroundColor")
		animation.duration = 0.2
		animation.repeatCount = 1.0
		animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
		animation.fromValue = UIColor(colorLiteralRed: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
		animation.toValue = UIColor.clear
		//animation.beginTime = CACurrentMediaTime() + 0.3
		self.layer.add(animation, forKey: "fadeOutAnimation")
		*/
		/*
		UIView.animate(withDuration: 2.0, delay: 1.0, options:UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
		DispatchQueue.main.async {
		
			self.backgroundColor = UIColor.clear
		}
		
		}) { (completed:Bool) -> Void in
		
		}
	*/

    }*/
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        self.layer.cornerRadius = 6
        let view = Bundle.main.loadNibNamed("SummarySectionView", owner: self, options: nil)?[0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
        
		
		
		
		
        //let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.animateTap(_:)))
        //self.addGestureRecognizer(tapGesture)
        
    }

    
    
    
	/*
    func animateTap(_ sender: UITapGestureRecognizer){
        
        if (!self.isAnimating){
            self.isAnimating = true
			
			UIView.animate(withDuration: 2.0, delay: 1.0, options:UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
				DispatchQueue.main.async {
					
					self.backgroundColor = UIColor.clear
				}
				
			}) { (completed:Bool) -> Void in
				
			}
        }
        
    }
	
    */
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
