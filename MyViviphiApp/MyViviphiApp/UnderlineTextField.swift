//
//  UnderlineTextField.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-12-03.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

class UnderlineTextField: UITextField {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {

        let startingPoint   = CGPoint(x: rect.minX, y: rect.maxY)
        let endingPoint     = CGPoint(x: rect.maxX, y: rect.maxY)
        
        let path = UIBezierPath()
        
        
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 2.0
        
        UIColor.black.setStroke()
        
        path.stroke()


        
    }
 


}
