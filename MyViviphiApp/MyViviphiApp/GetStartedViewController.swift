//
//  GetStartedViewController.swift
//  MyViviphiApp
//
//  Created by Sasha Ivanov on 2016-12-30.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class GetStartedViewController: UIViewController {
	
    var titles = ["MyNews","MyHealth","MyDictionary"]
	var content = ["Browse the latest content in cancer care selected for you by experts.","Discover the best available options for your specific cancer including genomic-guided targeted therapies.","Look up cancer-related medical terms and learn helpful definitions and explanations."]
	var images:[UIImage] = [#imageLiteral(resourceName: "NewsHiRes"),#imageLiteral(resourceName: "HealthHiRes"),#imageLiteral(resourceName: "DictionaryHiRes")]

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var startButton: FilledButton!
    
    @IBAction func startButtonPressed(_ sender: Any)
	{
		OperationQueue.main.addOperation
		{
			let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
			let nextViewController = storyboard.instantiateViewController(withIdentifier: "AppHomeScene") as UIViewController
			self.present(nextViewController, animated: true, completion: nil)
		}
    }
	
    override func viewDidLoad()
	{
        super.viewDidLoad()
		
		// Setup Stackview Properties
		stackView.alignment = .top
		stackView.distribution = .equalSpacing
		
		// Hide the back button since the user just logged in
		self.navigationItem.hidesBackButton = true
		
		addSectionsToStack()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	func addSectionsToStack(){
		
		// Add title view
		let title = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 45))
		title.text = "Get Started"
		title.textAlignment = .center
		title.font = UIFont.systemFont(ofSize: 36, weight: UIFontWeightLight)
		stackView.addArrangedSubview(title)
		
		// Add Constraints to title
		let titleHeightConstraint = NSLayoutConstraint(item: title, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 45)
		title.addConstraint(titleHeightConstraint)
		
		let titleWidthConstraint = NSLayoutConstraint(item: title, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width)
		title.addConstraint(titleWidthConstraint)
		
		for t in titles{
			
			// Get the index of the content in the arrays
			let index = titles.index(where: {$0 == t})
			
			// fill the section view
			let sectionView = GetStartedSectionView()
			sectionView.title.text = titles[index!]
			sectionView.content.text = content[index!]
			sectionView.image.image = images[index!]
			
			let newSize = sectionView.content.sizeThatFits(sectionView.content.contentSize)
			
			print(newSize.height)
			
			// Add Constraints
			let heightConstraint = NSLayoutConstraint(item: sectionView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: newSize.height + 60)
			sectionView.addConstraint(heightConstraint)
			
			let widthConstraint = NSLayoutConstraint(item: sectionView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width)
			sectionView.addConstraint(widthConstraint)
			
			// Add the view to the stack
			stackView.addArrangedSubview(sectionView)
			sectionView.content.sizeToFit()

		}
	}

	
	

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
