//
//  MVLoginScreenViewController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-10-19.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCLoginScreenViewController: UIViewController,UITextFieldDelegate
{
	
	// How this class will work:
	
	// Connect to server, send request to compare password and email
	// If the server rejects it, post a UIAlert, saying that the credentials are incorrect
	// If the server accepts it, write these values to NSUserDefaults
	// The app will then store these values and pull them for all requests
	// The app will stay logged in unless otherwise asked
	// On logout, clear the information from NSUserDefaults
	
	@IBOutlet weak var inputEmail: UITextField!
	@IBOutlet weak var inputPassword: UITextField!
	
	let loginController:MVLoginController? = MVLoginController()
	
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		inputEmail.delegate = self
		inputPassword.delegate = self
		
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VCLoginScreenViewController.dismissKeyboard))
		view.addGestureRecognizer(tap)
	}
	
	// Dispose of any resources that can be recreated.
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}
	
	// Allows the "Done" button to hide the keyboard
	func textFieldShouldReturn(_ textField: UITextField) -> Bool
	{
		textField.resignFirstResponder()
		return true
	}
	
	
	@IBAction func startLogin(_ sender: AnyObject)
	{
		if (SharedResourceManager.sharedInstance.checkIfAppOnline() == true)
		{
			if (inputEmail.text == "")
			{
				self.displayAlertWithMessage(theTitle: "No email", theMessage: "Please enter your email")
				return;
			}
				
			else if (inputPassword.text == "")
			{
				self.displayAlertWithMessage(theTitle: "No password", theMessage: "Please enter your password")
				return;
			}
				
			else
			{
				self.authenticateServerLogin(username: inputEmail.text!, password: inputPassword.text!)
			}
		}
			
		else
		{
			self.displayAlertWithMessage(theTitle: "Connection unavailable", theMessage: "Please connect your device to Wi-Fi")
		}
	}
	
	@IBAction func retrievePassword(_ sender: AnyObject)
	{
		// Opens the Viviphi Forgot Password Website in Safari
		UIApplication.shared.open(URL(string: "https://myviviphi.com/Account/ForgotPassword")!, options: [:], completionHandler: nil)
	}
	
	
	@IBAction func cancelLogin(_ sender: UIBarButtonItem)
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	func dismissKeyboard()
	{
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
	
	// If the connection can be made to the server (i.e. the password and username were correct) and a dictionary could be returned
	// Grab the API token and write it to the user defaults.
	
	func authenticateServerLogin(username:String, password:String)
	{
		loginController?.startSessionWith(request: (loginController?.makeHTTPRequestWith(theUsername: username, thePassword: password))!){resultDictionary -> () in
			
			if ((self.loginController?.connectionSuccessful)! == true)
			{
				if ((self.loginController?.loginSuccessful)! == true)
				{
					
					self.loginController?.updateLocalDictionary(theDictionary: resultDictionary)
					self.writeLoginDataToKeychain(thePassword: password, theUsername: username, accessToken: (self.loginController?.attributeDictionary?.value(forKey: "access_token") as! String?)!)
					
					OperationQueue.main.addOperation
					{
						let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
						let nextViewController = storyboard.instantiateViewController(withIdentifier: "getStarted") as UIViewController
						//self.present(nextViewController, animated: true, completion: nil)
						self.navigationController?.pushViewController(nextViewController, animated: true)
					}
				}
					
				else
				{
					OperationQueue.main.addOperation
						{
							self.displayAlertWithMessage(theTitle: "Login unsuccessful", theMessage: "The email or password you entered was incorrect. Please try again.")
					}
					
				}
			}
			// On segue, do something
		}
	}
	
	// If the login failed, put a pop-up that says the username or password was incorrect
	
	func displayAlertWithMessage(theTitle:String, theMessage:String)
	{
		let alertView = UIAlertController(title: theTitle, message: theMessage as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "OK", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	// Writes the login data to the app's internal storage. The username and token are stored to the userDefaults, while the password is stored to the key chain
	
	func writeLoginDataToKeychain(thePassword:String, theUsername:String, accessToken:String)
	{
		SharedResourceManager.sharedInstance.MyKeychainWrapper.mySetObject(thePassword, forKey: "v_Data")
		SharedResourceManager.sharedInstance.MyKeychainWrapper.writeToKeychain()
		
		print("PASSWORD STORED: " + String(describing: SharedResourceManager.sharedInstance.MyKeychainWrapper.myObject(forKey: "v_Data")!))
		
		let theAccessToken = "bearer " + accessToken
		
		UserDefaults.standard.set(theUsername as Any, forKey:"username")
		UserDefaults.standard.set(theAccessToken as Any, forKey: "accessToken")
		UserDefaults.standard.set(true, forKey: "hasLoginKey")
		UserDefaults.standard.synchronize()
	}
}
