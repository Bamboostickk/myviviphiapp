//
//  VCEditButtonView.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-01.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//


/*****************************************************************/
/*****************************************************************/
/*****************************************************************/
/***************************DEPRECIATED***************************/
/*****************************************************************/
/*****************************************************************/
/*****************************************************************/


import UIKit

class VCEditButtonView : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{

	@IBOutlet weak var theNavigationBar: UINavigationBar!
	@IBOutlet weak var textLabel: UILabel!
	
    @IBOutlet weak var collectionView: UICollectionView!
	let viewControllerIdentifier:String? = ""
	
	var questionsList:[String] = [String]()
	var questionDictionary:[Int:String] = [:]
	
	var answerObjects:[MVPatientAnswers] = [MVPatientAnswers]()
	var selectedDictionary:[Int:Bool] = [:]
	
	var genderObject:[MVPatientAnswers] = [MVPatientAnswers]()
	var cancerObject:[MVPatientAnswers] = [MVPatientAnswers()]
	
	var allowsMultipleSelection:Bool? = false
	
	var selectedList:[Bool] = [Bool]()
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{	
		self.dismiss(animated: true, completion: nil)
	}
	
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		collectionView.allowsMultipleSelection = true
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		
		self.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		theNavigationBar.topItem?.title = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!
		self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText!

		if let hasMultipleSupport = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionSupportsMultipleAnswers
		{
			self.allowsMultipleSelection = hasMultipleSupport
		}
		
		setupPageForQuestionType()
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return questionsList.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QuestionCollectionViewCell
		
		cell.label.text = questionsList[indexPath.item]
		cell.label.textColor = self.view.tintColor
		
		if (self.selectedList[indexPath.item] == true)
		{
			cell.isSelected = true
		}
	
		return cell
	}
	
	// MARK: - UICollectionViewDelegate protocol
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		// handle tap events
		//print("You selected cell #\(indexPath.item)!")
		let cell = collectionView.cellForItem(at: indexPath)
		cell?.isSelected = true
		buttonAction(cell:cell!)
	}
	
	
	// Probably not needed
	
	func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
	{
		// handle tap events
		print("You deselected cell #\(indexPath.item)!")
		let cell = collectionView.cellForItem(at: indexPath)
		cell?.isSelected = false
	}

	// Sets up the page
	
	func setupPageForQuestionType()
	{
		let question:String = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionName!

		switch question
		{
			case "Gender":
				questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfGenders!.values).sorted(by: <)
				questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfGenders!
				answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientGender!
			case "Ethnicity":
				questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfEthnicities!.values).sorted(by: <)
				questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfEthnicities!
				answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientEthnicity!
			case "Health Insurer":
				questionsList = Array(SharedResourceManager.sharedInstance.questionController.listOfHealthInsurers!.values).sorted(by: <)
				questionDictionary = SharedResourceManager.sharedInstance.questionController.listOfHealthInsurers!
				answerObjects = SharedResourceManager.sharedInstance.patientDataController.patientInsuruer!
			default:
				break
		}
		
		compareQuestionWithSelection()
		//addButtons(listOfOptions: questionsList)
	}
	
	// Actions to carry out when a button is pressed
	// When a button is pressed, grabs the button's title and then updates the questionnaire
	// Either by loading a new scene, or grabbing new data and then loading the new scene
	
	func buttonAction(cell:UICollectionViewCell)
	{
		let selectedCell:QuestionCollectionViewCell = cell as! QuestionCollectionViewCell
		
		let buttonValue:String = selectedCell.label.text!
		
		if (buttonValue == "Other")
		{
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "EditOtherView") as? VCEditOtherView {
				present(resultController, animated: true, completion: nil)
			}
		}
		
		else
		{
			self.loadNextScreen()
		}

		self.writeChoice(buttonValue: buttonValue, willClear: false)
				
//		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		
	
	}
	
	// Determines what kind of view controller to load next based on the question ID
	// Only useful if the questions are in a particular order.
	// We can write an algorithm that forces a particualer scene to load based on what the question itself is
	// Needs to be smarter, i.e. checks for question name
	
	func loadNextScreen()
	{
		let patientIDValue = SharedResourceManager.sharedInstance.patientID
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
			DispatchQueue.main.async{
				self.updateCancerForGender()
				self.updateCancerFieldsFromType()
				self.dismiss(animated: true, completion: nil)
			}
		}
	}
	
	// Displays error message if nothing is selected
	
	func displayErrorMessage()
	{
		let alertView = UIAlertController(title: "Nothing selected", message: "Please make a selection" as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
	
	// Method only to be used if multiple selections are allowed. Checks if anything is selected
	
	func checkIfAnythingSelected() -> Bool
	{
		for value in self.selectedList
		{
			if (value == true)
			{
				return true
			}
		}
		
		return false
	}
	
	func writeChoice(buttonValue:String, willClear:Bool)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let selectedKeyFromArray = keyForValueArray[0]
		print("KEY: " + String(describing:selectedKeyFromArray))
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
	SharedResourceManager.sharedInstance.serverRequestController.writeSelectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID, willClear: willClear)
		
	}
	
	func removeChoice(buttonValue:String)
	{
		let keyForValueArray = self.questionDictionary.allKeys(forValue: buttonValue)
		let selectedKeyFromArray = keyForValueArray[0]
		
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: selectedKeyFromArray)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)

		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	func deselectCustomChoice()
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: "")
	}
	
	func compareQuestionWithSelection()
	{
		var selectedArray:[Int] = [Int]()

		for _ in (0...self.questionsList.count - 1)
		{
			self.selectedList.append(false)
		}
		
		if (self.answerObjects.count > 0)
		{
			for index in (0...self.answerObjects.count - 1)
			{
				selectedArray.append(self.answerObjects[index].codeID)
			}
			
			for (key, _) in self.questionDictionary
			{
				if (selectedArray.contains(key))
				{
					self.selectedDictionary[key] = true
				}
				
				else
				{
					self.selectedDictionary[key] = false
				}
			}
			
			for (key, value) in self.questionDictionary
			{
				if (self.selectedDictionary[key] == true)
				{
					let indexOfValue:Int = self.questionsList.index(of: value)!
					self.selectedList[indexOfValue] = true
				}
			}
		}
		
	}
	
	func updateCancerForGender()
	{
		let genderValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientGender![0].codeID
		
		if (genderValue != 0)
		{
			SharedResourceManager.sharedInstance.questionController.listOfCancers = [:]
			SharedResourceManager.sharedInstance.questionController.getCancersForGender(genderID: genderValue){ () -> () in
			}
		}
		
	}
	
	func updateCancerFieldsFromType()
	{
		if (SharedResourceManager.sharedInstance.patientDataController.patientCancer!.count > 0)
		{
			let cancerValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientCancer![0].codeID
			
			if (cancerValue != 0)
			{
				SharedResourceManager.sharedInstance.questionController.listOfHistologies = [:]
				SharedResourceManager.sharedInstance.questionController.listOfCancerStages = [:]
				SharedResourceManager.sharedInstance.questionController.listOfMarkers = [:]
				SharedResourceManager.sharedInstance.questionController.listOfAlterations = [:]
				
				SharedResourceManager.sharedInstance.questionController.getHistology(cancerID: cancerValue){() -> () in}
				SharedResourceManager.sharedInstance.questionController.getCancerStage(cancerID: cancerValue)
				SharedResourceManager.sharedInstance.questionController.getTumours(cancerID: cancerValue)
			}
		}
	}
}
