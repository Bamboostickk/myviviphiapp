//
//  SelectedOptionsTableViewController.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-12-06.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit


class SelectedOptionsTableViewController: UITableViewController {

	var delegate: SendDataBack?
	
	var questionDictionary:[String:MVTableEntry] = [:]
	var orderedTableEntries:[MVTableEntry] = [MVTableEntry]()
    /*
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        return CoverScreenPresentation(presentedViewController: presented, presenting: presenting)
        
    }
    */
    func hideSelectedAlterations(){

    }


    override func viewDidLoad()
	{
        super.viewDidLoad()
        
		self.prepareAnswerObjectArray()

		
		guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
		statusBar.backgroundColor = UIColor.clear
		
        // Disabled toolbar for "Hide Selected Alterations button" because a different approach will be needed. At the moment, just tap the grey area to hide the view.
        /*
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let hideSelectedButton = UIBarButtonItem(`: "Hide Selected Alterations", style: .plain, target: self, action: #selector(hideSelectedAlterations))

        toolbar.setItems([flexibleSpace,hideSelectedButton,flexibleSpace], animated: false)
        self.view.addSubview(toolbar)
        */
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
	
	override func viewDidDisappear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
		guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
		statusBar.backgroundColor = UIColor.white
		delegate?.didSendSelectionsBack(updatedSelections: self.questionDictionary)
	}


	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.orderedTableEntries.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
	
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectedItem", for: indexPath)

        cell.textLabel?.text = self.orderedTableEntries[indexPath.row].description
		
        return cell
    }

	override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath)
	{

		self.removeChoice(codeIDNumberForm: self.orderedTableEntries[indexPath.row].codeID)
		self.questionDictionary.removeValue(forKey:self.orderedTableEntries[indexPath.row].description)
		self.orderedTableEntries.remove(at: indexPath.row)
		
		self.tableView.deleteRows(at: [indexPath], with: .fade)
		//self.tableView.reloadData()
	
	}

	func removeChoice(codeIDNumberForm:Int)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let codeID:String = String(describing: codeIDNumberForm)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeUnselectToServer(thePatientID: patientID, theQuestionID: activeQuestion, theCodeID: codeID, theLookUpType: lookupID)
	}
	
	func prepareAnswerObjectArray()
	{
		let sortedOptions:[String] =  Array(self.questionDictionary.keys).sorted(by: <)
		
		for entry in sortedOptions
		{
			self.orderedTableEntries.append(self.questionDictionary[entry]!)
		}
	}
}

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

	
    // Override to support editing the table view.
	/*
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		
		if editingStyle == .delete
		{
            // Delete the row from the data source
			
			self.removeChoice(codeIDNumberForm: self.questionDictionary[self.options[indexPath.row]]!.codeID)
			self.questionDictionary.removeValue(forKey: (cell.textLabel?.text)!)
			//tableView.deleteRows(at: [indexPath], with: .fade)
			self.tableView.reloadData()
        }
    }
	*/

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */



/*
class CoverScreenPresentation: UIPresentationController {
    
    override var frameOfPresentedViewInContainerView : CGRect {
        
        let container = self.containerView!.frame
        
        let barHeight = self.presentingViewController.navigationController?.navigationBar.frame.height
        
        return CGRect(x: 0, y: container.height - 300, width: container.width, height: container.height - 300)
        
    }
}*/
