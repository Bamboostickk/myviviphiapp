//
//  VCPatientSummaryLanding.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-12-20.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCPatientSummaryLanding: UIViewController
{
	
	var arrayOfQuestions:[String] = [String]()
	var questionDictionary:[Int:String] = [:]
	var arrayOfAnswers:[String] = [String]()
	
	var allViewSections:[UIView] = []
	
	var hasGender:Bool = false
	var hasCancer:Bool = false
	var completionPercentage:Double = 0.00
	
	var patientName:String = ""
	
	var patientGender:String = ""
	var patientAge:String = ""
	var patientCancer:String = ""
	var patientHistology:String = ""
	var patientStage:String = ""
	var patientTumourMarkers:String = ""
	var patientGenomicAlterations:String = ""
	var patientOtherDiseases:String = ""
	var patientMedications:String = ""
	var patientAllergies:String = ""
	var patientEthnicity:String = ""
	var patientPostalCode:String = ""
	var patientInsurer:String = ""
	
	var titles:[[String]] = [[String]]()
	
	var selectedChoices:[[String]] = [[String]]()
	
	var destinationIdentifiers = [["PDFViewer"],[],["Gender","Age"], [],["Cancer Type"], ["Histology", "Stage"], [], ["Tumor Markers"], [], ["Genomic Alterations"], [],["Health Problems"],[],["Medications","Allergies"],[], ["Postal Code", "Ethnicity"], ["Health Insurer"]]
	
	//var destinationIdentifiers = [["PDFViewer"],[],["Cancer Type"], ["Histology", "Stage"], [], ["Tumor Markers"], [], ["Genomic Alterations"], [], ["Gender","Age"], [], ["Health Problems"],[],["Medications","Allergies"],[], ["Postal Code", "Ethnicity"], ["Health Insurer"]]
	
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstStrategyContainerView: UIView!
	@IBOutlet weak var stackView: UIStackView!
	var stackItemHeight:CGFloat = 50
	
	override func viewDidLoad()
	{
		
		super.viewDidLoad()
		
		//self.title = SharedResourceManager.sharedInstance.patientDataController.patientsList[SharedResourceManager.sharedInstance.patientID]
		
		/*
        if (firstStrategyContainerView.subviews.count > 0){
            if let firstVC = firstStrategyContainerView.subviews[0] as? VCFirstReportScreen{
                firstVC.createReportButton.addTarget(<#T##target: Any?##Any?#>, action: <#T##Selector#>, for: <#T##UIControlEvents#>)
            }
        }*/

		//self.populatePatientDataFields()
		
	}
	
	
	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.setToolbarHidden(true, animated: false)	// make sure toolbar hidden from pdf viewer
		
		/*
		SharedResourceManager.sharedInstance.patientDataController.obtainListOfPatientsFromServerWithDelay(){listOfPatients -> () in
			
			if(listOfPatients.count > 0)
			{
				print("HIT!")
				self.populatePatientDataFields()
				//self.scrollView.isHidden = false
				//self.firstStrategyContainerView.isHidden = true
			}
			
			if (self.allViewSections.count > 0)
			{
				self.reloadTheStack()
			}
		}
		
		if (SharedResourceManager.sharedInstance.shouldAutomaticallyShowStrategy){
			SharedResourceManager.sharedInstance.shouldAutomaticallyShowStrategy = false
			VCOverlayController.show(UIApplication.shared.keyWindow!, loadingText: "Retrieving your report")
			SharedResourceManager.sharedInstance.serverRequestController.retrievePDFForPatient(patientID: String(describing: SharedResourceManager.sharedInstance.patientID)){fileURL -> () in
				DispatchQueue.main.async
					{
						let stringURL:String = "\(fileURL)"
						
						let localReportURL = stringURL
						
						if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewer") as? VCPDFViewController
						{
							resultController.thePDFPath = stringURL
							self.navigationController?.pushViewController(resultController, animated: true)
							VCOverlayController.hide()
						}
				}
			}
		}
		*/
		
		self.populatePatientDataFields()

		//super.viewWillAppear(animated)

		
		
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)

		

	}
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func reloadTheStack()
	{
		// Update Selected Choices
		//self.updateQuestionsAndChoices()
		self.obtainValuesFromPatientController()
		
		
		self.titles = [[self.patientName  + "'s Genomic Strategy"],[],["Gender","Age"], [],["Cancer Type"], ["Histology", "Stage"], [], ["Tumor Markers"], [], ["Genomic Alterations"], [],["Health Problems"],[],["Medications","Allergies"],[], ["Postal Code", "Ethnicity"], ["Health Insurer"]]
		
		self.selectedChoices = [["View Treatment Strategy"],[],[self.patientGender, self.patientAge], [],[self.patientCancer], [self.patientHistology, self.patientStage], [], [self.patientTumourMarkers], [], [self.patientGenomicAlterations], [],[self.patientOtherDiseases],[],[self.patientMedications,self.patientAllergies],[], [self.patientPostalCode, self.patientEthnicity], [self.patientInsurer]]
		
		//self.titles = [[self.patientName  + "'s Genomic Strategy"],[],["Cancer Type"], ["Histology", "Stage"], [], ["Tumor Markers"], [], ["Genomic Alterations"], [],["Gender","Age"], [],["Health Problems"],[],["Medications","Allergies"],[], ["Postal Code", "Ethnicity"], ["Health Insurer"]]
		
		//self.selectedChoices = [["View Treatment Strategy"],[],[self.patientCancer], [self.patientHistology, self.patientStage], [], [self.patientTumourMarkers], [], [self.patientGenomicAlterations], [], [self.patientGender, self.patientAge], [],[self.patientOtherDiseases],[],[self.patientMedications,self.patientAllergies],[], [self.patientPostalCode, self.patientEthnicity], [self.patientInsurer]]
		
		// Update Completion Percentage
		self.completionPercentage = Double(SharedResourceManager.sharedInstance.patientDataController.activePatientPercentComplete)/100
		
		// Remove all subviews from the stack (perhaps a bit brute forceish)
		for s in self.stackView.subviews
		{
			s.removeFromSuperview()
		}
		
		// Recreate every view by calling the add questions to stack function
		self.addQuestionsToStack()
	}
	
	// Populates the table for the first time.
	
	func populatePatientDataFields()
	{
		
		// Note: use UIApplication.shared.keyWindow! instead of self.view to overlay on everything
		VCOverlayController.show(UIApplication.shared.keyWindow!, loadingText: "Retrieving Your Information")

		let activeID = SharedResourceManager.sharedInstance.patientID
		
		if (activeID != 0)
		{
            // Show the scrollview if it is not their first report, and hide the first strategy view
            self.scrollView.isHidden = false
            self.firstStrategyContainerView.isHidden = true
            
			SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: activeID){() -> () in
				
				self.updateQuestionsAndChoices()
				self.updateCancerForGender()
				self.updateCancerFieldsFromType()
				self.obtainValuesFromPatientController()
				
				self.titles = [[self.patientName  + "'s Genomic Strategy"],[],["Gender","Age"], [],["Cancer Type"], ["Histology", "Stage"], [], ["Tumor Markers"], [], ["Genomic Alterations"], [],["Health Problems"],[],["Medications","Allergies"],[], ["Postal Code", "Ethnicity"], ["Health Insurer"]]
				
				self.selectedChoices = [["View Treatment Strategy"],[],[self.patientGender, self.patientAge], [],[self.patientCancer], [self.patientHistology, self.patientStage], [], [self.patientTumourMarkers], [], [self.patientGenomicAlterations], [],[self.patientOtherDiseases],[],[self.patientMedications,self.patientAllergies],[], [self.patientPostalCode, self.patientEthnicity], [self.patientInsurer]]
				
				//self.titles = [[self.patientName  + "'s Genomic Strategy"],[],["Cancer Type"], ["Histology", "Stage"], [], ["Tumor Markers"], [], ["Genomic Alterations"], [],["Gender","Age"], [],["Health Problems"],[],["Medications","Allergies"],[], ["Postal Code", "Ethnicity"], ["Health Insurer"]]
				
				//self.selectedChoices = [["View Treatment Strategy"],[],[self.patientCancer], [self.patientHistology, self.patientStage], [], [self.patientTumourMarkers], [], [self.patientGenomicAlterations], [], [self.patientGender, self.patientAge], [],[self.patientOtherDiseases],[],[self.patientMedications,self.patientAllergies],[], [self.patientPostalCode, self.patientEthnicity], [self.patientInsurer]]
				
				self.completionPercentage = Double(SharedResourceManager.sharedInstance.patientDataController.activePatientPercentComplete)/100
				
				DispatchQueue.main.async
				{
					self.addQuestionsToStack()
					self.view.setNeedsDisplay()
					VCOverlayController.hide()
				}
			}
		}
			
		else
		{
			SharedResourceManager.sharedInstance.internetChecker.retrieveTokenWithDelay { () -> () in
				SharedResourceManager.sharedInstance.patientDataController.obtainListOfPatientsFromServerWithDelay(){listOfPatients -> () in
					
					print("FIRST LOGIN, NO DEFAULT REPORT YET")
					print("ACCOUNT HAS REPORTS: " + String(describing: listOfPatients.count))
					
					if (listOfPatients.count > 0)
					{
						// Show the scrollview if it is not their first report, and hide the first strategy view
						self.scrollView.isHidden = false
						self.firstStrategyContainerView.isHidden = true
						
						let firstPatient:MVPatient = listOfPatients[0]
						let firstPatientID:Int = firstPatient.patientID
						let firstPatientName:String = firstPatient.patientName
						let firstPatientPercent:Int = firstPatient.patientPercent
						
						// Do not remove: if a patient ID was zero when the app starts up, this finds the first report and writes it to the app.
						// If there is no ID in the key, the app will crash
						
						UserDefaults.standard.setValue(firstPatientID as Any, forKey: "ActivePatientID")
						UserDefaults.standard.synchronize()
						
						self.patientName = firstPatientName
						
						SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: firstPatientID){() -> () in
							
							SharedResourceManager.sharedInstance.patientID = firstPatientID
							
							self.updateQuestionsAndChoices()
							self.updateCancerForGender()
							self.updateCancerFieldsFromType()
							self.obtainValuesFromPatientController()
							
							self.titles = [[self.patientName  + "'s Genomic Strategy"],[],["Gender","Age"], [],["Cancer Type"], ["Histology", "Stage"], [], ["Tumor Markers"], [], ["Genomic Alterations"], [],["Health Problems"],[],["Medications","Allergies"],[], ["Postal Code", "Ethnicity"], ["Health Insurer"]]
							
							self.selectedChoices = [["View Treatment Strategy"],[],[self.patientGender, self.patientAge], [],[self.patientCancer], [self.patientHistology, self.patientStage], [], [self.patientTumourMarkers], [], [self.patientGenomicAlterations], [],[self.patientOtherDiseases],[],[self.patientMedications,self.patientAllergies],[], [self.patientPostalCode, self.patientEthnicity], [self.patientInsurer]]
							
							//self.titles = [[self.patientName  + "'s Genomic Strategy"],[],["Cancer Type"], ["Histology", "Stage"], [], ["Tumor Markers"], [], ["Genomic Alterations"], [],["Gender","Age"], [],["Health Problems"],[],["Medications","Allergies"],[], ["Postal Code", "Ethnicity"], ["Health Insurer"]]
							
							//self.selectedChoices = [["View Treatment Strategy"],[],[self.patientCancer], [self.patientHistology, self.patientStage], [], [self.patientTumourMarkers], [], [self.patientGenomicAlterations], [], [self.patientGender, self.patientAge], [],[self.patientOtherDiseases],[],[self.patientMedications,self.patientAllergies],[], [self.patientPostalCode, self.patientEthnicity], [self.patientInsurer]]
							
							self.completionPercentage = Double(firstPatientPercent)/100
							
							DispatchQueue.main.async
							{
								self.addQuestionsToStack()
								self.view.setNeedsDisplay()
								VCOverlayController.hide()
							}
						}
					}
					
					// When the account has no strategies
					
					else
					{
						// Hide the scrollview if it's their first report, and show the first strategy view instead
						
						print("WILL LOAD FIRST STRATS VIEW")
						
						self.scrollView.isHidden = true
						self.firstStrategyContainerView.isHidden = false
						VCOverlayController.hide()
					}
				}
			}
		}
		
	}
	
	func obtainValuesFromPatientController()
	{
		self.patientName = SharedResourceManager.sharedInstance.patientDataController.activePatientName
		self.patientGender = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[1]!
		self.patientAge = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[2]!
		self.patientCancer = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[3]!
		self.patientHistology = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[13]!
		self.patientStage = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[4]!
		self.patientTumourMarkers = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[5]!
		self.patientGenomicAlterations = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[6]!
		self.patientOtherDiseases = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[7]!
		self.patientMedications = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[8]!
		self.patientAllergies = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[9]!
		self.patientEthnicity = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[10]!
		self.patientPostalCode = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[11]!
		self.patientInsurer = SharedResourceManager.sharedInstance.patientDataController.patientDataDisplay[12]!
	}
	
	func updateCancerForGender()
	{
		let genderValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientGender![0].codeID
		
		if (genderValue != 0)
		{
			SharedResourceManager.sharedInstance.questionController.getCancersForGender(genderID: genderValue){ () -> () in
				self.hasGender = true
			}
		}
	}
	
	func updateCancerFieldsFromType()
	{
		if (SharedResourceManager.sharedInstance.patientDataController.patientCancer!.count > 0)
		{
			let cancerValue:Int = SharedResourceManager.sharedInstance.patientDataController.patientCancer![0].codeID
			
			if (cancerValue != 0)
			{
				SharedResourceManager.sharedInstance.questionController.getHistology(cancerID: cancerValue){() -> () in}
				SharedResourceManager.sharedInstance.questionController.getCancerStage(cancerID: cancerValue)
				SharedResourceManager.sharedInstance.questionController.getTumours(cancerID: cancerValue)
			}
		}
	}

	func addQuestionsToStack()
	{
		stackView.addArrangedSubview(spaceView(height: 8))
		stackView.addArrangedSubview(donutView(scale: 0.38, percentage: completionPercentage))
		
		for t in titles{
			switch (t.count) {
				
			// If the list is empty, then just add a line
			case 0:
				stackView.addArrangedSubview(lineView(height: 1))
				break;
				
			// If the list contains one element then add a long section that stretches across the view
			case 1:
				
				let index = titles.index(where: {$0 == t})
				
				// Update Section Labels
				let sectionView = SummarySectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: stackItemHeight))
				sectionView.sectionTitle.text = t[0]
				sectionView.sectionLabel.text = selectedChoices[index!][0]
				
				// Update Section Status Image
				if (sectionView.sectionLabel.text == "Not answered")
				{
					sectionView.status = .incomplete
				}
				
				// Add Constraints
				let heightConstraint = NSLayoutConstraint(item: sectionView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: stackItemHeight)
				sectionView.addConstraint(heightConstraint)
				
				let widthConstraint = NSLayoutConstraint(item: sectionView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width)
				sectionView.addConstraint(widthConstraint)
				
				// Add Custom Gesture Recognizer to View and attatch appropriate storyboard ID
				let gestureRec = SegueTapGestureRecognizer(target: self, action:  #selector(self.performSegueOnSectionTap(sender:)))
				gestureRec.theDestinationIdentifier = destinationIdentifiers[index!][0]
				sectionView.addGestureRecognizer(gestureRec)
				
				// Add the view to the stack
				stackView.addArrangedSubview(sectionView)
				allViewSections.append(sectionView)
				break;
				
			// If the list contains two elements then add two sections, each half the width of the view
			case 2:
				
				let index = titles.index(where: {$0 == t})
				
				// Create a horizontal stackview to split the two questions
				let horizontalStackView = UIStackView()
				horizontalStackView.axis = .horizontal
				horizontalStackView.alignment = .fill
				
				// Update Section 1 Labels
				let sectionView1 = SummarySectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: stackItemHeight))
				sectionView1.sectionTitle.text = t[0]
				sectionView1.sectionLabel.text = selectedChoices[index!][0]
				
				// Update Section 1 Status Image
				if (sectionView1.sectionLabel.text == "Not answered")
				{
					sectionView1.status = .incomplete
				}
				
				// Add Custom Gesture Recognizer to View 1 and attatch appropriate storyboard ID
				let gestureRec1 = SegueTapGestureRecognizer(target: self, action:  #selector(self.performSegueOnSectionTap(sender:)))
				gestureRec1.theDestinationIdentifier = destinationIdentifiers[index!][0]
				sectionView1.addGestureRecognizer(gestureRec1)
				
				// Update Section 2 Labels
				let sectionView2 = SummarySectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: stackItemHeight))
				sectionView2.sectionTitle.text = t[1]
				sectionView2.sectionLabel.text = selectedChoices[index!][1]
				
				// Update Section 2 Status Image
				if (sectionView2.sectionLabel.text == "Not answered")
				{
					sectionView2.status = .incomplete
				}
				
				// Add Custom Gesture Recognizer to View 2 and attatch appropriate storyboard ID
				let gestureRec2 = SegueTapGestureRecognizer(target: self, action:  #selector(self.performSegueOnSectionTap(sender:)))
				gestureRec2.theDestinationIdentifier = destinationIdentifiers[index!][1]
				sectionView2.addGestureRecognizer(gestureRec2)
				
				// Add Constraints
				let heightConstraint1 = NSLayoutConstraint(item: sectionView1, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: stackItemHeight)
				sectionView1.addConstraint(heightConstraint1)
				
				let widthConstraint1 = NSLayoutConstraint(item: sectionView1, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width/2)
				sectionView1.addConstraint(widthConstraint1)
				
				let heightConstraint2 = NSLayoutConstraint(item: sectionView2, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: stackItemHeight)
				sectionView2.addConstraint(heightConstraint2)
				
				let widthConstraint2 = NSLayoutConstraint(item: sectionView2, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width/2)
				sectionView2.addConstraint(widthConstraint2)
				
				let heightConstraintStack = NSLayoutConstraint(item: horizontalStackView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: stackItemHeight)
				horizontalStackView.addConstraint(heightConstraintStack)
				
				// Add the views to the horizontal stack
				horizontalStackView.addArrangedSubview(sectionView1)
				horizontalStackView.addArrangedSubview(sectionView2)
				
				// Add the horizontal stack to the main stack view
				stackView.addArrangedSubview(horizontalStackView)
				allViewSections.append(sectionView1)
				allViewSections.append(sectionView2)
				break;
				
			default:
				break;
			}
		}
		
		// Add a little bit of space at the bottom too
		stackView.addArrangedSubview(spaceView(height: 8))

	}
	
	// Returns a UIView that contains a horizonal line in the center of it
	func lineView(height:CGFloat) -> UIView{
		
		// Create the line view
		let line = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*0.92, height: 0.5))
		line.layer.borderWidth = 0.5
		line.layer.borderColor = UIColor(colorLiteralRed: 200/255, green: 199/255, blue: 204/255, alpha: 1.0).cgColor
		
		// Add constraints for line
		let lineHeightConstraint = NSLayoutConstraint(item: line, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 1)
		line.addConstraint(lineHeightConstraint)
		
		// Create a Holder view to place the line into, so there is space above and below the line
		let holder = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*0.92, height: height))
		
		// Add constraints for holder
		let holderHeightConstraint = NSLayoutConstraint(item: holder, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: height)
		holder.addConstraint(holderHeightConstraint)
		
		// position the line in the center of the holder
		line.center = CGPoint(x: 0, y: holder.frame.height/2)
		
		// add the line to the holder view
		holder.addSubview(line)
		
		self.allViewSections.append(line)
		
		return holder
	}
	
	// Returns a UIView that contains a horizonal line in the center of it
	func spaceView(height:CGFloat) -> UIView{
		
		// Create a Holder view to place the line into, so there is space above and below the line
		let holder = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*0.92, height: height))
		
		// Add constraints for holder
		let holderHeightConstraint = NSLayoutConstraint(item: holder, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: height)
		holder.addConstraint(holderHeightConstraint)
		
		return holder
	}

	
	
	func donutView(scale:CGFloat,percentage:Double) -> UIView{
		
		// Create Donut View
		let donut = DonutView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*scale, height: self.view.frame.width*scale))
		donut.percentage = percentage
		
		// Add Donut View Constraints
		let donutHeightConstraint = NSLayoutConstraint(item: donut, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width*scale)
		donut.addConstraint(donutHeightConstraint)
		
		let donutWidthConstraint = NSLayoutConstraint(item: donut, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width*scale)
		donut.addConstraint(donutWidthConstraint)
		
		// Create a Holder view to place the line into, so there is space above and below the line
		let holder = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*0.95, height: self.view.frame.width*scale))
		
		// Add constraints for holder
		let holderHeightConstraint = NSLayoutConstraint(item: holder, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width*scale)
		holder.addConstraint(holderHeightConstraint)
		
		// position the line in the center of the holder
		donut.center = CGPoint(x: 0, y: holder.frame.height/2)
		
		// add the line to the holder view
		holder.addSubview(donut)
		
		self.allViewSections.append(donut)
		
		return holder
	}
	
	
	
	// MARK: - Navigation
	/*
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
		
		if(segue.identifier == "createNewReport"){

			ReturnFromQuestionnaire().questionnaireFinnishedDelegate = self
		
		}

		
	}
	*/
	
	func performSegueOnSectionTap(sender : SegueTapGestureRecognizer)
	{
		//let controller = storyboard!.instantiateViewController(withIdentifier: sender.storyboardID!)
		
		// Animate the tapped view's background color in and out on tap
		UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseOut], animations: {
			sender.view!.backgroundColor = UIColor(colorLiteralRed: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
		}, completion: { (finished: Bool) in
		
			UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseInOut], animations: {
				sender.view!.backgroundColor = UIColor.clear
			}, completion: { (finished: Bool) in
				
			})
		
		})
		
		// Switch the storyboard ID that we attatched to the gesture recognizer and load it's appropriate views
		switch sender.theDestinationIdentifier!
		{
		case "PDFViewer":
			VCOverlayController.show(UIApplication.shared.keyWindow!, loadingText: "Retrieving your report")
			SharedResourceManager.sharedInstance.serverRequestController.retrievePDFForPatient(patientID: String(describing: SharedResourceManager.sharedInstance.patientID)){fileURL -> () in
				DispatchQueue.main.async
				{
					let stringURL:String = "\(fileURL)"
					
					let localReportURL = stringURL
					
					if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewer") as? VCPDFViewController
					{
						resultController.thePDFPath = stringURL
						self.navigationController?.pushViewController(resultController, animated: true)
						VCOverlayController.hide()
					}
				}
			}
			
			break

		case "Gender":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 1
			
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
				
			}
		
		case "Age":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 2
			
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AgeView") as? VCQuestionAge {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}
			
			
			
		case "Cancer Type":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 3
			
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}
			
		case "Histology":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 4
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}

		
		case "Stage":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 5
			
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}
		
		case "Tumor Markers":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 6
			
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AlterationView") as? VCQuestionTableView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}
			
		case "Genomic Alterations":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 7
			
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "AlterationView") as? VCQuestionTableView{
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}

		case "Health Problems":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 8
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}

		
		case "Medications":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 9
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}

		
		case "Allergies":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 10
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ChecklistView") as? VCQuestionChecklistView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}

		
		case "Ethnicity":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 11
			
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}
			
		case "Postal Code":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 12
			
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "PostalCodeView") as? VCQuestionPostalCode {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			
			}
			
		case "Health Insurer":
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber = 13
			
			// Note: This is not using the "Edit Question" View
			if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView {
				
				// Tell the controller we are just editing the question
				resultController.isEditingQuestion = true
				
				// Put the controller in a transparent navigation controller
				let transparentNavigationController = TransparentNavigationController(rootViewController: resultController)
				
				// Present the navigation controller (with question inside)
				present(transparentNavigationController, animated: true, completion: nil)
			}
			
		default:
			break
		}
		
		
		//self.navigationController?.pushViewController(controller, animated: true)
	}

	func updateQuestionsAndChoices()
	{
		SharedResourceManager.sharedInstance.questionController.getQuestions()
		
		SharedResourceManager.sharedInstance.questionController.getGenders()
		SharedResourceManager.sharedInstance.questionController.getGenomicAlterations()
		SharedResourceManager.sharedInstance.questionController.getOtherConditions()
		SharedResourceManager.sharedInstance.questionController.getMedications()
		SharedResourceManager.sharedInstance.questionController.getAllergies()
		SharedResourceManager.sharedInstance.questionController.getHealthInsurers()
		SharedResourceManager.sharedInstance.questionController.getEthnicities()
		
		SharedResourceManager.sharedInstance.questionController.resetQuestionNumbers()
	}
	
}
