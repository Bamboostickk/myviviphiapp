//
//  QuestionCollectionViewCell.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-11-18.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

class QuestionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    let outlineColor:CGColor = UIColor(colorLiteralRed: 221/255, green: 221/255, blue: 221/255, alpha: 1.0).cgColor
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        layer.borderWidth = 1.0
        layer.borderColor = outlineColor
        layer.cornerRadius = 6
    }
    
    
    
    override var isSelected: Bool {
        didSet{
			
			let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
			colorAnimation.fromValue = !isSelected ? tintColor.cgColor : UIColor.clear.cgColor
			colorAnimation.toValue = isSelected ? tintColor.cgColor : UIColor.clear.cgColor
			colorAnimation.duration = 0.15
			//colorAnimation.autoreverses = true
			
			colorAnimation.repeatCount = 1.0
			self.layer.add(colorAnimation, forKey: "animateSelection")
			
			
            self.layer.borderColor = isSelected ? tintColor.cgColor : outlineColor
            self.backgroundColor = isSelected ? tintColor : UIColor.clear
            self.label.textColor = isSelected ? UIColor.white : tintColor

			self.reloadInputViews()

        }
    }
	
	override func tintColorDidChange() {
		self.layer.borderColor = isSelected ? tintColor.cgColor : outlineColor
		self.backgroundColor = isSelected ? tintColor : UIColor.clear
		self.label.textColor = isSelected ? UIColor.white : tintColor
		self.reloadInputViews()
	}
	
	
}
