//
//  MVConnectionCheckController.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-02.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import Foundation
import SystemConfiguration

// This class handles functionality related to connectivity for both the app to the server, as well as whether or not the app is able to access the internet

class MVConnectionCheckController
{
	// Call this function whenever we need to test if a WiFi or data connection is available
	// If this returns false, the app should alert the user
	// This is only called if the user is doing something that requires an internet connection
	
	func isConnectionAvailble()->Bool
	{
		
		let rechability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.apple.com")
		
		var flags : SCNetworkReachabilityFlags = SCNetworkReachabilityFlags()
		
		if (SCNetworkReachabilityGetFlags(rechability!, &flags) == false)
		{
			return false
		}
		
		let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
		let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
		
		return (isReachable && !needsConnection)
	}
	
	// This method retrieves the API token from the server
	// It is intended to be called periodically to update the token
	// No input parameters are needed: the username and password is pulled from UserDefaults and the keychain, respectively
	// No return values: the resulting token is written to the UserDefault, as well.
	
	func retrieveToken()
	{
		let postData:NSMutableData = NSMutableData.init(data: "grant_type=password" .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		var username = "&username="
		var password = "&password="
		
		username += UserDefaults.standard.value(forKey: "username") as! String
		password += SharedResourceManager.sharedInstance.MyKeychainWrapper.myObject(forKey: "v_Data") as! String
		
		print("HAS LOGGED IN, REQUEST TOKEN")
		print(username + password)
		
		postData.append(username.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		postData.append(password.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "token"
		
		let fileURL = URL(string:urlString)
		
		var request = URLRequest(url: fileURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
		request.httpMethod = "POST"
		request.httpBody = postData as Data
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)
				
				if (httpResponse.statusCode == 200)
				{
					do
					{
						let resultingData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
						let JSONObject = JSON(resultingData)
	
						SharedResourceManager.sharedInstance.tokenAPI = JSONObject["access_token"].string!
					}
						
					catch let error as NSError
					{
						print(error.localizedDescription)
					}
				}
				
				else
				{
					print("ERROR " + String(describing: httpResponse.statusCode))
				}
			}
		}
		dataTask.resume()
	}
	
	func retrieveTokenWithDelay(onFinish:@escaping (() -> Void))
	{
		let postData:NSMutableData = NSMutableData.init(data: "grant_type=password" .data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		var username = "&username="
		var password = "&password="
		
		username += UserDefaults.standard.value(forKey: "username") as! String
		password += SharedResourceManager.sharedInstance.MyKeychainWrapper.myObject(forKey: "v_Data") as! String

		postData.append(username.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		postData.append(password.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)
		
		let urlString = SharedResourceManager.sharedInstance.URL_PATH + "token"
		
		let fileURL = URL(string:urlString)
		
		print("DELAYED REQUEST TOKEN")
		print(username + password)
		
		var request = URLRequest(url: fileURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
		request.httpMethod = "POST"
		request.httpBody = postData as Data
		
		let session = URLSession.shared
		
		let dataTask:URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
			if ((error) != nil)
			{
				print("\(error)")
			}
				
			else
			{
				let httpResponse = (response as! HTTPURLResponse)
				
				print("DELAYED TOKEN RETRIEVAL " + String(describing: httpResponse.statusCode))
				
				if (httpResponse.statusCode == 200)
				{
					do
					{
						let resultingData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
						let JSONObject = JSON(resultingData)

						SharedResourceManager.sharedInstance.tokenAPI = JSONObject["access_token"].string!
						
						onFinish()
					}
						
					catch let error as NSError
					{
						print(error.localizedDescription)
					}
				}
					
				else
				{
					//print("ERROR " + String(describing: httpResponse.statusCode))
				}
			}
		}
		dataTask.resume()
	}

}
