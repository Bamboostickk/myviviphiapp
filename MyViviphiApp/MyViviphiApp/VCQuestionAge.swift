//
//  VCQuestionAge.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2016-11-16.
//  Copyright © 2016 Wellness Computational. All rights reserved.
//

import UIKit

class VCQuestionAge:UIViewController,UITextFieldDelegate
{
	@IBOutlet var theNavigationBar: UINavigationBar!
	@IBOutlet var textLabel: UILabel!
	@IBOutlet var subtextLabel: UILabel!
	
	@IBOutlet var ageValue: UITextField!
	
	
	var isEditingQuestion = false
	var nextButtonText = "Next" // Set what text should be used in the top right corner
	
	var patientAge:Int = 0
	
	// Constraint age to 0 - 99
	
	override func willMove(toParentViewController parent: UIViewController?)
	{
		
		super.willMove(toParentViewController: parent)
		
		if parent == nil
		{
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		}
	}
	
	@IBAction func finishedSettingAge(_ sender: AnyObject)
	{
		
		// Note we should add the ability to skip the question here
		
		if (ageValue.text != "")
		{
			// Convert the patient's age to an Integer value
			self.patientAge = Int(ageValue.text!)!
			
			// Check if the age is within the supported range
			if ((self.patientAge >= 0) && (self.patientAge < 100))
			{
				
				self.customChoice(userInput: String(describing: self.patientAge))
				
				// If the patient is just editng their answer
				if isEditingQuestion{
					let patientIDValue = SharedResourceManager.sharedInstance.patientID
					SharedResourceManager.sharedInstance.patientDataController.obtainListOfAnswersFromServer(patientID: patientIDValue){ () -> () in
						DispatchQueue.main.async{
							self.dismiss(animated: true, completion: nil)
						}
					}
				}
					
				// Otherwise when we are in the questionnaire
				else
				{
					
					SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.updateValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID, values: [self.ageValue.text!])
					
					if let resultController = storyboard!.instantiateViewController(withIdentifier: "ButtonComplexView") as? VCQuestionButtonComplexView
					{
						self.navigationController?.pushViewController(resultController, animated: true)
					}
				}
			}
				
			else
			{
				displayErrorMessage(theTitle: "Age Not Supported", theMessage: "Ages between 0 and 99 are currently supported")
			}
			
		}
			
		else
		{
			displayErrorMessage(theTitle: "Nothing Entered", theMessage: "Please enter your age")
		}
	}
	
	@IBAction func backButtonPressed(_ sender: AnyObject)
	{
		SharedResourceManager.sharedInstance.questionController.activeQuestionNumber -= 1
		SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func cancelButtonPressed(_ sender: AnyObject){
		self.dismiss(animated: true, completion: nil)
	}
	
	// Allows the "Done" button to hide the keyboard
	func textFieldShouldReturn(_ textField: UITextField) -> Bool
	{
		textField.resignFirstResponder()
		return true
	}
	
	// Called when the characters are about to change in the textfield
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
	{
		// Get the new text string
		let newText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
		
		// If the new text is empty add the "Skip" button to the top right corner
		if(newText?.isEmpty)!{
			let nextButton = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.finishedSettingAge(_:)))
			self.navigationItem.setRightBarButton(nil, animated: true)
			self.navigationItem.setRightBarButton(nextButton, animated: true)
			return true // return should update when we have an empty string
		}
			
			// Otherwise if the old text is empty (and the next text wasn't empty from the previous statement) add the "Next" button to the top right corner
		else if(ageValue.text?.isEmpty)!{
			let nextButton = UIBarButtonItem(title: nextButtonText, style: .done, target: self, action: #selector(self.finishedSettingAge(_:)))
			self.navigationItem.setRightBarButton(nil, animated: true)
			self.navigationItem.setRightBarButton(nextButton, animated: true)
		}
		
		// Only return true if the age is less than 999 and it is an int entered (prevents bluetooth keyboards from typing chars)
		if let num = Int(newText!){
			if num < 999 && newText != "0000"{
				return true
			}
		}
		
		// This should only be executed if the user types a letter, or a number > 999
		return false
	}
	
	
	
	func dismissKeyboard()
	{
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
	
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		// When in edit mode
		if (isEditingQuestion)
		{
			
			nextButtonText = "Done"
			
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
			
			// Update the question Title
			self.textLabel.text = SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionText
			
			// Get the previous answer to the question
			let previousAge = SharedResourceManager.sharedInstance.patientDataController.patientAge?[0].customText

			// Check if the question was answered before
			if (previousAge?.isEmpty)!{
				// Add Skip button if they didn't answer the question originally
				let nextBarButton = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.finishedSettingAge(_:)))
				self.navigationItem.rightBarButtonItem = nextBarButton
			}
				
			else
			{
				// Add Done button if they did answer the question originally
				self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: nextButtonText, style: .done, target: self, action: #selector(self.finishedSettingAge(_:)))
				
				// Set the age field to contain their previous answer
				self.ageValue.text = previousAge
			}
			
			
			// Add Cancel button to the left since we are in edit mode
			self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelButtonPressed(_:)))
			
		}
			
		// When in the questionnaire
		else
		{
			nextButtonText = "Next"
			
			if (SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.age.count > 0)
			{
				// Add Skip button by default
				self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(self.finishedSettingAge(_:)))
			}
			
			else
			{// Add Skip button by default
				self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(self.finishedSettingAge(_:)))
				
			}

			
			
			
			SharedResourceManager.sharedInstance.questionController.activeQuestionNumber += 1
			SharedResourceManager.sharedInstance.questionController.updateAllQuestionValues()
		}
		
		
		ageValue.delegate = self
		ageValue.becomeFirstResponder()
		
		
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VCQuestionAge.dismissKeyboard))
		view.addGestureRecognizer(tap)
	}
	
	// Dispose of any resources that can be recreated.
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(true)
		let ageArray:[String] = SharedResourceManager.sharedInstance.patientQuestionnaireAnswers.retrieveValueForQuestion(questionID: SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		
		if (ageArray.count > 0)
		{
			self.ageValue.text = ageArray[0]
		}
	}
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	func customChoice(userInput:String)
	{
		let patientID:String =	String(SharedResourceManager.sharedInstance.patientID)
		let activeQuestion:String = String(SharedResourceManager.sharedInstance.questionController.activeQuestionID)
		let lookupID:String = String(SharedResourceManager.sharedInstance.questionController.returnActiveQuestion().questionLookupType)
		
		SharedResourceManager.sharedInstance.serverRequestController.writeCustomTextToServer(thePatientID: patientID, theQuestionID: activeQuestion, theLookUpType: lookupID, theAnswerText: userInput)
	}
	
	// Displays error message if nothing is entered
	
	func displayErrorMessage(theTitle:String, theMessage:String)
	{
		let alertView = UIAlertController(title: theTitle, message: theMessage as String, preferredStyle: .alert)
		let acceptAction = UIAlertAction(title: "Okay", style: .default, handler:nil)
		alertView.addAction(acceptAction)
		self.present(alertView, animated: true, completion: nil)
	}
}
