//
//  MyNewsTableViewCell.swift
//  Viviphi
//
//  Created by Sasha Ivanov on 2016-12-04.
//  Copyright © 2016 madebysasha. All rights reserved.
//

import UIKit

class MyNewsTableViewCell: UITableViewCell {

    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
