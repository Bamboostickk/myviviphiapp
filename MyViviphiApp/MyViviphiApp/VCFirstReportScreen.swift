//
//  VCFirstReportScreen.swift
//  MyViviphiApp
//
//  Created by Douglas Yuen on 2017-01-09.
//  Copyright © 2017 Wellness Computational. All rights reserved.
//

import UIKit

class VCFirstReportScreen: UIViewController
{
	
    @IBOutlet weak var createReportButton: FilledButton!
    
	// Do any additional setup after loading the view, typically from a nib.
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
	}
	
	// Dispose of any resources that can be recreated.
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		
	}
	
}
